<?php

class Admin_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function login($email, $password)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        $this->db->limit(1);

        $query = $this->db->get();


        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }

    function prepare_where($where)
    {
        if (is_array($where) && count($where) > 0) {
            foreach ($where as $column => $value) {
                $this->db->where($column, $value);
            }
        } elseif (is_string($where)) {
            $this->db->where($where);
        }

    }


    function prepare_order_by($order_by)
    {
        if (is_array($order_by) && count($order_by) > 0) {
            foreach ($order_by as $column => $value) {
                $this->db->order_by($column, $value);
            }
        } elseif (is_string($order_by)) {
            $this->db->order_by($order_by);
        }

    }


    function table_fetch_rows($table, $where = array(), $order_by = array())
    {
        $this->db->select('*');
        $this->db->from($table);

        $this->prepare_where($where);
        $this->prepare_order_by($order_by);

        $query = $this->db->get();
        return $query->result();
    }

    function table_fetch_row($table, $where = array(), $order_by = array())
    {
        $this->db->select('*');
        $this->db->from($table);

        $this->prepare_where($where);
        $this->prepare_order_by($order_by);

        $query = $this->db->get();
        return $query->result();
    }


    function get_id()
    {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        } else {
            $id = $this->uri->segment(4);
        }
        return $id;
    }

    function get_table_fields($table)
    {
        $fields = $this->db->list_fields($table);
        return $fields;
    }

    private function prepare_table_data($table, $data = array())
    {
        $fields = $this->get_table_fields($table);
        // print_r($fields);
        if (false == $fields || count($fields) == 0) {
            return false;
        }

        $insert = array();
        $data = empty($data) ? $this->input->post(null, true) : $data;
        //print_r($data);

        foreach ($fields as $field) {
            if (!isset($data[$field])) {
                continue;
            }

            $insert[$field] = $data[$field];
        }

        if (!empty($insert)) {
            return $insert;
        } else
            return false;
    }

    function table_insert($table, $data = array())
    {
        $insert = $this->prepare_table_data($table, $data);

        if ($insert == false) {
            return false;
        }

        if ($this->db->insert($table, $insert)) {
            return $this->db->insert_id();
        } else
            return false;
    }


    function table_update($table, $data = array(), $where = array())
    {
        $insert = $this->prepare_table_data($table, $data);

        if ($insert == false) {
            return false;
        }

        $this->prepare_where($where);

        if ($this->db->update($table, $insert)) {
            return true;
        } else
            return false;
    }

    function delete_table_row($table, $where)
    {
        $this->prepare_where($where);
        $this->db->delete($table);
    }

    public function purchase_list($id, $invoiceNo, $startDate, $endDate)
    {
        $this->db->select('purchases.*,purchaseshead.name as purchasesHeadName')->from('purchases');
        $this->db->join('purchaseshead', 'purchases.purchasesHeadId = purchaseshead.id', 'left');
        if ($id > 0) {
            $this->db->where('purchases.purchasesHeadId', $id)->where('purchases.issueDate BETWEEN "' . $startDate . '" and "' . $endDate . '"');
        } elseif ($invoiceNo != '') {
            $this->db->where('purchases.invoiceNo', $invoiceNo);
        } else {
            $this->db->where('purchases.issueDate BETWEEN "' . $startDate . '" and "' . $endDate . '"');
        }
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return false;
        }
        $result = $query->result_array();
        return array($result,$id,$invoiceNo,$startDate,$endDate);
    }

    public function expenses_list($id, $startDate, $endDate)
    {
        $this->db->select('expenses.*,expenseshead.name as expensesHeadName')->from('expenses');
        $this->db->join('expenseshead', 'expenses.expensesHeadId = expenseshead.id', 'left');
        if ($id > 0) {
            $this->db->where('expenses.expensesHeadId', $id)->where('expenses.paidDate BETWEEN "' . $startDate . '" and "' . $endDate . '"');
        } else {
            $this->db->where('expenses.paidDate BETWEEN "' . $startDate . '" and "' . $endDate . '"');
        }
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
            return false;
        }
        $result = $query->result_array();
        return array($result,$id,$startDate,$endDate);
    }

    public function reports_list($id, $startDate, $endDate)
    {
        if (isset($id) && is_numeric($id)) {
            $queryOB = $this->db->query("select * from client_openingbalance where clientId =" . $id . " order by clientId limit 1");
            $detailOB = $queryOB->row_array();

            $queryPast = $this->db->query("SELECT inv.id, inv.discount,inv.invoice_number, inv.container_number, inv.invoice_date, 0 AS amount FROM invoices  inv
		                  WHERE inv.clientId = " . $id . " AND inv.invoice_date < '" . $startDate . "'

                            UNION select ap.id, '','','', ap.paidDate, ap.amount from account_paid ap
                            WHERE ap.clientId = " . $id . " AND ap.paidDate < '" . $startDate . "'

                            ORDER BY invoice_date");

            $rslistPast = $queryPast->result_array();

            $grandPast = (is_array($detailOB) && !empty($detailOB) ? $detailOB['amount'] : 0);
            $paidPast = 0;
            $balancePast = 0;

            foreach ($rslistPast as $k => $row) {
                if ($row['amount'] == "0.00" && !empty($row['invoice_number'])) {
                    $query1 = $this->db->query("SELECT * FROM invoice_description WHERE invoice_id = " . $row['id']);
                    $rsResults1 = $query1->result_array();
                    $gst = 0;
                    $total = 0;
                    foreach ($rsResults1 as $result) {
                        $gst = $gst + ($result['gst'] == 15) ? (($result['charges'] * 15) / 100) : 0;
                        $total = $total + $result['charges'] + $gst;
                    }
                    $total = $total - $row['discount'];
                    $grandPast += $total;
                }
                $paidPast += $row['amount'];
            }
            $query = $this->db->query("SELECT inv.id, inv.invoice_number, inv.container_number,inv.discount, inv.invoice_date, 0 AS amount FROM invoices  inv
                                        WHERE inv.clientId = " . $id . "
                                        AND inv.invoice_date >= '" . $startDate . "' AND inv.invoice_date < '" . $endDate . "'

                                        UNION select ap.id, '','','', ap.paidDate, ap.amount from account_paid ap
                                        WHERE ap.clientId = " . $id . "
                                        AND ap.paidDate >= '" . $startDate . "' AND ap.paidDate <= '" . $endDate . "'

                                        ORDER BY invoice_date");

            $rslist = $query->result_array();
            return array($rslist, $grandPast, $paidPast,$id,$startDate,$endDate);
        }

    }

    function creditNoteTotal($id)
    {

        $query = $this->db->query("SELECT * FROM credit_note_description WHERE invoice_id = " . $id);
        $rsResults = $query->result_array();
        return $rsResults;
    }

    function acceptance($id)
    {
        $query = $this->db->query("SELECT * FROM acceptance WHERE invoiceId = " . $id);
        $rsResults = $query->result_array();
        return $rsResults;
    }

    function invoiceDescription($id)
    {
        $query = $this->db->query("SELECT * FROM invoice_description WHERE invoice_id = " . $id);
        $rsResults = $query->result_array();
        return $rsResults;
    }

    function booking_airFreight($id)
    {
        $query = $this->db->query("SELECT * FROM booking_airfreight WHERE invoiceId = " . $id);
        $rsResults = $query->result_array();
        return $rsResults;
    }
    function booking_seaFreight($id)
    {
        $query = $this->db->query("SELECT * FROM booking_seafreight WHERE invoiceId = " . $id);
        $rsResults = $query->result_array();
        return $rsResults;
    }

    function booking_airFreightDetails()
    {
        $query = $this->db->query("SELECT i.* FROM  booking_airfreight baf INNER JOIN invoices i
				ON baf.invoiceId = i.id
				where i.bookingFlag='N'");
        $res = $query->result_array();
        return $res;
    }

    function booking_seaFreightDetails()
    {
        $query = $this->db->query("SELECT i.* FROM  booking_seafreight baf INNER JOIN invoices i
				ON baf.invoiceId = i.id
				where i.bookingFlag='N'");
        $res = $query->result_array();
        return $res;
    }

    function airwaybill_detail($id)
    {
		$this->db->select('airwaybill.*,clients.name,clients.address,clients.email,clients.phone')->from('airwaybill');
		$this->db->join('clients', 'airwaybill.clientId = clients.id' ,'left');
		$query = $this->db->where('airwaybill.invoiceId',$id)->get();
        $result = $query->result_array();
        return $result;
    }

    function airfreight_detail($id)
    {
        $this->db->select('air_freight.*,clients.name,clients.address,clients.email,clients.phone')->from('air_freight');
        $this->db->join('clients', 'air_freight.clientId = clients.id' ,'left');
        $query = $this->db->where('air_freight.invoiceId',$id)->get();
        $result = $query->result_array();
        return $result;
    }

    function invoice_list($id,$invoiceNo,$containerNo,$date,$customGST)
    {
        $customGSTQ = "";
        if($customGST == 0){ $customGSTQ = "and customGST = 0" ;}
        if ($invoiceNo!=''){
            $query = $this->db->query("SELECT inv.*, c.name AS clientName FROM invoices  inv
                    LEFT JOIN clients c
                    ON inv.clientId = c.id
                    WHERE  invoice_number = '".$invoiceNo."'");

        }else if ($containerNo!=''){
            $query = $this->db->query("SELECT inv.*, c.name AS clientName FROM invoices  inv
                    LEFT JOIN clients c
                    ON inv.clientId = c.id
                    WHERE  container_number LIKE '%".$containerNo."%'");
        }else if ($id > 0){
            $query = $this->db->query("SELECT inv.*, c.name AS clientName FROM invoices  inv
                    LEFT JOIN clients c
                    ON inv.clientId = c.id
                    WHERE  c.id ='".$id."'");
        }else{
            $query = $this->db->query("SELECT inv.*, c.name AS clientName FROM invoices  inv
                    LEFT JOIN clients c
                    ON inv.clientId = c.id
                    WHERE EXTRACT(YEAR_MONTH FROM inv.invoice_date) = '$date'  $customGSTQ ORDER BY id DESC");
        }

        $rslist = $query->result_array();

        $queryCGST = $this->db->query("SELECT inv.*, c.name AS clientName FROM invoices  inv
                    LEFT JOIN clients c
                    ON inv.clientId = c.id
                    WHERE EXTRACT(YEAR_MONTH FROM inv.invoice_date) = '$date' and customGST = 1 ORDER BY id DESC");
        $rslistcgst = $queryCGST->result_array();

        return array($rslist,$rslistcgst);


    }

    function seafreight($id)
    {
        $this->db->select('sea_freight.*, clients.name,clients.address, clients.email,clients.phone')->from('sea_freight');
        $this->db->join('clients', 'sea_freight.clientId = clients.id' ,'left');
        $query = $this->db->where('sea_freight.invoiceId',$id)->get();
        $rsResults = $query->result_array();
        return $rsResults;
    }

    function bookingseafreight_pdf($id)
    {
        $this->db->select('booking_seafreight.*, clients.name,clients.address, clients.email,clients.phone')->from('booking_seafreight');
        $this->db->join('clients', 'booking_seafreight.consignorClientId = clients.id' ,'left');
        $query = $this->db->where('booking_seafreight.invoiceId',$id)->get();
        $rsResults = $query->result_array();
        return $rsResults;
    }

    function bookingairfreight_pdf($id)
    {
        $this->db->select('booking_airfreight.*, clients.name,clients.address, clients.email,clients.phone')->from('booking_airfreight');
        $this->db->join('clients', 'booking_airfreight.consignorClientId = clients.id' ,'left');
        $query = $this->db->where('booking_airfreight.invoiceId',$id)->get();
        $rsResults = $query->result_array();
        return $rsResults;
    }


}

?>