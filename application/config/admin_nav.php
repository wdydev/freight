<?php
return array(
    array(
        'name'  => 'Users',
        'icon'  => 'fa fa-user',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => 'users/add',
                'icon'  => 'fa fa-plus'
            ),
            array(
                'name'  => 'View',
                'url'   =>  'users/view',
                'icon'  => 'fa fa-eye'
            )
        )
    ),

    array(
        'name'  => 'Freight',
        'icon'  => 'fa fa-truck',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => 'freight/add',
                'icon'  => 'fa fa-plus'
            ),
            array(
                'name'  => 'View',
                'url'   =>  'freight/view',
                'icon'  => 'fa fa-eye'
            )
        )
    ),

    array(
        'name'  => 'Invoice',
        'icon'  => 'fa fa-newspaper-o',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => 'invoice/add',
                'icon'  => 'fa fa-plus'
            ),
            array(
                'name'  => 'View',
                'url'   =>  'invoice/view',
                'icon'  => 'fa fa-eye'
            ),
        )
    ),

    array(
        'name'  => 'Purchase',
        'icon'  => 'fa fa-newspaper-o',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => 'purchase/add',
                'icon'  => 'fa fa-plus'
            ),
            array(
                'name'  => 'View',
                'url'   =>  'purchase/view',
                'icon'  => 'fa fa-eye'
            ),
            array(
                'name'  => 'Purchases_head',
                'url'   =>  'purchase/purchaseshead_view',
                'icon'  => 'fa fa-plus'
            )
        )
    ),

    array(
        'name'  => 'Expenses',
        'icon'  => 'fa fa-newspaper-o',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => 'expenses/add',
                'icon'  => 'fa fa-plus'
            ),
            array(
                'name'  => 'View',
                'url'   =>  'expenses/view',
                'icon'  => 'fa fa-eye'
            ),
            array(
                'name'  => 'Expenses_head',
                'url'   =>  'expenses/expenseshead_view',
                'icon'  => 'fa fa-plus'
            )
        )
    ),

    array(
        'name'  => 'Reports',
        'icon'  => 'fa fa-pie-chart',
        'sub'   => array(
            array(
                'name'  => 'Reports List',
                'url'   => 'reports/view',
                'icon'  => 'fa fa-eye'
            ),
            array(
                'name'  => 'Add Account',
                'url'   => 'reports/add_account',
                'icon'  => 'fa fa-plus'
            ),
            array(
                'name'  => 'Account List',
                'url'   =>  'reports/view_account',
                'icon'  => 'fa fa-eye'
            ),
            array(
                'name'  => 'Add Opening balance',
                'url'   => 'reports/add_opening_blnc',
                'icon'  => 'fa fa-plus'
            ),
            array(
                'name'  => 'Opening Balance List',
                'url'   =>  'reports/view_opening_blnc',
                'icon'  => 'fa fa-eye'
            )
        )
    ),

    array(
        'name'  => 'Clients',
        'icon'  => 'fa fa-users',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => 'clients/add',
                'icon'  => 'fa fa-plus'
            ),
            array(
                'name'  => 'View',
                'url'   =>  'clients/view',
                'icon'  => 'fa fa-eye'
            )
        )
    ),

);