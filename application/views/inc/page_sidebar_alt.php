<?php @session_start(); ?>

<div id="sidebar-alt" tabindex="-1" aria-hidden="true">

    <a href="javascript:void(0)" id="sidebar-alt-close" onclick="App.sidebar('toggle-sidebar-alt');"><i class="fa fa-times"></i></a>


    <div id="sidebar-scroll-alt">

        <div class="sidebar-content">

            <div class="sidebar-section">
                <h2 class="text-light">Profile</h2>
                <?php if($this->session->userdata('logged_in')){ ?>
                <form action="<?php echo base_url('users/edit/'.$user->id);?>" method="post" class="form-control-borderless">
                    <div class="form-group">
                        <label for="user_name">Username</label>
                        <input type="text" id="user_name" name="user_name" class="form-control" value="<?php echo $user->name;?>">
                    </div>
                    <div class="form-group">
                        <label for="user_email">Email</label>
                        <input type="email" id="user_email" name="user_email" class="form-control" value="<?php echo $user->email;?>">
                    </div>
                    <div class="form-group">
                        <label for="user_password">New Password</label>
                        <input type="password" id="user_password" name="user_password" class="form-control" placeholder="Enter new Password">
                    </div>
                    <div class="form-group">
                        <label for="user_confirm_password">Confirm Password</label>
                        <input type="password" id="user_confirm_password" name="user_confirm_password" class="form-control" placeholder="Retype new Password">
                    </div>

                    <div class="form-group remove-margin">
                        <button type="submit" class="btn btn-effect-ripple btn-primary" onclick="App.sidebar('close-sidebar-alt');">Save</button>
                    </div>
                </form>
                <?php } ?>
            </div>

        </div>

    </div>

</div>


