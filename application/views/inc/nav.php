<?php
$primary_nav = array(
    array(
        'name'  => 'Dashboard',
        'url'   => 'index.php',
        'icon'  => 'gi gi-compass'
    ),
    array(
        'name'  => 'Accounts',
        'icon'  => 'fa fa-user',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => base_url('admin/accounts/add')
            ),
            array(
                'name'  => 'Overview',
                'url'   =>  base_url('admin/accounts/list')
            )
        )
    ),

    array(
        'name'  => 'Clients',
        'icon'  => 'fa fa-child',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => base_url('admin/clients/add')
            ),
            array(
                'name'  => 'Overview',
                'url'   =>  base_url('admin/clients/list')
            )
        )
    ),

    array(
        'name'  => 'Partners',
        'icon'  => 'fa fa-users',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => base_url('admin/partners/form')
            ),
            array(
                'name'  => 'Overview',
                'url'   =>  base_url('admin/partners/list')
            )
        )
    ),

    array(
        'name'  => 'News',
        'icon'  => 'fa fa-newspaper-o',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => base_url('admin/news/form')
            ),
            array(
                'name'  => 'Overview',
                'url'   =>  base_url('admin/news/list')
            )
        )
    ),

    array(
        'name'  => 'Members',
        'icon'  => 'fa fa-users',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => base_url('admin/members/form')
            ),
            array(
                'name'  => 'Overview',
                'url'   =>  base_url('admin/members/list')
            )
        )
    ),

    array(
        'name'  => 'Page',
        'icon'  => 'fa fa-file-powerpoint-o',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => base_url('admin/page/form')
            ),
            array(
                'name'  => 'Overview',
                'url'   =>  base_url('admin/page/list')
            )
        )
    ),


    array(
        'name'  => 'Slider',
        'icon'  => 'fa fa-exchange',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => base_url('admin/slider/form')
            ),
            array(
                'name'  => 'Overview',
                'url'   =>  base_url('admin/slider/list')
            )
        )
    ),

    array(
        'name'  => 'Events',
        'icon'  => 'fa fa-th-large',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => base_url('admin/events/form')
            ),
            array(
                'name'  => 'Overview',
                'url'   =>  base_url('admin/events/list')
            )
        )
    ),

    array(
        'name'  => 'Profiles',
        'icon'  => 'fa fa-smile-o',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => base_url('admin/profiles/form')
            ),
            array(
                'name'  => 'Overview',
                'url'   =>  base_url('admin/profiles/list')
            )
        )
    ),

    array(
        'name'  => 'Gallery',
        'icon'  => 'fa fa-picture-o',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => base_url('admin/gallery/add')
            ),
            array(
                'name'  => 'Overview',
                'url'   =>  base_url('admin/gallery/list')
            )
        )
    ),

    array(
        'name'  => 'Services',
        'icon'  => 'fa fa-strikethrough',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => base_url('admin/services/form')
            ),
            array(
                'name'  => 'Overview',
                'url'   =>  base_url('admin/services/list')
            )
        )
    ),

    array(
        'name'  => 'Location',
        'icon'  => 'fa fa-bank',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   => base_url('admin/location/form')
            ),
            array(
                'name'  => 'Overview',
                'url'   =>  base_url('admin/location/list')
            )
        )
    ),

    array(
        'name'  => 'Inquiries',
        'icon'  => 'fa fa-question',
        'sub'   => array(
            array(
                'name'  => 'Add',
                'url'   =>  base_url('admin/inquiries/form'),
                'icon'  => 'fa fa-plus'
            ),
            array(
                'name'  => 'Overview',
                'url'   =>  base_url('admin/inquiries/list')
            )
        )
    ),


);