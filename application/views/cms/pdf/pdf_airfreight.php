<style type='text/css'>
    <!--
    td {
        vertical-align: top;
    }

    .spacingtd td {
        padding: 5px;
        vertical-align: top;
    }

    .box {
        border: 1px solid #000;
        background: #fff;
        width: 20px;
        height: 20px;
        float: left;
    }

    .acceptTC {
        float: left;
    }

    -->
</style>

<table border='0' class="table" cellspacing='0' cellpadding='0' style='border-collapse:collapse;'>
    <tr>
        <td width='215' height='165'>
            <table border='1' class='table' cellpadding='0' cellspacing='0' style='border-collapse:collapse;'>
                <tr>
                    <td width='210'>SHIPPER:</td>
                </tr>
                <tr>
                    <td height='50'>
                        <pre><?php echo(!empty($rsResults) ?(($rsResults[0]['clientId'] == 0) ? $rsResults[0]['shipper'] : ($rsResults[0]['name'] . "<br/>" . $rsResults[0]['address'] . "<br/>" . $rsResults[0]['phone'] . "<br/>" . $rsResults[0]['email'])) : ""); ?></pre>
                    </td>
                </tr>
                <tr>
                    <td width='210'>CONSIGNEE:</td>
                </tr>
                <tr>
                    <td height='50'>
                        <pre><?php echo (!empty($rsResults) ? $rsResults[0]['consignee'] : ""); ?></pre>
                    </td>
                </tr>
                <tr>
                    <td height='56'>
                        <table border='0' cellspacing='5'>
                            <tr>
                                <td>MAWB No.</td>
                                <td>Shipper's Ref</td>
                            </tr>
                            <tr>
                                <td><?php echo (!empty($rsResults) ? $rsResults[0]['mawbNo'] : ""); ?></td>
                                <td><?php echo (!empty($rsResults) ? $rsResults[0]['shipperRef'] : ""); ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height='70'>Airport of Departure (address of first<br/> Carrier) and requested Routing<br/>
                        <?php echo (!empty($rsResults) ? $rsResults[0]['airportDept'] : ""); ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border='0' style='font-size:10px;'>
                            <tr>
                                <td width='40'>To</td>
                                <td width='80'>Flight <br/>/ Date</td>
                                <td width='80'>To/Flight<br/>/Date</td>
                            </tr>
                            <tr>
                                <td><?php echo (!empty($rsResults) ? $rsResults[0]['to'] : ""); ?></td>
                                <td><?php echo(!empty($rsResults) ? ($rsResults[0]['flightDate'] != "0000-00-00" ? date("d-M-Y", strtotime($rsResults[0]['flightDate'])) : "") : ""); ?></td>
                                <td><?php echo(!empty($rsResults) ? ($rsResults[0]['toFlightDate'] != "0000-00-00" ? date("d-M-Y", strtotime($rsResults[0]['toFlightDate'])) : "") :""); ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <td width='280'>
            <table border='1' class='spacingtd table' cellpadding='0' cellspacing='0' style='border-collapse:collapse;'>
                <tr>
                    <td style='width:406px;line-height:160%;  font-size:12px;'>
                        <img align='left' style=' width:100px;'
                             src='<?php echo base_url('assets/img/logo_04.png'); ?>'/>

                        <div style='padding-left:100px;'>DIVINE LOGISTICS LTD<br/>
                            Unit-3,61 Crooks Road, East Tamaki <br/>
                            PO Box: 76877 Manukau City-2241<br/>
                            Auckland, New Zealand<br/>
                            Phone: 09-263 7439 / 2151601<br/>
                            Fax: 09-263 7438<br/></div>
                    </td>
                </tr>
                <tr>
                    <td><b>SHIPPING LETTER OF INSTRUCTIONS</b></td>
                </tr>
                <tr>
                    <td height='170'>
                        <div style='width:500px;height:120px;line-height:150%; font-size:12px;'>
                            It is agreed that goods are here in are accepted in apparent good order and condition Mawb
                            No.
                            Shipper's Ref (except as noted) SUBJECT TO THE CONDITIONS OF CONTRACT OBTAINABLE ON
                            APPLICATION.
                            THE SHIPPERS ATTENTION IS DRAWN TO THE NOTICE CONCERNING CARRIER'S LIMITATION Airport of
                            Departure
                            (address of first OF LIABILITY.Shipper may increase such limitation of liability by
                            declaring a higher value for Carrier)
                            and requested Routing carriage and paying asupplemental charge if required
                        </div>
                        <table border='1' style='border-collapse:collapse;font-size:10px;'>
                            <tr>
                                <td>Express</td>
                                <td width='150'><?php echo (!empty($rsResults) ? $rsResults[0]['express'] :""); ?></td>
                                <td>Standby</td>
                                <td width='120'><?php echo (!empty($rsResults) ? $rsResults[0]['standBy'] : ""); ?></td>
                            </tr>
                            <tr>
                                <td>Currency</td>
                                <td></td>
                                <td>Declared/Value for customs</td>

                                <td>NZ$ <?php echo (!empty($rsResults) ? $rsResults[0]['nzAmt'] : ""); ?></td>

                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table border='1' class='spacingtd table' cellpadding='0' cellspacing='0'
       style='width:680px; font-size:12px; border-collapse:collapse;'>
    <tr>
        <td width='150'>Airport of Destination</td>
        <td style='width:100px;line-height:150%;  font-size:12px;'>
            <?php echo (!empty($rsResults) ? $rsResults[0]['airportDest'] : ""); ?>
        </td>
        <td width='150'>Consol Ref.</td>
        <td style='width:264px;line-height:150%;  font-size:12px;'>
            <?php echo (!empty($rsResults) ? $rsResults[0]['consoleRef'] : ""); ?>
        </td>
    </tr>
    <tr>
        <td>Handling Information</td>
        <td colspan='3'><?php echo (!empty($rsResults) ? $rsResults[0]['handlingInf'] : ""); ?></td>
    </tr>
    <tr>
        <td>Notify the consignee on arrival</td>
        <td colspan='3'><?php echo (!empty($rsResults) ? $rsResults[0]['notifyConsignee'] : ""); ?></td>
    </tr>
</table>
<table border='1' class='spacingtd' cellpadding='0' cellspacing='0' style='font-size:10px;border-collapse:collapse;'>
    <tr style='background:#000; color:#fff;'>
        <td width='70'>No.Pieces <br/>Pkgs</td>
        <td width='70'>Gross <br/>Weight Kg</td>
        <td width='70'>Rate Class <br/>Commodity <br/>Item No.</td>
        <td width='70'>Chargeable <br/>Weight Kg</td>
        <td width='65'>Rate/Charge</td>
        <td width='60'>Total</td>
        <td width='190'>Nature and Quality of Goods<br/>Including Dimension of Volumne</td>
    </tr><?php
    $cou = 0;
    if (is_array($wsResults) && !empty($wsResults)) {
        foreach ($wsResults as $result) {
            if (is_array($result) && !empty($results)) {
                ?>
                <tr>
                    <td><?php echo $result->noPieces; ?></td>
                    <td><?php echo $result->grossWeightKg; ?></td>
                    <td><?php echo $result->rateClass; ?></td>
                    <td> <?php echo $result->chargeableWeight; ?></td>
                    <td><?php echo $result->rateCharge; ?></td>
                    <td><?php echo $result->total; ?></td>
                    <td width='190'><?php echo $result->nature; ?></td>
                </tr>
                <?php
                $cou++;
            }
        }
    }
    if ($cou < 5) {
        for ($i = $cou; $i <= 5; $i++) { ?>

            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        <?php }
    }
    ?>
</table>
<table border='1' class='spacingtd' cellpadding='0' cellspacing='0'
       style='width:680px;font-size:10px;border-collapse:collapse;'>
    <tr>
        <td width='150'>FREIGHT CHARGES</td>
        <td width='40'>PREPAID</td>
        <td width='40'>COLLECT</td>
        <td width='434' rowspan='4'>
            Are goods security cleared as per Dept. <br/><br/>of transport and Regional services Regulations<br/><br/>
            <?php if (!empty($rsResults) && ($rsResults[0]['goodsSecurity'] == "Y")) { ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>YES
            <?php } elseif (!empty($rsResults) &&($rsResults[0]['goodsSecurity'] == "N")) { ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>NO
            <?php } ?>
        </td>
    </tr>
    <tr>
        <td>NZ ZONE SERVICE</td>
        <td><?php if (!empty($rsResults) &&($rsResults[0]['nzZoneService'] == "P")) { ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>YES
            <?php } else {
                echo "";
            } ?>
        </td>
        <td><?php if (!empty($rsResults) &&($rsResults[0]['nzZoneService'] == "C")) { ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>YES
            <?php } else {
                echo "";
            } ?>
        </td>
    </tr>
    <tr>
        <td>NZ TERM CHARGE</td>
        <td><?php if (!empty($rsResults) &&($rsResults[0]['nzTermCharge'] == "P")) { ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>YES
            <?php } else {
                echo "";
            } ?>
        </td>
        <td><?php if (!empty($rsResults) &&($rsResults[0]['nzTermCharge'] == "C")) { ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>YES
            <?php } else {
                echo "";
            } ?>
        </td>
    </tr>
    <tr>
        <td>Air FREIGHT</td>
        <td><?php if (!empty($rsResults) &&($rsResults[0]['airFreight'] == "P")) { ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>YES
            <?php } else {
                echo "";
            } ?>
        </td>
        <td><?php if (!empty($rsResults) &&($rsResults[0]['airFreight'] == "C")) { ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>YES
            <?php } else {
                echo "";
            } ?>
        </td>
    </tr>
    <tr>
        <td>DEST THC</td>
        <td><?php if (!empty($rsResults) &&($rsResults[0]['destThc'] == "P")) { ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>YES
            <?php } else {
                echo "";
            } ?>
        </td>
        <td><?php if (!empty($rsResults) &&($rsResults[0]['destThc'] == "C")) { ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>YES
            <?php } else {
                echo "";
            } ?>
        </td>
        <td width='425' rowspan='3'>
            Shipper certifies that the particurals on the face hereof are correct and that insofar as
            any part of the consignment contains dangerous goods, such part is properly described by
            name and in proper condition for carriage by air according to the international Air Transport
            Assosiation's Dangerous Goods Regulations.
        </td>
    </tr>
    <tr>
        <td> DEST ZONE ONC</td>
        <td><?php if (!empty($rsResults) &&($rsResults[0]['destZoneThc'] == "P")) { ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>YES
            <?php } else {
                echo "";
            } ?>
        </td>
        <td><?php if (!empty($rsResults) &&($rsResults[0]['destZoneThc'] == "C")) { ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>YES
            <?php } else {
                echo "";
            } ?>
        </td>
    </tr>
    <tr>
        <td>OTHER CHARGES</td>
        <td><?php if (!empty($rsResults) &&($rsResults[0]['otherCharges'] == "P")) { ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>YES
            <?php } else {
                echo "";
            } ?>
        </td>
        <td><?php if (!empty($rsResults) &&($rsResults[0]['otherCharges'] == "C")) { ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>YES
            <?php } else {
                echo "";
            } ?>
        </td>
    </tr>
    <tr>
        <td colspan='4'>
            <br/><br/><br/>
            <table border='0'>
                <tr>
                    <td>Executed on</td>

                    <td>Date</td>
                    <td width='100'><?php echo (!empty($rsResults) &&($rsResults[0]['executedDate']!="0000-00-00")?date("d-M-Y",
                        strtotime($rsResults[0]['executedDate'])):"");?>
                    </td>
                    <td>Signature of Shipper &nbsp;&nbsp;&nbsp;<?php echo (!empty($rsResults) ? ($rsResults[0]['clientId']==0?"":$rsResults[0]['name']) : "");?></td>
                    <td width='100'></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan='4'>
            ALL BUSINESS IS TRANSACTED UNDER THE COMPANY'S STANDARD TRADING CONDITIONS OBTAINABLE
            ON APPLICATION<br/>";
            <?php if (!empty($rsResults) &&($rsResults[0]['acceptTC']=="Y")){ ?>
            <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>
            <?php }else{ ?>
            <img src="<?php echo base_url('assets/img/box.png'); ?>"/>
            <?php } ?>
            I accept to all the <a href=\"http://www.divinelogistics.co.nz/downloads/terms_condo.pdf\">terms
                and conditions</a> as on website
        </td>
    </tr>
</table>