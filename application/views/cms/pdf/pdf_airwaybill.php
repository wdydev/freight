<div style='width:750px;text-align:right;'>HAWB No. S : <?php echo $detail[0]->invoice_number; ?></div>
<table border='0' class="table" cellspacing='0' cellpadding='0'>
    <tr>
        <td width='300' height='165'>
            <table border='1' class='table' cellpadding='0' cellspacing='0'>
                <tr>
                    <td colspan='6' width='300' style='position:relative;'><span class='fontSm'>Shipper Name and Address:</span>
                        <pre><?php echo(($rsResults[0]['clientId'] == 0) ? $rsResults[0]['shipper'] : ($rsResults[0]['name'] . "<br/>" . $rsResults[0]['address'] . "<br/>" . $rsResults[0]['phone'] . "<br/>" . $rsResults[0]['email'])); ?></pre>
                        <div class='sideBox'>
                            <span class='fontSm'>Shipper Account Number</span><br/>
                            <?php echo $rsResults[0]['shipperAccNo']; ?> test
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan='6' style='position:relative;'><span class='fontSm'>Consignee's Name and Address:</span>
                        <pre><?php echo $rsResults[0]['consignee']; ?></pre>
                        <div class='sideBox'>
                            <span class='fontSm'>Consignee Account Number</span><br/>
                            <?php echo $rsResults[0]['consigneeAccNo']; ?> test
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan='6' height='70'><span class='fontSm'>Issuing Carrier's Agent Name and City:</span>
                        <pre><?php echo $rsResults[0]['issueCarrier']; ?></pre>
                    </td>
                </tr>
                <tr>
                    <td colspan='2' height='27'><span
                            class='fontSm'>Agent's IATA Code</span><br/><?php echo $rsResults[0]['agentIATACode']; ?></td>
                    <td colspan='4'><span class='fontSm'>Account No.</span><br/><?php echo $rsResults[0]['agentAccNo']; ?>
                    </td>
                </tr>
                <tr>
                    <td colspan='6' height='50'><span class='fontSm'>Airport of Departure (Addr. of First Carrier) <br/>and Requested Routing</span>
                        <br/><?php echo $rsResults[0]['airportDeparture']; ?>
                    </td>
                </tr>
                <tr>
                    <td height='30'><span class='fontSm'>To</span><br/><br/><?php echo $rsResults[0]['toMain']; ?></td>
                    <td><span
                            class='fontSm'>By First Carrier /<br/> Routing and Destination</span><br/><?php echo $rsResults[0]['byFirstCarrier']; ?>
                    </td>
                    <td>to<br/><?php echo $rsResults[0]['to1']; ?></td>
                    <td>by<br/><?php echo $rsResults[0]['by1']; ?></td>
                    <td>to<br/><?php echo $rsResults[0]['to2']; ?></td>
                    <td>by<br/><?php echo $rsResults[0]['by2']; ?></td>
                </tr>
                <tr>
                    <td colspan='2' height='40'><span
                            class='fontSm'>Airport of Destination</span><br/><?php echo $rsResults[0]['airportOfDestination']; ?>
                    </td>
                    <td colspan='4'><span
                            class='fontSm'>Requested Flight/Date</span><br/><?php echo $rsResults[0]['requestedFlight']; ?>
                        / <?php echo date("d-M", strtotime($rsResults[0]['requestedDate'])); ?></td>
                </tr>

            </table>
        </td>
        <td>
            <table border='1' class='table' cellpadding='0' cellspacing='0'>
                <tr>
                    <td colspan='3' style='width:416px;line-height:150%;  font-size:12px;position:relative;'>
                        <div>Not Negotiable<br/>
                            <span style='font-size:18px;'>Air Waybill</span><br/>
                            Issued by

                        </div>
                        <div style='position:absolute; width:250px; right:0; top:5px; '>
                            <img align='left' style=' width:50px;'
                                 src='<?php echo base_url('assets/img/logo_04.png'); ?>'/>
                            DIVINE LOGISTICS LTD<br/>
                            <!--Unit-3,12 Lambie Drive Manukau <br/>
                            PO Box: 76877 Manukau City-2241<br/>
                            Auckland, New Zealand<br/>-->
                            Phone: 09-263 7439 / 2151601<br/>
                            Fax: 09-263 7438<br/></div>
                    </td>
                </tr>
                <tr>
                    <td colspan='3'>Copies 1,2 and 3 of this Air Waybill are originals and have the same validity.</td>
                </tr>
                <tr>
                    <td colspan='3'>
                        <div style=' line-height:150%; font-size:8px;width:416px;'>
                            It is agreed that the goods described herein are accepted in apparent good order and
                            condition (except as noted) for carriage. SUBJECT TO THE CONDITIONS OF THE CONTRACT ON THE
                            REVERSE HEREOF. ALL GOES MAY BE CARRIED BY AND OTHER MEANS INCLUDING ROAD OR ANY OTHER
                            CARRIER UNLESS SPECIFIC CONTRARY INSTRUCTIONS ARE GIVEN HEREON BY THE SHIPPER, AND SHIPPER
                            AGREES THAT THE SHIPMENT MAY BE CARRIED VIA INTERMEDIATE STOPPING PLACES WHICH THE CARRIER
                            DEEMS APPROPRIATE THE SHIPPER'S ATTENTION IS DRAWN TO THE NOTIC CONCERNING CARRIER'S
                            LIMITATION OF LIABILITY. Shipper may increase such limitation of liability by declaring a
                            higher value for carriage and paying a supplemental charge if required.
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan='3'><span>Accounting Information</span><br/>
                        <pre><?php echo $rsResults[0]['accoutingInformation']; ?></pre>
                    </td>
                </tr>
                <tr>
                    <td><span>Reference Number</span><br/>
                        <?php echo $rsResults[0]['referenceNo']; ?>
                    </td>
                    <td colspan='2'><span>Optional Shipping Information</span><br/>
                        <?php echo $rsResults[0]['optionalShippingInformation']; ?>
                    </td>

                </tr>
                <tr>
                    <td style='padding:0;'>
                        <table border='1' cellpadding='0' cellspacing='0'
                               style='border-collapse:collapse;font-size:12px; width:100px;'>
                            <tr>
                                <td rowspan='2'>Currency</td>
                                <td rowspan='2'>CHGS<br/>Code</td>
                                <td colspan='2'>WT/Val</td>
                                <td colspan='2'>Other</td>
                            </tr>
                            <tr>
                                <td>PPD</td>
                                <td>COLL</td>
                                <td>PPD</td>
                                <td>COLL</td>
                            </tr>
                            <tr>
                                <td><?php echo $rsResults[0]['currency']; ?></td>
                                <td><?php echo $rsResults[0]['chgsCode']; ?></td>
                                <td><?php echo $rsResults[0]['ppd_WTVal']; ?></td>
                                <td><?php echo $rsResults[0]['coll_WTVal']; ?></td>
                                <td><?php echo $rsResults[0]['ppd_Other']; ?></td>
                                <td><?php echo $rsResults[0]['coll_Other']; ?></td>
                            </tr>

                        </table>
                    </td>
                    <td style='width:95px;'><span class='fontSm'>Declared Value <br/>for Carriage</span><br/>
                        <?php echo $rsResults[0]['declaredValueCarriage']; ?>
                    </td>

                    <td style='width:92px;'><span class='fontSm'>Declared Value <br/>for Customs</span><br/>
                        <?php echo $rsResults[0]['declaredValueCustoms']; ?>
                    </td>
                </tr>
                <tr>
                    <td><span class='fontSm'>Amount of Insurance</span><br/>
                        <?php echo $rsResults[0]['amountOfInsurance']; ?>
                    </td>
                    <td colspan='2' class='fontSm'>
                        <div style='line-height:150%;width:210px; font-size:8px;'>INSURANCE - If Carrier offers
                            Insurance, and such Insurance is requested in accordance with the conditions thereof,
                            indicate amount to be insured in figures in box marked \"amount of insurance\"
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style='border:1px'>
            <span class='fontSm'>Handling Information </span><br/><?php echo $rsResults[0]['handlingInformation']; ?>
        </td>
        <td align='right' style='border:1px'>
            <span class='fontSm'>SCI</span><br/>
            <?php echo $rsResults[0]['sci']; ?>
        </td>
    </tr>
</table>
<table border='1' class='spacingtd' cellpadding='0' cellspacing='0' style='font-size:10px;border-collapse:collapse;'>
    <tr style='background:#000; color:#fff;'>
        <td width='70'>No.Pieces <br/>Pkgs</td>
        <td width='70'>Gross <br/>Weight Kg</td>
        <td width='70'>Rate Class <br/>Commodity <br/>Item No.</td>
        <td width='70'>Chargeable <br/>Weight Kg</td>
        <td width='65'>Rate/Charge</td>
        <td width='60'>Total</td>
        <td width='150'>Nature and Quality of Goods<br/>Including Dimension of Volumne</td>
    </tr>
    <?php echo
    $cou = 0;
    if (is_array($wsResults) && !empty($wsResults)) {
        foreach ($wsResults as $result) {
            if(is_array($result) && !empty($results)) {
                ?>
                <tr>
                    <td><?php echo $result->noPieces; ?></td>
                    <td><?php echo $result->grossWeightKg; ?></td>
                    <td><?php echo $result->rateClass; ?></td>
                    <td> <?php echo $result->chargeableWeight; ?></td>
                    <td><?php echo $result->rateCharge; ?></td>
                    <td><?php echo $result->total; ?></td>
                    <td width='190'><?php echo $result->nature; ?></td>
                </tr>
                <?php $cou++;
            }
        }
    }
    if ($cou < 3) {
        for ($i = $cou; $i <= 3; $i++) { ?>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        <?php }
    } ?>
</table>
<table border='1' class='table' cellpadding='0' cellspacing='0'>
    <tr>
        <td>Prepaid</td>
        <td>FREIGHT CHARGES</td>
        <td>COLLECT</td>
        <td rowspan='6' colspan='2'>
            <span>Other Charges</span><br/>
            <pre><?php echo $rsResults[0]['otherCharges']; ?></pre>
        </td>
    </tr>
    <tr>
        <td><?php echo $rsResults[0]['prepaid']; ?></td>
        <td><?php echo $rsResults[0]['weightCharge']; ?></td>
        <td><?php echo $rsResults[0]['collect']; ?></td>

    </tr>
    <tr>
        <td colspan='3'>Valuation Charge</td>
    </tr>
    <tr>
        <td colspan='3'><?php echo $rsResults[0]['valuationCharge']; ?></td>
    </tr>
    <tr>
        <td colspan='3'>Tax</td>
    </tr>
    <tr>
        <td colspan='3'><?php echo $rsResults[0]['tax']; ?></td>
    </tr>
    <tr>
        <td colspan='3'>Total Other Charges Due Agent</td>
        <td rowspan='4' colspan='2'>
            <div style='line-height:150%;width:330px; font-size:10px;'>Shipper certifies that the particurals on the
                face hereof are correct and that insofar as
                any part of the consignment contains dangerous goods, such part is propery described by
                name and in proper condition for carriage by air according to the international Air Transport
                Assosiation's Dangerous Goods Regulations.
            </div>
        </td>
    </tr>
    <tr>
        <td colspan='3'><?php echo $rsResults[0]['totalOtherChargesDueAgent']; ?></td>
    </tr>
    <tr>
        <td class='fontSm' colspan='3'>Total Other Charges Due Carrier</td>
    </tr>
    <tr>
        <td colspan='3'><?php echo $rsResults[0]['totalOtherChargesDueCarrier']; ?></td>
    </tr>
    <tr>
        <td colspan='3' width='200' height='50' style='background:#999;'>&nbsp;</td>
        <td align='center' colspan='2'>
            <span style='font-size:18px;'>DIVINE LOGISTICS LTD</span>
            <br/>Signature of Shipper or his Agent
        </td>

    </tr>
    <tr>
        <td colspan='2'>Total Prepaid</td>
        <td>Total Collect</td>
        <td rowspan='4' colspan='2'>
            <table border='0' width='500'>

                <tr>
                    <td><?php echo date("d-m-Y", strtotime($rsResults[0]['executedDate'])); ?></td>
                    <td align='center'><?php echo $rsResults[0]['atPlace']; ?></td>
                    <td align='right'><?php echo $rsResults[0]['signature']; ?></td>
                </tr>
                <tr>
                    <td>Executed on(date)</td>
                    <td align='center'>at (place)</td>
                    <td align='right'>Signature of Issuing Carrier or its Agent</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan='2'><?php echo $rsResults[0]['totalPrepaid']; ?></td>
        <td><?php echo $rsResults[0]['totalCollect']; ?></td>
    </tr>
    <tr>
        <td colspan='2'>Currency Conversion Rates</td>
        <td colspan='1'>CC. Charges in Dest. Currency</td>
    </tr>
    <tr>
        <td colspan='2'><?php echo $rsResults[0]['currencyConversionRates']; ?></td>
        <td><?php echo $rsResults[0]['ccChargesinDestCurrency']; ?></td>
    </tr>
    <tr>
        <td rowspan='2' colspan='2'>For Carrier's use only at Destination</td>
        <td>Charges at Destination</td>
        <td style='width:100px;'>Total Collect Charges</td>
        <td style='width:50px;'>&nbsp;</td>
    </tr>
    <tr>
        <td><?php echo $rsResults[0]['chargesAtDestination']; ?></td>
        <td><?php echo $rsResults[0]['totalCollectCharges']; ?></td>
        <td>&nbsp;</td>
    </tr>

</table>
<br/>
ALL BUSINESS IS TRANSACTED UNDER THE COMPANY'S STANDARD TRADING CONDITIONS OBTAINABLE
ON APPLICATION