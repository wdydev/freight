<?php


//creat objects of classes
$pdf = new FPDF('P','in',array(11.3,7));
//echo "<pre>";print_r($detail);die;
//echo "<pre>";print_r($query);die;


//set file name
$filename = "invoice_".$detail[0]->invoice_number.".pdf";
$space = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

$pdf->AddPage();
$pdf->SetLeftMargin(200);
$pdf->Image(base_url("assets/img/logo_03.png"),0.2,0.2,2.2,0.7);
$pdf->SetFont('Arial','',8);
$pdf->SetXY(2.9,0.2);
$pdf->MultiCell(3,0.2,"Divine Logistics Ltd\r\nUnit-3,61 Crooks Road,\r\nEast Tamaki, Auckland, NZ\r\nPO Box: 76877\r\nManukau City 2241, Auckland");
$pdf->SetXY(5,0.2);
$pdf->MultiCell(3.1,0.2,"Office: 09 2637439, 09 2151601\r\nMobile: 02102494202\r\nEmail: info@divinelogistics.co.nz\r\nWeb:   www.divinelogistics.co.nz");

$pdf->SetFont('Arial','B',14);
$pdf->SetXY(0.2,1.4);
$pdf->Cell(1,0.3,'Tax Credit Note '.$detail[0]->invoice_number);
$pdf->SetXY(0.2,1.6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1,0.3,'GST #: ');
$pdf->SetXY(0.7,1.6);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1,0.3,'102498070');
/* $pdf->SetXY(0.2,1.7);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1,0.3,'Client Name: '); */
if ($detail[0]->clientId=="0"){
    $pdf->SetXY(0.2,1.9);
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(1,0.3,$detail[0]->client_name);
    /* $pdf->SetXY(0.2,1.9);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(1,0.3,'Client Address: '); */
    $pdf->SetXY(0.2,2.1);
    //$pdf->SetFont('Arial','',9);
    $pdf->Cell(1,0.3,$detail[0]->client_address);
}else{
    $client = $this->admin_model->table_fetch_row('clients',array('id'=>$detail[0]->clientId));
    $pdf->SetXY(0.2,1.9);
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(1,0.3,$client[0]->name);
    $pdf->SetXY(0.2,2.05);
    $pdf->Cell(1,0.3,$client[0]->address);
    $pdf->SetXY(0.2,2.2);
    $pdf->Cell(1,0.3,$client[0]->phone);
    $pdf->SetXY(0.2,2.35);
    $pdf->Cell(1,0.3,$client[0]->email);
}
$pdf->SetXY(4.5,1.3);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1,0.3,'Account: ');
$pdf->SetXY(5.6,1.3);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1,0.3,$detail[0]->account_type);
$pdf->SetXY(4.5,1.5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1,0.3,'Invoice Date: ');
$pdf->SetXY(5.6,1.5);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1,0.3,date('d-M-Y', strtotime($detail[0]->invoice_date)));
$pdf->SetXY(4.5,1.7);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1,0.3,'Due Date: ');
$pdf->SetXY(5.6,1.7);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1,0.3,date('d-M-Y', strtotime($detail[0]->due_date)));
$pdf->SetXY(4.5,1.9);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1,0.3,'Terms: ');
$pdf->SetXY(5.6,1.9);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1,0.3,$detail[0]->terms);
$pdf->SetXY(4.5,2.1);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1,0.3,'Shipment: ');
$pdf->SetXY(5.6,2.1);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1,0.3,$detail[0]->shipment);
/*$pdf->SetXY(4.5,2.3);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1,0.3,'Container No.: ');
$pdf->SetXY(5.6,2.3);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1,0.3,$detail['container_number']);
*/
$pdf->SetXY(0.2,2.7);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(2.3,0.2,'Consignor',1);
$pdf->SetXY(0.2,2.9);
$pdf->SetFont('Arial','',9);
$pdf->Cell(2.3,0.2,$detail[0]->consignor,1);
$pdf->SetXY(2.5,2.7);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(2.5,0.2,'Client/Order Reference ',1);
$pdf->SetXY(2.5,2.9);
$pdf->SetFont('Arial','',9);
$pdf->Cell(2.5,0.2,$detail[0]->order_reference,1);
$pdf->SetXY(5.0,2.7);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1.9,0.2,'Consignee ',1);
$pdf->SetXY(5.0,2.9);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1.9,0.2,$detail[0]->consignee,1);

$pdf->SetXY(0.2,3.1);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(6.7,0.2,'Goods Description',1);
$pdf->SetXY(0.2,3.3);
$pdf->SetFont('Arial','',9);
$pdf->Cell(6.7,0.2,$detail[0]->goods_description,1);
$pdf->SetXY(5.0,3.1);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1.9,0.2,'LCL/FCL ',1);
$pdf->SetXY(5.0,3.3);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1.9,0.2,$detail[0]->LCL_FCL,1);

$pdf->SetXY(0.2,3.5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1.6,0.2,'Packages',1);
$pdf->SetXY(0.2,3.7);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1.6,0.2,$detail[0]->packages,1);
$pdf->SetXY(1.8,3.5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1.6,0.2,'Weight',1);
$pdf->SetXY(1.8,3.7);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1.6,0.2,$detail[0]->weight,1);
$pdf->SetXY(3.4,3.5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1.6,0.2,'Volume',1);
$pdf->SetXY(3.4,3.7);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1.6,0.2,$detail[0]->volume,1);
$pdf->SetXY(5.0,3.5);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1.9,0.2,'Chargeable',1);
$pdf->SetXY(5.0,3.7);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1.9,0.2,$detail[0]->chargeable,1);
if ($detail[0]->account_type=="Air"){
    $pdf->SetXY(0.2,3.9);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(2.5,0.2,'Flight / Flight Date',1);
    $pdf->SetXY(0.2,4.1);
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(2.5,0.2,$detail[0]->flight_name.' '.($detail[0]->flight_date=="0000-00-00"?"":date('d-M-Y', strtotime($detail[0]->flight_date))),1);
    $pdf->SetXY(2.7,3.9);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(2,0.2,'MAWB',1);
    $pdf->SetXY(2.7,4.1);
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(2,0.2,$detail[0]->mawb,1);
    $pdf->SetXY(4.7,3.9);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(2.2,0.2,'HAWB',1);
    $pdf->SetXY(4.7,4.1);
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(2.2,0.2,$detail[0]->hawb,1);
}else{
    $pdf->SetXY(0.2,3.9);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(2.5,0.2,'Vessel / Voyage',1);
    $pdf->SetXY(0.2,4.1);
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(2.5,0.2,$detail[0]->flight_name,1);
    $pdf->SetXY(2.7,3.9);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(2,0.2,'Container Number',1);
    $pdf->SetXY(2.7,4.1);
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(2,0.2,$detail[0]->container_number,1);
    $pdf->SetXY(4.7,3.9);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(2.2,0.2,'',1);
    $pdf->SetXY(4.7,4.1);
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(2.2,0.2,"",1);
}


$pdf->SetXY(0.2,4.3);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1.6,0.2,'Origin',1);
$pdf->SetXY(0.2,4.5);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1.6,0.2,$detail[0]->origin,1);
$pdf->SetXY(1.8,4.3);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1.6,0.2,'ETD',1);
$pdf->SetXY(1.8,4.5);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1.6,0.2,$detail[0]->etd,1);
$pdf->SetXY(3.4,4.3);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1.6,0.2,'Destination',1);
$pdf->SetXY(3.4,4.5);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1.6,0.2,$detail[0]->destination,1);
$pdf->SetXY(5.0,4.3);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1.9,0.2,'ETA',1);
$pdf->SetXY(5.0,4.5);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1.9,0.2,$detail[0]->eta,1);

$pdf->SetTextColor(255,255,255);
$pdf->SetFont('Arial','B',9);
$pdf->SetXY(0.2,5.0);
$pdf->Cell(4,0.2,'Description',1,0,'L',true);
$pdf->SetXY(4.2,5.0);
$pdf->Cell(1.3,0.2,'GST in NZD',1,0,'R',true);
$pdf->SetXY(5.5,5.0);
$pdf->Cell(1.4,0.2,'Charges in NZD',1,0,'R',true);

$pdf->SetTextColor(0,0,0);
$y_pos = 5.0;
$gst = 0;
$sub_total = 0;
$pdf->SetFont('Arial','',9);
if(is_array($query) && !empty($query)){
    foreach($query as $row){
        $gst = $gst + ($row['gst'] == 15 ? (($row['charges'] * 15) / 100) : 0);
        $sub_total += $row['charges'];
        $y_pos = $y_pos + 0.2;
        $gst_text = ($row['gst'] == 15) ? '15%' : 'Zero Rated';

        $pdf->SetXY(0.2,$y_pos);
        $pdf->Cell(4,0.2,$row['description']);
        $pdf->SetXY(4.2,$y_pos);
        $pdf->Cell(1.2,0.2,$gst_text,0,0,'R');
        $pdf->SetXY(5.4,$y_pos);
        $pdf->Cell(1.4,0.2,sprintf('%0.2f', $row['charges']),0,0,'R');
    }
}

$pdf->Line(0.2,7.6,6.8,7.6);

if($detail[0]->contact_enum == '1') {
    $pdf->SetXY(0.2,6.8);
    $pdf->MultiCell(2.7,0.2,'Please contact us within 7 days should there by any discrepancies');
}

$pdf->SetXY(4.3,7.6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1,0.3,'Sub Total');
$pdf->SetXY(5.5,7.6);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1.3,0.3,sprintf('%0.2f', round($sub_total,2)),0,0,'R');
$pdf->SetXY(4.3,7.8);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1,0.3,'Add GST');
$pdf->SetXY(5.5,7.8);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1.3,0.3,sprintf('%0.2f', round($gst,2)),0,0,'R');
if ($detail[0]->discount > 0){
    $pdf->SetXY(4.3,8.0);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(1,0.3,'Credit');
    $pdf->SetXY(5.5,8.0);
    $pdf->SetFont('Arial','',9);
    $pdf->Cell(1.3,0.3,sprintf('%0.2f', $detail[0]->discount),0,0,'R');
}
$pdf->SetTextColor(255,255,255);
$pdf->SetXY(4.3,8.25);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(1,0.3,'Total NZD',0,0,'L',true);
$pdf->SetTextColor(0,0,0);
$pdf->SetXY(5.5,8.25);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1.3,0.3,sprintf('%0.2f', round(($sub_total+$gst)-$detail[0]->discount,2)),0,0,'R');
$pdf->Line(0.2,8.6,6.8,8.6);
$pdf->SetTextColor(255,255,255);
$pdf->SetXY(0.2,8.6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(3,0.2,'Transfer Funds To:',0,0,'L',true);
$pdf->SetXY(0.2,8.8);
$pdf->SetTextColor(0,0,0);
$pdf->SetFont('Arial','',9);
$pdf->Cell(1,0.3,'Bank Details: ');
$pdf->SetXY(0.2,9.0);
$pdf->Cell(1,0.3,'Bank');
$pdf->SetXY(1.4,9.0);
$pdf->Cell(1,0.3,'011804');
$pdf->SetXY(0.2,9.2);
$pdf->Cell(1,0.3,'Account');
$pdf->SetXY(1.4,9.2);
$pdf->Cell(1,0.3,'0151562-00 ANZ');
$pdf->SetXY(0.2,9.4);
$pdf->Cell(1,0.3,'Swift');
$pdf->SetXY(1.4,9.4);
$pdf->Cell(1,0.3,'ANZ BNZ22');

$pdf->SetTextColor(255,255,255);
$pdf->SetXY(3.8,8.6);
$pdf->SetFont('Arial','B',9);
$pdf->Cell(3,0.2,'Address:',0,0,'L',true);
$pdf->SetFont('Arial','',9);
$pdf->SetXY(3.8,8.8);
$pdf->SetTextColor(0,0,0);
$pdf->MultiCell(3,0.2,"Divine Logistics Ltd.\r\nPO Box - 76877\r\nManukau City - 2241\r\nManukau");
$pdf->Line(0.2,9.65,6.8,9.65);
$pdf->SetXY(0.2,9.6);
$pdf->SetFont('Arial','',8);

$pdf->Cell(1,0.3,'**All shipments subject to our standard trading conditions, copies available on request or may be downloaded from our web site.**');
$pdf->Line(0.2,9.85,6.8,9.85);
$pdf->SetFont('Arial','',12);
$pdf->SetXY(1.2,9.9);
$pdf->Cell(1,0.3,'THIS IS A CREDIT NOTE. NO PAYMENT REQUIRED');
//finaly automatic save option for receipt inline to browser
$pdf->Output($filename, "I");
$pdf->Output('pdf/'.$detail[0]->invoice_number.'_invoice.pdf', 'F');
?>