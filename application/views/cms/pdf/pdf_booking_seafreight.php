<style type='text/css'>
    <!--
    td {
        vertical-align: top;
    }

    .spacingtd td {
        padding: 5px;
        vertical-align: top;
    }

    -->
</style>
<table border='0' style='width:700px;' cellpadding='5' cellspacing='0'>
    <tr>
        <td width='400'><img src='<?php echo base_url('assets/img/logo_03.png'); ?>' width='300'/></td>
        <td style='color:#348bbf;text-align:right;width:350px;line-height:150%;'>
            Unit-3,61 Crooks Road, East Tamaki <br/>
            PO Box: 76877 Manukau City-2241<br/>
            Auckland, New Zealand<br/>
            Phone: 09-263 7439 / 2151601<br/>
            Fax: 09-263 7438<br/>
        </td>
    </tr>
    <tr style='background:#999; color:#fff; font-size:25px;'>
        <td width='752' colspan='2'>Air Freight Departure Booking Confirmation</td>
    </tr>
    <tr>
        <td>Bill TO<br/>
            <pre><?php echo (!empty($rsResults) ? $rsResults[0]['billTo'] : ""); ?></pre>
        </td>
        <td align='right' width='300'>
            <table border='0' align='right'>
                <tr>
                    <td>SHIPMENT</td>
                    <td>SOO<?php echo (!empty($rsResults) ? $rsResults[0]['shipmentNo'] : ""); ?></td>
                </tr>
                <tr>
                    <td>CONSOL</td>
                    <td><?php echo (!empty($rsResults) ? $rsResults[0]['consol'] : ""); ?></td>
                </tr>
                <tr>
                    <td>DATE</td>
                    <td><?php echo (!empty($rsResults) ? (date("d-m-Y", strtotime($rsResults[0]['billDate']))):""); ?></td>
                </tr>
                <tr>
                    <td>CUT OFF DATE</td>
                    <td><?php echo (!empty($rsResults) ? (date("d-m-Y", strtotime($rsResults[0]['cutOffDate']))): ""); ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table border='1' cellspacing='5' cellpadding='5' style='width:750px;font-size:11px;'>

    <tr style='background:#999; color:#fff;'>
        <td colspan='5'>SHIPMENT DETAILS</td>
    </tr>
    <tr style='background:#000; color:#fff;'>
        <td colspan='2'>CONSIGNOR</td>
        <td colspan='3'>CONSIGNEE</td>
    </tr>
    <tr>
        <td colspan='2'>
            <pre><?php echo (!empty($rsResults) ? ($rsResults[0]['clientId'] == 0 ? $rsResults[0]['shipper'] : $rsResults[0]['clientName'] . "<br/>" . $rsResults[0]['address'] . "<br/>" . $rsResults[0]['phone'] . "<br/>" . $rsResults[0]['email']) : ""); ?></pre>
        </td>
        <td colspan='3'><?php echo (!empty($rsResults) ? $rsResults[0]['consignee'] : ""); ?></td>
    </tr>
    <tr style='background:#000; color:#fff;'>
        <td colspan='2'>EXPORT RECEIVING DEPOT</td>
        <td>CONSIGNOR'S REFERENCE</td>
        <td colspan='2'>CARRIER BOOKING REFERENCE</td>
    </tr>
    <tr>
        <td rowspan='5' colspan='2'>
            <pre><?php echo (!empty($rsResults) ? $rsResults[0]['exportReceivingDepot'] : ""); ?></pre>
            <br/>
            <b>Phone:</b><?php echo (!empty($rsResults) ? ($rsResults[0]['phone'] == "" ? "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" : $rsResults[0]['phone']) : ""); ?>
            <b>Fax:</b><?php echo (!empty($rsResults) ? $rsResults[0]['fax'] : ""); ?>
        </td>
        <td><?php echo (!empty($rsResults) ? $rsResults[0]['consignorRef'] : ""); ?>&nbsp;</td>
        <td colspan='2'><?php echo (!empty($rsResults) ? $rsResults[0]['carrierBookingRef'] : ""); ?>&nbsp;</td>
    </tr>
    <tr style='background:#000; color:#fff;'>
        <td colspan='3'>GOODS DESCRIPTION</td>
    </tr>
    <tr>
        <td colspan='3'><?php echo (!empty($rsResults) ? $rsResults[0]['carrierBookingRef'] : ""); ?>&nbsp;</td>
    </tr>
    <tr style='background:#000; color:#fff;'>
        <td colspan='3'>COMMODITY SUMMARY</td>
    </tr>
    <tr>
        <td colspan='3'><?php echo (!empty($rsResults) ? $rsResults[0]['carrierBookingRef'] : ""); ?>&nbsp;</td>
    </tr>
    <tr style='background:#000; color:#fff;'>
        <td width='100'>Ocean Bill of Lading</td>
        <td width='120'>WEIGHT</td>
        <td width='120'>VOLUME</td>
        <td width='120'>CHARGEABLE</td>
        <td width='115'>PACKAGES</td>
    </tr>
    <tr>
        <td><?php echo (!empty($rsResults) ? $rsResults[0]['oceanBillOfLading'] : ""); ?>&nbsp;</td>
        <td><?php echo (!empty($rsResults) ? $rsResults[0]['weight'] : ""); ?></td>
        <td><?php echo (!empty($rsResults) ? $rsResults[0]['volume'] : ""); ?></td>
        <td><?php echo (!empty($rsResults) ? $rsResults[0]['chargeable'] : ""); ?></td>
        <td><?php echo (!empty($rsResults) ? $rsResults[0]['packages'] : ""); ?></td>
    </tr>
    <tr style='background:#000; color:#fff;'>
        <td>GOODS COLLECTED FROM</td>
        <td>ETD</td>
        <td colspan='2'>GOODS DELIVERED TO</td>
        <td>ETA</td>
    </tr>
    <tr>
        <td width='100'><?php echo (!empty($rsResults) ? $rsResults[0]['goodsCollectedFrom'] : ""); ?>&nbsp;</td>
        <td width='100'><?php echo (!empty($rsResults) ? ($rsResults[0]['etd'] != "0000-00-00 00:00:00" ? date("d-m-Y", strtotime($rsResults[0]['etd'])) : "") : ""); ?></td>
        <td width='100' colspan='2'><?php echo (!empty($rsResults) ? $rsResults[0]['goodsDeliveredTo'] : ""); ?></td>
        <td width='100'><?php echo (!empty($rsResults) ? ($rsResults[0]['eta'] != "0000-00-00 00:00:00" ? date("d-m-Y", strtotime($rsResults[0]['eta'])) : "") : ""); ?></td>
    </tr>
    <tr style='background:#000; color:#fff;'>
        <td colspan='2'>ORDER NUMBERS / OWNER'S REFERENCE</td>
        <td colspan='3'>EXPORT CUSTOMS BROKER</td>
    </tr>
    <tr>
        <td colspan='2'><?php echo (!empty($rsResults) ? $rsResults[0]['orderNumber_Ref'] : ""); ?>&nbsp;</td>
        <td colspan='3'><?php echo (!empty($rsResults) ? $rsResults[0]['exportCustomsBroker'] : ""); ?>&nbsp;</td>
    </tr>
    <tr style='background:#000; color:#fff;'>
        <td colspan='5'>CARRIER</td>
    </tr>
    <tr>
        <td colspan='5'><?php echo (!empty($rsResults) ? $rsResults[0]['Carrier'] : ""); ?>&nbsp;</td>
    </tr>
</table><br/>
<table border='1' cellspacing='0' cellpadding='5' style='width:700px;border-collapse:collapse;'>
    <tr style='background:#999; color:#fff;'>
        <td colspan='7'>ROUTING INFORMATION</td>
    </tr>
    <tr style='background:#000; color:#fff;'>
        <td width='80'>Mode</td>
        <td width='100'>Flight/Date</td>
        <td width='130'>Carrier</td>
        <td width='100'>Load</td>
        <td width='100'>Disch.</td>
        <td width='100'>ETD</td>
        <td width='100'>ETA</td>
    </tr>
    <?php
    $cou = 0;
    if (!empty($wsResults)) {
        foreach ($wsResults as $result) {
            ?>
            <tr>
                <td><?php echo (!empty($result) ? $result->mode : ""); ?></td>
                <td><?php echo (!empty($result) ? $result->flightName : ""); ?> /
                    <?php echo (!empty($result) ? ($result->etd != "0000-00-00 00:00:00" ? date("d-m-Y", strtotime($result->flightDate)) : "") : ""); ?></td>
                <td><?php echo (!empty($result) ? $result->carrier : ""); ?></td>
                <td><?php echo (!empty($result) ? $result->loadDetail : ""); ?></td>
                <td><?php echo (!empty($result) ? $result->disch : ""); ?></td>
                <td><?php echo (!empty($result) ? ($result->etd != "0000-00-00 00:00:00" ? date("d-m-Y", strtotime($result->etd)) : "") : ""); ?></td>
                <td><?php echo (!empty($result) ? ($result->etd != "0000-00-00 00:00:00" ? date("d-m-Y", strtotime($result->eta)) : "") : ""); ?></td>
            </tr>
            <?php
            $cou++;
        }
    }
    if ($cou < 5) {
        for ($i = $cou; $i <= 5; $i++) {
            ?>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        <?php }
    } ?>
    <tr>
        <td colspan='7'>
            Marks and Numbers<br/>
            <?php echo (!empty($rsResults) ? $rsResult[0]['marksAndNumbers'] : ""); ?>
        </td>
    </tr>
</table>
<br/>
Yours Sincerely,<br/><br/>

<pre><?php echo (!empty($rsResults) ? $rsResults[0]['yourSincerely'] : ""); ?></pre>
<br/><br/>
ALL BUSINESS IS TRANSACTED UNDER THE COMPANY'S STANDARD TRADING CONDITIONS OBTAINABLE
ON APPLICATION