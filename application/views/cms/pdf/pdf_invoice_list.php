
<div id="page-content">
    <div class="block full">
        <table class="table" cellpadding="0" cellspacing="0">
            <tr>
                <td><img src="<?php echo base_url('assets/img/logo_03.png'); ?>"/> </td>
                <td>Divine Logistics Ltd<br/>Unit-3,12 Lambie Drive,<br/>Manukau Central, Auckland, NZ<br/>POBox: 76877<br/>Manukau City 2241, Auckland</td>
                <td>Office: 09 2637439, 09 2151601<br/>Mobile: 02102494202<br/>After Hours: 09-2673201<br/>Email :info@divinelogistics.co.nz<br/>Web :www.divinelogistics.co.nz</td>
            </tr>
        </table>
        <br/>
        <div class="table-responsive">
            <table border="1" class="table table-bordered table-vcenter" style="width:100%;">
                <thead>
                <tr>
                    <th class="text-center" width="50%">S.No</th>
                    <th width="70%">Invoice No.</th>
                    <th width="70%">Acc Type</th>
                    <th width="70%">Container No.</th>
                    <th width="50%">LCL/FCL</th>
                    <th width="100%">Invoice Date</th>
                    <th width="100%">Client Name</th>
                    <th width="70%">Total Amount</th>
                    <th width="70%">CR</th>
                    <th width="70%">B</th>
                    <th width="70%">Email</th>
                </tr>
                </thead>
                <tbody>
                <?php $grand_total = 0;
                $grand_total1 = 0;
                $credit_total = 0;
                $balance_total = 0;
                if (is_array($query[0]) && !empty($query[0])) {
                    foreach ($query[0] as $k => $row) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $k + 1; ?></td>
                            <td><?php echo (!empty($row) ? $row['invoice_number'] : ""); ?></td>
                            <td><?php echo (!empty($row) ? $row['account_type'] : ""); ?></td>
                            <td><?php echo (!empty($row) ? $row['container_number'] : ""); ?></td>
                            <td><?php echo (!empty($row) ? $row['LCL_FCL'] : ""); ?></td>
                            <td><?php echo (!empty($row) ? date("d M, Y", strtotime($row['invoice_date'])): ""); ?></td>
                            <td><?php echo (!empty($row) ? $row['clientName'] : ""); ?></td>
                            <td align="right">
                                <?php $gst = 0;
                                $total = 0;
                                $result = $this->admin_model->invoiceDescription($row['id']);
                                foreach ($result as $res) {
                                    $gst = ($res['gst'] == 15 ? (($res['charges'] * 15) / 100) : 0);
                                    $total = $total + $res['charges'] + $gst;
                                }
                                $total = $total - $row['discount'];
                                $grand_total += $total;
                                $balance_total += $total - $row['discount'];
                                $credit_total += $row['credit_note'];
                                echo $total;
                                ?>
                                <input type="hidden" value="<?php echo $total; ?>" name="total"
                                       id="total_<?php echo $row['id']; ?>"/>
                            </td>
                            <td align="right">
                                <?php
                                $result = $this->admin_model->creditNoteTotal($row['id']);
                                $gst = 0;
                                $total = 0;
                                foreach ($result as $res) {
                                    $gst = ($res['gst'] == 15 ? (($res['charges'] * 15) / 100) : 0);
                                    $total = $total + $res['charges'] + $gst;
                                }
                                $total = $total - $row['discount'];
                                $grand_total1 += $total;
                                $balance_total += $total - $row['discount'];
                                $credit_total += $row['credit_note'];
                                echo sprintf('%0.2f', $total);
                                ?>
                                <input type="hidden" value="<?php echo $total; ?>" name="total"
                                       id="total_<?php echo $row['id']; ?>"/>
                            </td>

                            <td>
                                <?php
                                if ($row['account_type'] == "Air") {
                                    $result = $this->admin_model->booking_airFreight($row['id']);
                                } else {
                                    $result = $this->admin_model->booking_seaFreight($row['id']);
                                }
                        if(is_array($result) && !empty($result)) {
                            if (($result[0]['id'] != NULL) && ($result[0]['id'] > 0)) {
                                    if ($row['bookingFlag'] == "N") { ?>
                                        <img src="<?php echo base_url('assets/img/delete.png'); ?>"/>;
                                    <?php } else { ?>
                                        <img src="<?php echo base_url('assets/img/green_button.png'); ?>"/>;
                                    <?php }
                                } }
                                ?>
                            </td>
                            <td><?php echo ($row['email'] == '1') ? 'Sent' : 'Not Sent'; ?></td>
                        </tr>
                    <?php }
                } ?>
                <tr>
                    <td colspan="7" align="right">Grand Total:</td>
                    <td align="right"><?php echo $grand_total; ?></td>
                    <td align="right"><?php echo $grand_total1; ?></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="7" align="right">Balance:</td>
                    <td align="right" colspan='2'><?php echo($grand_total - $grand_total1); ?></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>