
<div id="page-content">
    <div class="block full">
        <table class="invoice_table" cellpadding="0" cellspacing="0">
            <tr>
                <td><img src="<?php echo base_url('assets/img/logo_03.png'); ?>"/> </td>
                <td>Divine Logistics Ltd<br/>Unit-3,12 Lambie Drive,<br/>Manukau Central, Auckland, NZ<br/>POBox: 76877<br/>Manukau City 2241, Auckland</td>
                <td>Office: 09 2637439, 09 2151601<br/>Mobile: 02102494202<br/>After Hours: 09-2673201<br/>Email :info@divinelogistics.co.nz<br/>Web :www.divinelogistics.co.nz</td>
            </tr>
        </table>
        <br/>

        <div><?php if(!empty($query)){
            $client = $this->admin_model->table_fetch_row('clients', array('id' => $query[3])); }?>
            <div>Client Name: <b><?php echo (!empty($query) ? $client[0]->name : "" ); ?></b></div>
            <br/>

            <div>Statement of Account
                from <?php echo (!empty($query) ? "'" . date("Y M d", strtotime($query[4])) . "' to '" . date("Y M d", strtotime($query[5])) . "'" : ""); ?></div>
            <br/>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-vcenter"  border="1" style="width:100%">
                <thead>
                <tr>
                    <th class="text-center" style="width: 50px;">Serial No</th>
                    <th style="width: 100px;">Invoice Date</th>
                    <th style="width: 100px;">Invoice No.</th>
                    <th style="width: 100px;">Container No.</th>
                    <th style="width: 50px;">Note</th>
                    <th style="width: 80px;">Debit</th>
                    <th style="width: 80px;">Credit</th>
                    <th style="width: 100px;">Balance</th>
                </tr>
                </thead>
                <tbody>
                <?php $grand_total = 0;
                $paidTotal = 0;
                $balanceTotal = 0;
                $credit_total = 0;
                $balance_total = 0;
                $amtTotal = 0; ?>
                <tr>
                    <td></td>
                    <td colspan="4" class="text-right"><b> Opening balance : </b></td>
                    <td><b><?php echo (!empty($query) ? $query[1] : ""); ?> </b></td>
                    <td><b><?php echo (!empty($query) ? $query[2] : ""); ?> </b></td>
                    <td><b><?php echo (!empty($query) ? ($query[1] - $query[2]) : ""); ?> </b></td>
                </tr>
                <?php if (is_array($query[0]) && !empty($query[0])) {
                    foreach ($query[0] as $k => $row) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $k + 1; ?></td>
                            <td><?php echo (!empty($row) ? date("Y M d",strtotime($row['invoice_date'])) : ""); ?></td>
                            <td><?php echo (!empty($row) ? $row['invoice_number'] : ""); ?></td>
                            <td><?php echo (!empty($row) ? $row['container_number'] : ""); ?></td>

                            <td> <?php $paidTotal = 0;
                                $creditNoteTotal = 0;
                                if ($row['amount'] == "0.00" && !empty($row['invoice_number'])) {
                                    $result = $this->admin_model->creditNoteTotal($row['id']);
                                    $gst = 0;
                                    $total = 0;
                                    foreach ($result as $res) {

                                        $gst = $gst + ($res['gst'] == 15) ? (($res['charges'] * 15) / 100) : 0;
                                        $total = $total + $res['charges'] + $gst;
                                    }
                                    $creditNoteTotal = $total;
                                    echo $total;
                                    $paidTotal += $creditNoteTotal;
                                }
                                ?></td>
                            <td align="right">
                                <?php
                                if ($row['amount'] == "0.00" && !empty($row['invoice_number'])) {
                                    $result = $this->admin_model->invoiceDescription($row['id']);
                                    $gst = 0;
                                    $total = 0;
                                    foreach ($result as $res) {
                                        $gst = ($res['gst'] == 15) ? (($res['charges'] * 15) / 100) : 0;
                                        $total = $total + $res['charges'] + $gst;
                                    }
                                    $total = $total - $row['discount'];
                                    $grand_total += $total;
                                    echo $total;


                                } else {
                                    $paidTotal += $row['amount'];
                                }
                                ?>
                                <?php ?>

                            </td>
                            <td align="right"><?php echo $row['amount']+$creditNoteTotal ; ?></td>
                            <td></td>
                        </tr>
                    <?php } }
                $totalRes = ($query[1] + $grand_total) - ($query[2] + $paidTotal);
                if ($totalRes > 0){ }else{ $totalRes = "0.00"; }
                ?>
                <tr>
                    <td></td>
                    <td colspan="4" class="text-right"><b> Grand Total : </b></td>
                    <td><b><?php echo ($query[1] + $grand_total); ?> </b></td>
                    <td><b><?php echo ($query[2] + $paidTotal); ?> </b></td>
                    <td><b><?php echo $totalRes; ?> </b></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
