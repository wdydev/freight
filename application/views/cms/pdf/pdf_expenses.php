
<div id="page-content">
    <div class="block full">
        <table class="invoice_table" cellpadding="0" cellspacing="0">
            <tr>
                <td><img src="<?php echo base_url('assets/img/logo_03.png'); ?>"/> </td>
                <td>Divine Logistics Ltd<br/>Unit-3,12 Lambie Drive,<br/>Manukau Central, Auckland, NZ<br/>POBox: 76877<br/>Manukau City 2241, Auckland</td>
                <td>Office: 09 2637439, 09 2151601<br/>Mobile: 02102494202<br/>After Hours: 09-2673201<br/>Email :info@divinelogistics.co.nz<br/>Web :www.divinelogistics.co.nz</td>
            </tr>
        </table>
        <br/>

        <div>
            <div>Purchases Statement
                from <?php echo "'" . date("Y M d", strtotime($query[2])) . "' to '" . date("Y M d", strtotime($query[3])) . "'"; ?></div>
            <br/>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 100px;">Serial No</th>
                    <th style="width: 100px;">Date</th>
                    <th style="width: 100px;">Name</th>
                    <th style="width: 100px;">Amount</th>
                </tr>
                </thead>
                <tbody>
                <?php $amtTotal = 0;
                if (is_array($query[0]) && !empty($query[0])) {
                    foreach ($query[0] as $k => $row) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $k + 1; ?></td>
                            <td><?php echo $row['paidDate']; ?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['amount']; ?></td>
                        </tr>
                        <?php $amtTotal = $amtTotal + $row['amount'];
                    }
                } ?>
                <tr>
                    <td></td>
                    <td colspan="2" class="text-center"><b>Total : </b></td>
                    <td><b><?php echo $amtTotal; ?> </b></td>
                </tr>
                </tbody>
            </table><br />
            <div style="text-align:right;font-size:large;"><strong>Grand Total : <?php echo $amtTotal; ?></strong></div>
        </div>
    </div>
</div>
