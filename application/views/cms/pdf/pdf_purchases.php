
<div id="page-content">
    <div class="block full">
        <table class="invoice_table" cellpadding="0" cellspacing="0">
            <tr>
                <td><img src="<?php echo base_url('assets/img/logo_03.png'); ?>"/> </td>
                <td>Divine Logistics Ltd<br/>Unit-3,12 Lambie Drive,<br/>Manukau Central, Auckland, NZ<br/>POBox: 76877<br/>Manukau City 2241, Auckland</td>
                <td>Office: 09 2637439, 09 2151601<br/>Mobile: 02102494202<br/>After Hours: 09-2673201<br/>Email :info@divinelogistics.co.nz<br/>Web :www.divinelogistics.co.nz</td>
            </tr>
        </table>
        <br/>

        <div>
            <div>Purchases Statement
                from <?php echo "'" . date("Y M d", strtotime($query[3])) . "' to '" . date("Y M d", strtotime($query[4])) . "'"; ?></div>
            <br/>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 100px;">Serial No</th>
                    <th style="width: 100px;">Issue Date</th>
                    <th style="width: 100px;">Invoice No.</th>
                    <th style="width: 100px;">CR No.</th>
                    <th style="width: 100px;">Name</th>
                    <th style="width: 100px;">Amount</th>
                    <th style="width: 100px;">GST</th>
                    <th style="width: 100px;">Paid</th>
                    <th style="width: 100px;">Paid Date</th>
                </tr>
                </thead>
                <tbody>
                <?php $amtTotal = 0;
                $gstTotal = 0;
                $paidTotal = 0;
                if (is_array($query[0]) && !empty($query[0])) {
                    foreach ($query[0] as $k => $row) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $k + 1; ?></td>
                            <td><?php echo $row['issueDate']; ?></td>
                            <td><?php echo $row['invoiceNo']; ?></td>
                            <td><?php echo $row['crNo']; ?></td>
                            <td><?php echo $row['purchasesHeadName']; ?></td>
                            <td><?php echo $row['amount']; ?></td>
                            <td><?php echo $row['gst']; ?></td>
                            <td><?php echo $row['paid']; ?></td>
                            <td><?php echo $row['paidDate']; ?></td>
                        </tr>
                        <?php $amtTotal = $amtTotal + $row['amount'];
                        $gstTotal = $gstTotal + $row['gst'];
                        $paidTotal = $paidTotal + $row['paid'];
                    }
                } ?>
                <tr>
                    <td></td>
                    <td colspan="4" class="text-center"><b>Total : </b></td>
                    <td><b><?php echo $amtTotal; ?> </b></td>
                    <td><b><?php echo $gstTotal; ?> </b></td>
                    <td><b><?php echo $paidTotal; ?> </b></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>



