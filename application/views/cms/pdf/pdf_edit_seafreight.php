<style type='text/css'>
    .spacingtd td {
        padding: 5px;
        vertical-align: top;
    }
</style>
<table border='1' style='width:700px;' class='spacingtd' cellpadding='0' cellspacing='0'
       style='border-collapse:collapse;'>
    <tr>
        <td>SHIPPER</td>
        <td colspan='2' style='width:300px;'><span style='font-size:18px;font-weight:bold;'>SHIPPING INSTRUCTION</span>
        </td>
        <td rowspan='2' style='width:100px;'>BOOKING
            NO:<br/><br/><?php echo (!empty($detail) ? $detail[0]->invoice_number : ""); ?></td>
    </tr>
    <tr>
        <td width='150'>
            <pre><?php echo (!empty($rsResults) && $rsResults[0]['clientId'] == 0 ? $rsResults[0]['shipper'] : ""); ?>
                <br/><?php echo (!empty($rsResults) ? $rsResults[0]['address'] : ""); ?>
                <br/><?php echo (!empty($rsResults) ? $rsResults[0]['phone'] : ""); ?>
                <br/><?php echo (!empty($rsResults) ? $rsResults[0]['email'] : ""); ?></pre>
        </td>
        <td colspan='2' style='font-size:12px;'>

            <div style='line-height:150%;width:300px;'>
                <img align='left' style=' width:100px;' src="<?php echo base_url('assets/img/logo_04.png'); ?>"/>
                DIVINE LOGISTICS LTD<br/>
                Unit-3,61 Crooks Road, East Tamaki<br/>
                PO Box: 76877 Manukau City-2241<br/>
                Auckland, New Zealand<br/>
                Phone: 09-263 7439 / 2151601<br/>
                Fax: 09-263 7438<br/></div>

        </td>

    </tr>
    <tr>
        <td>CONSIGNEE:</td>
        <td colspan='3' rowspan='3' style='width:560px;line-height:120%;font-size:11px;'>

            Received by the carrier from the shipper in apparant good order and condition ( Unless otherwise noted
            herein) the total numbers or quantity of containers or unit/packages
            indicated stated by the shipper to comprise the good specified below for carriage
            subject to the all the terms and conditions (Available on our website) from the place
            of reciept or the port of loading whichever is applicable,to the port of discharge
            or the place of delivery whichever is applicable. In accepting this Bill Of Lading
            the merchant expressly accepts and agrees to all its terms conditions exceptions
            whether printed stamped or written otherwise incorporated notwithstanding
            the non signing of this Bill of Lading by the Merchant
        </td>
    </tr>
    <tr>
        <td height='40'>
            <pre><?php echo(!empty($rsResults[0]) ? $rsResults[0]['consignee'] : ""); ?></pre>
        </td>
    </tr>
    <tr>
        <td>NOTIFY PARTY:</td>
    </tr>
    <tr>
        <td rowspan='6'>
            <pre><?php echo(!empty($rsResults[0]) ? $rsResults[0]['notifyParty'] : ""); ?></pre>
        </td>
        <td colspan='3'>
            <b>Total no. Of
                container/Packages: </b><?php echo(!empty($rsResults[0]) ? $rsResults[0]['noOfContainer'] : ""); ?></td>
    </tr>
    <tr>
        <td width='150'><b>Vessel and Voyage No.</b></td>
        <td><?php echo(!empty($rsResults[0]) ? $rsResults[0]['vesselVoyageNo'] : ""); ?></td>
        <td></td>
    </tr>
    <tr>
        <td><b>Place of Receipt</b></td>
        <td colspan='2'><b>Port of Loading</b></td>
    </tr>
    <tr>
        <td><?php echo(!empty($rsResults[0]) ? $rsResults[0]['placeReceipt'] : ""); ?></td>
        <td colspan='2'><?php echo(!empty($rsResults[0]) ? $rsResults[0]['portLoading'] : ""); ?></td>
    </tr>
    <tr>
        <td><b>Port of Discharge</b></td>
        <td colspan='2'><b>Final Destination</b></td>
    </tr>
    <tr>
        <td><?php echo(!empty($rsResults[0]) ? $rsResults[0]['portDischarge'] : ""); ?></td>
        <td colspan='2'><?php echo(!empty($rsResults[0]) ? $rsResults[0]['finalDestination'] : ""); ?></td>
    </tr>
</table>
<table border='1' class='spacingtd' cellpadding='0' cellspacing='0'
       style='width:650px;font-size:10px;border-collapse:collapse;'>
    <tr style='background:#000; color:#fff;'>
        <td>ITEM NO.</td>
        <td width='100'>Marks and No.</td>
        <td width='121'>Kind of Packages</td>
        <td width='150'>Description</td>
        <td width='38'>Net Weight</td>
        <td width='39'>Gross Weight</td>
        <td width='115'>Total Cubic Measurement</td>
    </tr>
    <?php
    $cou = 0;
    if (!empty($wsResults)) {
        foreach ($wsResults as $result) {
            ?>
            <tr>
                <td><?php echo $result->itemNo; ?></td>
                <td width='100'><?php echo $result->marksAndNo; ?></td>
                <td width='121'><?php echo $result->kindOfPackages; ?></td>
                <td width='150'><?php echo $result->description; ?></td>
                <td width='38'><?php echo $result->netWeight; ?></td>
                <td width='39'><?php echo $result->grossWeight; ?></td>
                <td width='115'><?php echo $result->totalCubic; ?></td>
            </tr>
            <?php
            $cou++;
        }
    }
    if ($cou < 5) {
        for ($i = $cou; $i <= 5; $i++) {
            ?>
            <tr>
                <td>&nbsp;</td>
                <td width='100'>&nbsp;</td>
                <td width='121'>&nbsp;</td>
                <td width='150'>&nbsp;</td>
                <td width='38'>&nbsp;</td>
                <td width='39'>&nbsp;</td>
                <td width='115'>&nbsp;</td>
            </tr>
        <?php }
    } ?>
</table>
<table border='1' class='spacingtd' cellpadding='0' cellspacing='0'
       style='width:700px;font-size:10px;border-collapse:collapse;'>
    <tr>
        <td><b>VALUE FOR CUSTOMS <br/>NZ$ </b><?php echo(!empty($rsResults[0]) ? $rsResults[0]['nzAmt'] : ""); ?></td>
        <td width='40'>&nbsp;</td>
        <td width='40'>&nbsp;</td>
        <td width='40'>&nbsp;</td>
        <td colspan='3'>&nbsp;</td>

    </tr>
    <tr style='background:#000; color:#fff;'>
        <td width='160'>FREIGHT CHARGES</td>
        <td width='40'>PREPAID</td>
        <td width='40'>COLLECT</td>
        <td width='40'>&nbsp;</td>
        <td width='106'>TYPE OF SERVICE</td>
        <td width='40'>&nbsp;</td>
        <td width='170'>SPECIAL INSTRUCTION</td>

    </tr>
    <tr>
        <td>1. NZ ZONE SERVICE</td>
        <td><?php if (!empty($rsResults[0]) && $rsResults[0]['nzZoneService'] == "P"){ ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>" /><?php } else {
            echo "";} ?></td>
        <td><?php if (!empty($rsResults[0]) && $rsResults[0]['nzZoneService'] == "C"){ ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>" /><?php } else {
                echo "";} ?></td>
        <td rowspan='7'>&nbsp;</td>
        <td>FCL/FCL</td>
        <td><?php echo(!empty($rsResults[0]) ? $rsResults[0]['service1'] : ""); ?>    </td>
        <td rowspan='7'><?php echo(!empty($rsResults[0]) ? $rsResults[0]['specialInstruction'] : ""); ?></td>
    </tr>
    <tr>
        <td>2. NZ TERM CHARGE</td>
        <td><?php if (!empty($rsResults[0]) && $rsResults[0]['nzTermCharge'] == "P"){ ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>" /><?php } else {
                echo "";} ?></td>
        <td><?php if (!empty($rsResults[0]) && $rsResults[0]['nzTermCharge'] == "C"){ ?>
            <img src="<?php echo base_url('assets/img/tick.png'); ?>" /><?php } else {
                echo "";} ?></td>

        <td>FCL/FCL</td>
        <td><?php echo(!empty($rsResults[0]) ? $rsResults[0]['service2'] : ""); ?>    </td>

    </tr>
    <tr>
        <td>3. OCEAN FREIGHT</td>
        <td><?php if (!empty($rsResults[0]) && $rsResults[0]['oceanFreight'] == "P"){ ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>" /><?php } else {
                echo "";} ?></td>
        <td><?php if (!empty($rsResults[0]) && $rsResults[0]['oceanFreight'] == "C"){ ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>" /><?php } else {
                echo "";} ?></td>

        <td>LCL/FCL</td>
        <td><?php echo(!empty($rsResults[0]) ? $rsResults[0]['service3'] : ""); ?>    </td>

    </tr>
    <tr>
        <td>4. DEST THC</td>
        <td><?php if (!empty($rsResults[0]) && $rsResults[0]['descThc'] == "P"){ ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>" /><?php } else {
                echo "";} ?></td>
        <td><?php if (!empty($rsResults[0]) && $rsResults[0]['descThc'] == "C"){ ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>" /><?php } else {
                echo "";} ?></td>

        <td>LCL/LCL</td>
        <td><?php echo(!empty($rsResults[0]) ? $rsResults[0]['service4'] : ""); ?>    </td>

    </tr>
    <tr>
        <td>5. DEST ZONE ONC</td>
        <td><?php if (!empty($rsResults[0]) && $rsResults[0]['destZoneThc'] == "P"){ ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>" /><?php } else {
                echo "";} ?></td>
        <td><?php if (!empty($rsResults[0]) && $rsResults[0]['destZoneThc'] == "C"){ ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>" /><?php } else {
                echo "";} ?></td>

        <td>BREAK BULK</td>
        <td><?php echo(!empty($rsResults[0]) ? $rsResults[0]['service5'] : ""); ?>    </td>

    </tr>
    <tr>
        <td>6. OTHER CHARGES</td>
        <td><?php if (!empty($rsResults[0]) && $rsResults[0]['destZoneThc'] == "P"){ ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>" /><?php } else {
                echo "";} ?></td>
        <td><?php if (!empty($rsResults[0]) && $rsResults[0]['destZoneThc'] == "C"){ ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>" /><?php } else {
                echo "";} ?></td>

        <td>RORO</td>
        <td><?php echo(!empty($rsResults[0]) ? $rsResults[0]['service6'] : ""); ?></td>

    </tr>
</table>

<table border='1' class='spacingtd' cellpadding='0' cellspacing='0'
       style='width:700px;font-size:14px;border-collapse:collapse;'>
    <tr>
        <td width='270'>IS THE SHIPMENT DANGEROUS GOODS<br/>
            <?php if (!empty($rsResults[0]) && $rsResults[0]['shipmentDangerous'] == "Y"){ ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>" /><?php } else {
                echo "";} ?>
            YES &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <?php if (!empty($rsResults[0]) && $rsResults[0]['shipmentDangerous'] == "N"){ ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>" /><?php } else {
                echo "";} ?>
            NO
            <br/><br/><br/>
            <span style='font-size:10px;'>Any part of the consignment contains dangerous goods,such part is properly described by name and is NAME in proper condition for carriage by sea according to the applicable dangerous goods regulation.</span>
        </td>
        <td width='258'>
            <b>FOR THE SHIPPER<br/>
                NAME</b><br/>
            <?php (!empty($rsResults) && $rsResults[0]['clientId'] == 0 ? $rsResults[0]['shipperName'] : ""); ?>
            <br/><br/>AT <?php echo(!empty($rsResults) ? $rsResults[0]['at'] : ""); ?>
            <br/><br/>DATED <?php echo(!empty($rsResults) && $rsResults[0]['dated'] != "0000-00-00" ? date("d-M-Y", strtotime($rsResults[0]['dated'])) : ""); ?>
        </td>
        <td width='160'><b>NO. OF ORIGINAL <br/>B/Ls</b><br/><?php echo (!empty($rsResults) ? $rsResults[0]['originalBL']:"");?></td>
    </tr>
    <tr>
        <td colspan='3' style='font-size:12px;'>ALL BUSINESS IS TRANSACTED UNDER THE COMPANY'S STANDARD TRADING
            CONDITIONS OBTAINABLE ON APPLICATION.
            <br/>
            <?php
            if (!empty($rsResults) && ($rsResults[0]['acceptTC'] == "Y")) {
                ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>
            <?php } else {
                ?>
                <img src="<?php echo base_url('assets/img/tick.png'); ?>"/>
            <?php } ?>
            I accept to all the <a href=\"http://www.divinelogistics.co.nz/downloads/terms_condo.pdf\">terms and
                conditions</a> as on website
        </td>
    </tr>
</table>
<?php
return;
?>