
<table border='0' class="table" cellpadding='5' cellspacing='0'>
    <tr>
        <td><img src='<?php echo base_url('assets/img/logo_03.png'); ?>' /></td>
        <td style='color:#348bbf;text-align:right;width:340px'>Freight Forwarders <br/>

            Air Freight <br/>

            Sea Freight <br/>

            Custom Clearance
        </td>
    </tr>
    <tr>
        <td colspan='2' style='padding-top:30px;'>
            <table border='0' class="table" cellpadding='5' cellspacing='0'>
                <tr>
                    <td>I,</td>
                    <td style='float:left; border-bottom:1px solid #999;  font-size:18px; padding-left:30px; width:650px; display:block; height:20px;'>
                        <?php echo(!empty($detail) ? $detail[0]->client_name : ""); ?>
                    </td>
                </tr>
            </table>
            <p style='line-height:300%; font-size:16px;'>
                (Name of person and Company Name )<br/>
                Authorize DIVINE LOGISTICS LTD unit-3, 12 Lambie Drive Manukau Auckland to act as forwarding agent for
                export/Import control and act as Customs Broker.
                <br/>This authorization will remain in effect until revoked by written letter Divine Logistics Ltd .
            </p>
            <br/><br/><br/>
            <table border='0' class="table" cellpadding='5' cellspacing='0'>
                <tr>
                    <td>Signed:</td>
                    <td style='float:left; border-bottom:1px solid #999;  font-size:18px; padding-left:30px; width:300px; display:block; height:20px;'></td>
                </tr>
                <tr>
                    <td colspan='2' height='20'></td>
                </tr>
                <tr>
                    <td>Title:</td>
                    <td style='float:left; border-bottom:1px solid #999;  font-size:18px; padding-left:30px; width:300px; display:block; height:20px;'></td>
                </tr>
                <tr>
                    <td colspan='2' height='20'></td>
                </tr>
                <tr>
                    <td>Date:</td>
                    <td style='float:left; border-bottom:1px solid #999;  font-size:18px; padding-left:30px; width:300px; display:block; height:20px;'></td>
                </tr>
            </table>
            <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
            <table border='0' class="table" align='center' cellpadding='10' cellspacing='10'>
                <tr>
                    <td>Web: <a href='http://www.divinelogistics.co.nz'>www.divinelogistics.co.nz</a></td>
                    <td>Unit-3, 12 Lambie Drive Manakau</td>
                    <td>PO Box: 76877 Manukau 2241</td>
                </tr>
                <tr>
                    <td>Email:<a href='mailto:info@divinelogistics.co.nz'>info@divinelogistics.co.nz</a></td>
                    <td>Phone: 09-2637439/2151601</td>
                    <td>Fax: 09-2637438</td>
                </tr>
            </table>
        </td>
    </tr>
</table>