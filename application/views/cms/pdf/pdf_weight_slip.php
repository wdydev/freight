<style type='text/css'>

    .spacingtd td {
        padding: 5px;
    }

</style>
<table border='0' style='width:700px;' cellpadding='5' cellspacing='0'>
    <tr>
        <td width='400'><img src="<?php echo base_url('assets/img/logo_03.png'); ?>" width='300'/></td>
        <td style='color:#348bbf;text-align:right;width:350px;line-height:150%;'>
            Unit-3,61 Crooks Road, East Tamaki <br/>
            PO Box: 76877 Manukau City-2241<br/>
            Auckland, New Zealand<br/>
            Phone: 09-263 7439 / 2151601<br/>
            Fax: 09-263 7438<br/>
        </td>
    </tr>
    <tr>
        <td colspan='2' style='padding-top:30px;'>
            <h1>Divine Logistics - Weight Slip</h1>

            <div
                style='margin-left:430px;padding:10px; border:1px solid #000; font-size:16px; display:block; width:305px;'>
                <table border='0' cellpadding='5' cellspacing='0'>
                    <tr>
                        <td><b>Client Name:</b></td>
                        <td><?php echo (!empty($detail) ? $detail[0]->clientName:""); ?><br/><br/></td>
                    </tr>
                    <tr>
                        <td><b>Destination:</b></td>
                        <td><?php echo (!empty($detail) ?$detail[0]->destination:""); ?></td>
                    </tr>
                </table>
            </div>
            <div style='clear:both;'></div>


            <table border='1' cellpadding='20' class='spacingtd' cellspacing='20'
                   style='border-collapse:collapse; border:1px solid #999;'>
                <tr>
                    <td>Date</td>
                    <td colspan='2'>Shipper</td>
                    <td colspan='2'>Consignee</td>
                    <td width='100'>Signed</td>
                </tr>
                <tr>
                    <td><?php echo(!empty($detail) && $detail[0]->date != "0000-00-00 00:00:00" ? date("Y-m-d", strtotime($detail[0]->date)) : ""); ?>
                        <br/><br/></td>
                    <td colspan='2'><?php echo (!empty($detail) ?$detail[0]->shipper:""); ?><br/><br/></td>
                    <td colspan='2'><?php echo (!empty($detail) ?$detail[0]->consignee:""); ?><br/><br/></td>
                    <td></td>
                </tr>
                <tr style='background:#000;color:#fff;'>
                    <td>PCS</td>
                    <td>Weight</td>
                    <td width='197'>DIMS</td>
                    <td width='197'>L X W X H</td>
                    <td>Volume</td>
                    <td>Volume Weight</td>
                </tr>
                <?php
                $weight = 0;
                $volume = 0;
                $counter = 1;
                $volumeWeight = 0;
                if(!empty($rsResults)){
                foreach ($rsResults as $result){
                ?>
                <tr>
                    <td><?php echo $result->pcs; ?></td>
                    <td><?php echo $result->weight; ?></td>
                    <td width='197'><?php echo $result->dims; ?></td>
                    <td width='197'><?php echo $result->lwh; ?></td>
                    <td><?php echo $result->volume; ?> CBM</td>
                    <td><?php echo $result->volumeweight; ?></td>
                </tr>
<?php $weight += $result->weight;
$volumeWeight += $result->volumeweight;
$volume += $result->volume;
$counter++;
} }
if ($counter < 10) {
    for ($i = $counter; $i <= 10; $i++) {?>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td width='197'>&nbsp;</td>
                    <td width='197'>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
   <?php  }
}?>
                <tr>
                    <td>&nbsp;</td>
                    <td><?php echo $weight;?> </td>
                    <td width='197'>&nbsp;</td>
                    <td width='197'>&nbsp;</td>
                    <td><?php echo $volume;?> CBM</td>
                    <td><?php echo $volumeWeight;?></td>
                </tr>
            </table>

        </td>
    </tr>
</table>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<table border='0' align='center' cellpadding='10' cellspacing='10'>
    <tr>
        <td>Web: <a href='http://www.divinelogistics.co.nz'>www.divinelogistics.co.nz</a></td>
        <td>Unit-3, 12 Lambie Drive Manakau</td>
        <td>PO Box: 76877 Manukau 2241</td>
    </tr>
    <tr>
        <td>Email:<a href='mailto:info@divinelogistics.co.nz'>info@divinelogistics.co.nz</a></td>
        <td>Phone: 09-2637439/2151601</td>
        <td>Fax: 09-2637438</td>
    </tr>
</table>