
<div id="page-content">
    <div class="block full">
        <table class="invoice_table" cellpadding="0" cellspacing="0">
            <tr>
                <td width="400"><img src="<?php echo base_url('assets/img/logo_03.png'); ?>"/> </td>
                <td style='color:#348bbf;text-align:right;width:340px;line-height:150%;'>
                    Unit-3,61 Crooks Road, East Tamaki <br/>
                    PO Box: 76877 Manukau City-2241<br/>
                    Auckland, New Zealand<br/>
                    Phone: 09-263 7439 / 2151601<br/>
                    Fax: 09-263 7438<br/>
                </td>
            </tr>
        </table>
        <br/>

        <div><b>Direct shippers Freight Acceptance Questionnaire</b><br/><br/></div>

                <div><p style='line-height:150%;'>The following details are required to assist in determining whether the provisions for the carriage
                        of Dangerous goods are applicable to the despatch of your consignment. <br/>
                        Please circle YES or No as appropriate</p></div><br/>

                    <table border='1' cellpadding='20' class='spacingtd' cellspacing='20' style='border-collapse:collapse; border:1px solid #999;'>
                        <input type="hidden" name="invoiceId" value="<?php echo $this->uri->segment(3);?>" />
                        <tr>
                            <td colspan='2'>ARE ANY OF THE FOLLOWING ITEMS INCLUDED IN THE CONSIGNMENT BEING PRESENTED?</td>
                            <td class='noborderleft'>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width='20'>1</td>
                            <td>Fireworks,ammunition,sporting firearms or explosives</td>
                            <td width='45'><?php if($rsResults[0]->acc_1=="Y"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />YES
                                         <?php }else{ echo "" ;}?>
                            </td>
                            <td width='45'><?php if($rsResults[0]->acc_1=="N"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />NO
                                <?php }else{ echo "" ;}?>
                            </td>
                        </tr>
                        <tr>
                            <td width='20'>2</td>
                            <td>Cylinders of compressed air,oxygenor liquid petrolium gas(LPG)any type of aeroso</td>
                            <td width='45'><?php if($rsResults[0]->acc_2=="Y"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />YES
                                <?php }else{ echo "" ;}?>
                            </td>
                            <td width='45'><?php if($rsResults[0]->acc_2=="N"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />NO
                                <?php }else{ echo "" ;}?>
                            </td>
                        </tr>
                        <tr>
                            <td width='20'></td>
                            <td>canEg(Deodorant shaving cream hairspray paintetc</td>
                            <td width='45'><?php if($rsResults[0]->acc_2a=="Y"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />YES
                                <?php }else{ echo "" ;}?>
                            </td>
                            <td width='45'><?php if($rsResults[0]->acc_2a=="N"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />NO
                                <?php }else{ echo "" ;}?>
                            </td>
                        </tr>
                        <tr>
                            <td width='20'>3</td>
                            <td>Comping stoves, cigerrette or pipe lighters or cigarrette lighter refills</td>
                            <td width='45'><?php if($rsResults[0]->acc_3=="Y"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />YES
                                <?php }else{ echo "" ;}?>
                            </td>
                            <td width='45'><?php if($rsResults[0]->acc_3=="N"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />NO
                                <?php }else{ echo "" ;}?>
                            </td>
                        </tr>
                        <tr>
                            <td width='20'>4</td>
                            <td>Nail polish, colognes,perfumes,paint,fuel,or paintthinners</td>
                            <td width='45'><?php if($rsResults[0]->acc_4=="Y"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />YES
                                <?php }else{ echo "" ;}?>
                            </td>
                            <td width='45'><?php if($rsResults[0]->acc_4=="N"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />NO
                                <?php }else{ echo "" ;}?>
                            </td>
                        </tr>
                        <tr>
                            <td width='20'>5</td>
                            <td>Matches</td>
                            <td width='45'><?php if($rsResults[0]->acc_5=="Y"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />YES
                                <?php }else{ echo "" ;}?>
                            </td>
                            <td width='45'><?php if($rsResults[0]->acc_5=="N"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />NO
                                <?php }else{ echo "" ;}?>
                            </td>
                        </tr>
                        <tr>
                            <td width='20'>6</td>
                            <td>Dry Ice,specimens or samples</td>
                            <td width='45'><?php if($rsResults[0]->acc_6=="Y"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />YES
                                <?php }else{ echo "" ;}?>
                            </td>
                            <td width='45'><?php if($rsResults[0]->acc_6=="N"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />NO
                                <?php }else{ echo "" ;}?>
                            </td>
                        </tr>
                        <tr>
                            <td width='20'>7</td>
                            <td>Detergents, bleaches, drain or oven cleaners</td>
                            <td width='45'><?php if($rsResults[0]->acc_7=="Y"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />YES
                                <?php }else{ echo "" ;}?>
                            </td>
                            <td width='45'><?php if($rsResults[0]->acc_7=="N"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />NO
                                <?php }else{ echo "" ;}?>
                            </td>
                        </tr>
                        <tr>
                            <td width='20'>8</td>
                            <td>Wet cell batteries</td>
                            <td width='45'><?php if($rsResults[0]->acc_8=="Y"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />YES
                                <?php }else{ echo "" ;}?>
                            </td>
                            <td width='45'><?php if($rsResults[0]->acc_8=="N"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />NO
                                <?php }else{ echo "" ;}?>
                            </td>
                        </tr>
                        <tr>
                            <td width='20'>9</td>
                            <td>Fiberglass repair kits, adheves and puncture repair kits</td>
                            <td width='45'><?php if($rsResults[0]->acc_9=="Y"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />YES
                                <?php }else{ echo "" ;}?>
                            </td>
                            <td width='45'><?php if($rsResults[0]->acc_9=="N"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />NO
                                <?php }else{ echo "" ;}?>
                            </td>
                        </tr>
                        <tr>
                            <td width='20'>10</td>
                            <td>Medicines containing alcohol</td>
                            <td width='45'><?php if($rsResults[0]->acc_10=="Y"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />YES
                                <?php }else{ echo "" ;}?>
                            </td>
                            <td width='45'><?php if($rsResults[0]->acc_10=="N"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />NO
                                <?php }else{ echo "" ;}?>
                            </td>
                        </tr>
                        <tr>
                            <td width='20'>11</td>
                            <td>Camping equipment</td>
                            <td width='45'><?php if($rsResults[0]->acc_11=="Y"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />YES
                                <?php }else{ echo "" ;}?>
                            </td>
                            <td width='45'><?php if($rsResults[0]->acc_11=="N"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />NO
                                <?php }else{ echo "" ;}?>
                            </td>
                        </tr>
                        <tr>
                            <td width='20'>12</td>
                            <td>Diving equipment</td>
                            <td width='45'><?php if($rsResults[0]->acc_13=="Y"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />YES
                                <?php }else{ echo "" ;}?>
                            </td>
                            <td width='45'><?php if($rsResults[0]->acc_13=="N"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />NO
                                <?php }else{ echo "" ;}?>
                            </td>
                        </tr>
                        <tr>
                            <td width='20'>13</td>
                            <td>Anytype of chemical, pesticides,herbicides etc</td>
                            <td width='45'><?php if($rsResults[0]->acc_13=="Y"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />YES
                                <?php }else{ echo "" ;}?>
                            </td>
                            <td width='45'><?php if($rsResults[0]->acc_13=="N"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />NO
                                <?php }else{ echo "" ;}?>
                            </td>
                        </tr>
                        <tr>
                            <td width='20'>14</td>
                            <td>Machiner with internal comustion engines such as lawn mowers garden trimmers or chainsaws</td>
                            <td width='45'><?php if($rsResults[0]->acc_14=="Y"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />YES
                                <?php }else{ echo "" ;}?>
                            </td>
                            <td width='45'><?php if($rsResults[0]->acc_14=="N"){?>
                                    <img src="<?php echo base_url('assets/img/tick.png');?>" />NO
                                <?php }else{ echo "" ;}?>
                            </td>
                        </tr>

                    </table><br />
                    <div><p style='line-height:200%; font-size:10px;'><b style='font-size:10px;'>PLEASE NOTE: </b>The Civil Aviation Authority CAA has developed legeslation in regard to the management of Dangerous Goods
                        as airfreight. Under sub regulation 262B of the Civil Aviation Act, amended in 1992 the following could be applicable;
                        Heavy penalties,including imprisonment apply to shippers who do not declare dangerous goods in any item to an airline
                        IF YOU ARE UNSURE THAT ANY ITEM YOU ARE CONSIGNING COULD BE CONSIDERED DANGEROUS GOODS,PLEASE
                        ASK OUR STAFF FOR ASSISTANCE</p></div>
                    <table border='0' class='spacingtd'  cellpadding='10' cellspacing='0'>
                        <tr>
                            <td colspan='2' height='50'><b>SHIPPERS NAME : </b> <span style='font-size:18px;'><?php echo $rsResults[0]->shipperName;?></span></td>
                        </tr>
                        <tr>
                            <td width='350'><b>SHIPPERS SIGNATURE</b></td>
                            <td width='350'><b>DATE : </b><?php echo (($rsResults[0]->date=='')||$rsResults[0]->date == '0000-00-00')?date('Y-m-d') :$rsResults[0]->date ;?></td>
                        </tr>
                    </table>

                    <br/><br/><br/>
                    Airline personnel only to complete<br/><br/>
                    <div style='border-top:1px solid #999;height:25px;padding-top:5px;'>Airway Bill number</div>
                    <div style='border-top:1px solid #999;height:25px;padding-top:5px;'>Additional information</div>
                    <div style='border-top:1px solid #999;height:25px;padding-top:5px;'>&nbsp;</div>
                    <div style='border-top:1px solid #999;height:25px;'>&nbsp;</div>

                </div>
            </div>
