<style type='text/css'>
    .bold{ font-weight: bold; }
    .spacingtd td{ padding:5px;}
    .invoice_table{border:none; margin:0 3%; width:94%;}
    .invoice_inner_table{border:none; width:94%;}
    .invoice_table td{padding:7px 4px; vertical-align:top;}
    .invoice_table td label{width:110px; float:left;}
    #container_wrap{display:none;}

</style>

<div class="row">
    <div class="row col-md-12">
        <div class="block">
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li><a href="<?php echo base_url('invoice/edit/' . $this->uri->segment(3)); ?>">Edit Invoice</a></li>
                    <?php if (!empty($detail1) &&($detail1[0]->account_type == "Air")){ ?>
                        <li><a href="<?php echo base_url('invoice/edit_air_freight/' . $this->uri->segment(3)); ?>">Edit
                                Air Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_airfreight/' . $this->uri->segment(3)); ?>">Edit Booking(Air)</a></li>
                    <?php } else { ?>
                        <li><a href="<?php echo base_url('invoice/edit_sea_freight/' . $this->uri->segment(3)); ?>">Edit Sea
                                Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_seafreight/' . $this->uri->segment(3)); ?>">Edit Booking(Sea)</a></li>
                    <?php } ?>
                    <li><a href="<?php echo base_url('invoice/edit_acceptance/' . $this->uri->segment(3)); ?>">Edit
                            Acceptance</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_credit_note/' . $this->uri->segment(3)); ?>">Edit
                            Credit Note</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_airwaybill/' . $this->uri->segment(3)); ?>">Edit
                            AirwayBill</a></li>
                    <li class="active"><a href="<?php echo base_url('invoice/edit_weight_slip/' . $this->uri->segment(3)); ?>">Edit
                            Weight Slip</a></li>
                    <li><a href="<?php echo base_url('invoice/inventory_list/' . $this->uri->segment(3)); ?>">Inventory List</a></li>
                    <li><a href="<?php echo base_url('invoice/email/' . $this->uri->segment(3)); ?>">Email</a></li>
                </ul>
            </div>

            <div class="col-md-12">
                <div class="col-md-6"><img src="<?php echo base_url('assets/img/logo_03.png'); ?>"/></div>
                <div class="col-md-3" style="float:right;text-align:right;">Unit-3,61 Crooks Road<br/>East Tamaki, Auckland, NZ<br/>PO
                    Box: 76877 Manukau City-2241<br/>Phone: 09-263 7439 / 2151601<br/>Fax: 09-263 7438
                </div>

            </div>
            <div class="clearfix"></div>
            <div><h1><b>Divine Logistics - Weight Slip</b></h1></div>
            <br/>

            <form action="" method="post" class="form-horizontal form-bordered">
            <table border='0' class="invoice_table" style=cellpadding='5' cellspacing='0'>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <table style="margin-left:670px;padding:10px; border:1px solid #000; font-size:16px; display:block; width:300px;"
                            cellpadding='5' cellspacing='5'>
                            <tr>
                                <td class="bold">Client Name</td>
                                <td><input type="text" name="clientName" value="<?php echo (!empty($query) ? $query[0]->clientName:"");?>"/>
                                </td>
                            </tr>
                            <tr>
                                <td class="bold">Destination</td>
                                <td><input type="text" name="destination" value="<?php echo (!empty($query) ? $query[0]->destination:""); ?>"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br/><br/>
            <table border='1' width="100%" cellpadding='20' class='invoice_table' cellspacing='20'
                   style='border-collapse:collapse; border:1px solid #999;'>
                <tr>
                    <td class="bold">Date</td>
                    <td colspan="2" class="bold">Shipper</td>
                    <td colspan="2" class="bold">Consignee</td>
                    <td class="bold">Signed</td>

                </tr>
                <tr>
                    <td><input type="text" name="date" id="date"
                               value="<?php echo (!empty($query) && $query[0]->date != "0000-00-00 00:00:00" ? date("d-m-Y", strtotime($query[0]->date)) : ""); ?>"/>
                    </td>
                    <td colspan="2"><input type="text" name="shipper" value="<?php echo (!empty($query) ? $query[0]->shipper:""); ?>"/></td>
                    <td colspan="2"><input type="text" name="consignee" value="<?php echo (!empty($query) ? $query[0]->consignee:""); ?>"/></td>
                    <td>&nbsp;</td>
                </tr>
                <tr style='background:#000;color:#fff;'>
                    <td width="118">PCS</td>
                    <td width="118">Weight</td>
                    <td width="180">DIMS</td>
                    <td width="180">L X W X H</td>
                    <td width="121">Volume</td>
                    <td width="115">Volume Weight</td>
                </tr>
            </table>

            <?php
            $weight = 0;
            $volume = 0;
            $volumeWeight = 0;
            if(!empty($wsResults)){
            foreach ($wsResults as $result) { ?>
                <table border="1" class="invoice_inner_table invoice_table" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="130"><input type="checkbox" name="remove[]" value="<?php echo $result->id; ?>"/>
                            <input type="hidden" name="id[]" value="<?php echo $result->id?>" class="td89"/>
                            <input type="text" name="pcs[]" value="<?php echo $result->pcs; ?>" class="td89"/></td>
                        <td><input type="text" name="weight[]" value="<?php echo $result->weight; ?>" class="td89"/></td>
                        <td width="197"><input type="text" name="dims[]" value="<?php echo $result->dims;?>"/></td>
                        <td width="197"><input type="text" name="lwh[]" value="<?php echo $result->lwh; ?>"/></td>
                        <td><input type="text" name="volume[]" value="<?php echo $result->volume; ?>" class="td89"/>CBM</td>
                        <td><input type="text" name="volumeweight[]" value="<?php echo $result->volumeweight;?>"
                                   class="td89"/></td>
                    </tr>
                </table>

                <?php
                $weight += $result->weight;
                $volume += $result->volume;
                $volumeWeight += $result->volumeweight;
            } }?>
            <div id="more"></div>
            <table border="1" class="invoice_inner_table invoice_table" id="invoice_description" cellpadding="0"
                   cellspacing="0">
                <tr>
                    <td width="118"><strong style="float:right;">Total Weight</strong></td>
                    <td width="118"><strong><?php echo  $weight;?></strong></td>
                    <td width="180"><strong></strong></td>
                    <td width="180"><strong style="float:right;">Total Volume</strong></td>
                    <td width="121"><strong><?php echo $volume; ?> CBM</strong></td>
                    <td width="115"><strong><?php  $volumeWeight;?></strong></td>
                </tr>
            </table>
            <table class="invoice_inner_table invoice_table" id="invoice_description" cellpadding="0" cellspacing="0">
                <tr>
                    <td><input type="button" id="add_more" value="ADD MORE"/>&nbsp;
                        <input type="submit" value="UPDATE" name="save"/>
                        <br/>
                        <a target="_blank" href="<?php echo base_url('invoice/pdf_weight_slip?invoiceId='. $this->uri->segment(3))?>">PREVIEW PDF</a>
                    </td>

                </tr>
            </table>

            </form>

        </div>
    </div>
</div>

<script language="javascript">
    $(document).ready(function() {
        $("#add_more").click(function() {
            $("#more").append('<table  border="1" class="invoice_inner_table invoice_table" cellpadding="0" cellspacing="0"><tr><td><input type="hidden" name="id[]" value="0" /><input type="text" name="pcs[]" class="td89" /></td><td><input type="text" name="weight[]" class="td89" /></td><td width="197"><input type="text" name="dims[]"/></td><td width="197"><input type="text" name="lwh[]" /></td><td><input type="text" name="volume[]" class="td89" /></td><td><input type="text" name="volumeweight[]" class="td89" /></td></tr></table>');
        });
        $("#date").datepicker({dateFormat: 'dd-mm-yy'});
    });

</script>
