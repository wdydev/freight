<div class="row">
    <div class="row col-md-12">
        <div class="block">
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li><a href="<?php echo base_url('invoice/edit/' . $this->uri->segment(3)); ?>">Edit Invoice</a></li>
                    <?php if (!empty($detail) &&($detail[0]->account_type == "Air")){ ?>
                        <li><a href="<?php echo base_url('invoice/edit_air_freight/' . $this->uri->segment(3)); ?>">Edit
                                Air Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_airfreight/' . $this->uri->segment(3)); ?>">Edit Booking(Air)</a></li>
                    <?php } else { ?>
                        <li><a href="<?php echo base_url('invoice/edit_sea_freight/' . $this->uri->segment(3)); ?>">Edit Sea
                                Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_seafreight/' . $this->uri->segment(3)); ?>">Edit Booking(Sea)</a></li>
                    <?php } ?>
                    <li><a href="<?php echo base_url('invoice/edit_acceptance/' . $this->uri->segment(3)); ?>">Edit
                            Acceptance</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_credit_note/' . $this->uri->segment(3)); ?>">Edit
                            Credit Note</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_airwaybill/' . $this->uri->segment(3)); ?>">Edit
                            AirwayBill</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_weight_slip/' . $this->uri->segment(3)); ?>">Edit
                            Weight Slip</a></li>
                    <li class="active"><a href="<?php echo base_url('invoice/inventory_list/' . $this->uri->segment(3)); ?>">Inventory List</a></li>
                    <li><a href="<?php echo base_url('invoice/email/' . $this->uri->segment(3)); ?>">Email</a></li>
                </ul>
            </div>

            <div class="col-md-12">
                <div class="col-md-6"><img src="<?php echo base_url('assets/img/logo_03.png'); ?>"/></div>
                <div class="col-md-3" style="float:right;text-align:right;">Unit-3,61 Crooks Road<br/>East Tamaki,
                    Auckland, NZ<br/>PO
                    Box: 76877 Manukau City-2241<br/>Phone: 09-263 7439 / 2151601<br/>Fax: 09-263 7438
                </div>

            </div>
            <div class="clearfix"></div>
            <br/>

            <form class="form-horizontal form-bordered" action="" method="post">

                <div class="form-group">
                    <label class="col-md-2 control-label" for="shipperName">Inventory List for : </label>

                    <div class="col-md-4">
                        <input type="text" id="shipperName" name="shipperName" class="form-control"
                               value="<?php echo (!empty($ilResults)?$ilResults[0]->shipperName:""); ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" for="address">Address : </label>

                    <div class="col-md-4">
                        <input type="text" id="address" name="address" class="form-control"
                               value="<?php echo (!empty($ilResults)?$ilResults[0]->address:""); ?>"/>
                    </div>
                </div>


                <br/><br/>
                <table border='1' width="602px;" cellpadding='20' class='invoice_table' cellspacing='20'
                       style='border-collapse:collapse; border:1px solid #999;margin-left:50px;'>

                    <tr style='background:#000;color:#fff;'>
                        <td width="100px">Serial No.</td>
                        <td width="">Description</td>

                    </tr>
                </table>


                <?php $j = 0;
                if(!empty($wrResults)){
                foreach ($wsResults as $result) {
                    $j++; ?>
                    <table border="1" class="invoice_table" cellpadding="0" cellspacing="0" style="margin-left:50px;">
                        <tr>

                            <td width="100px"><?php echo $j ?>.&nbsp;&nbsp;<input type="checkbox" name="remove[]"
                                                                                  value="<?php echo $result->id; ?>"/>
                                <input type="hidden" name="descriptionId[]" value="<?php echo $result->id; ?>"/>
                            </td>
                            <td><input type="text" name="description[]" style="width:500px;"
                                       value="<?php echo $result->description; ?>"/></td>

                        </tr>
                    </table>


                <?php } } ?>
                <div id="more"></div>
                </table>


                <input type="hidden" name="j" id="j" value="<?php echo $j; ?>"/>

                <div class="form-group">
                    <label class="col-md-2 control-label" for="noPackages">Total No. of packages </label>

                    <div class="col-md-3">
                        <input type="text" id="noPackages" name="noPackages" class="form-control"
                               value="<?php echo (!empty($ilResults)?$ilResults[0]->noPackages:""); ?>"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label" for="valueOfGoods">Value for Goods </label>

                    <div class="col-md-3">
                        <input type="text" id="valueOfGoods" name="valueOfGoods" class="form-control"
                               value="<?php echo (!empty($ilResults)?$ilResults[0]->valueOfGoods:""); ?>"/>
                    </div>
                </div>

                <br/>
                <table class="invoice_table" id="invoice_description" cellpadding="0" cellspacing="0"
                       style="margin-left:50px;">
                    <tr>
                        <td><input type="button" id="add_more" value="ADD MORE"/>&nbsp;
                            <input type="submit" value="UPDATE" name="save"/>
                            <br/><br/>
                            <a target="_blank" href="<?php echo base_url('invoice/pdf_inventory_list?invoiceId='.$this->uri->segment(3));?>" >PREVIEW PDF</a>
                        </td>

                    </tr>
                </table>

            </form>
        </div>
    </div>
</div>

<script language="javascript">
    $(document).ready(function () {
        $("#add_more").click(function () {
            var j = parseInt($("#j").val()) + 1;
            $("#j").val(j);
            $("#more").append('<table  border="1" class="invoice_table" cellpadding="0" cellspacing="0" style="margin-left:50px;"><tr><td width="100">' + j + '.<input type="hidden" name="descriptionId[]" value="0" /></td><td><input type="text" name="description[]" style="width:500px;" /></td></tr></table>');
        });
    });
</script>