<div class="row">
    <div class="row col-md-12">
        <div class="block">
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li><a href="<?php echo base_url('invoice/edit/' . $this->uri->segment(3)); ?>">Edit Invoice</a></li>
                    <?php if (!empty($detail) &&($detail[0]->account_type == "Air")){ ?>
                        <li><a href="<?php echo base_url('invoice/edit_air_freight/' . $this->uri->segment(3)); ?>">Edit
                                Air Freight</a></li>
                        <li class="active"><a href="<?php echo base_url('invoice/edit_booking_airfreight/' . $this->uri->segment(3)); ?>">Edit Booking(Air)</a></li>
                    <?php } else { ?>
                        <li><a href="<?php echo base_url('invoice/edit_sea_freight/' . $this->uri->segment(3)); ?>">Edit Sea
                                Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_seafreight/' . $this->uri->segment(3)); ?>">Edit Booking(Sea)</a></li>
                    <?php } ?>
                    <li><a href="<?php echo base_url('invoice/edit_acceptance/' . $this->uri->segment(3)); ?>">Edit
                            Acceptance</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_credit_note/' . $this->uri->segment(3)); ?>">Edit
                            Credit Note</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_airwaybill/' . $this->uri->segment(3)); ?>">Edit
                            AirwayBill</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_weight_slip/' . $this->uri->segment(3)); ?>">Edit
                            Weight Slip</a></li>
                    <li><a href="<?php echo base_url('invoice/inventory_list/' . $this->uri->segment(3)); ?>">Inventory List</a></li>
                    <li><a href="<?php echo base_url('invoice/email/' . $this->uri->segment(3)); ?>">Email</a></li>

                </ul>
            </div>


            <table class="invoice_table table" cellpadding="0" cellspacing="0">
                <tr>
                    <td><img src="<?php echo base_url('assets/img/logo_03.png'); ?>"/></td>
                    <td>Divine Logistics Ltd<br/>Unit-3,61 Crooks Road<br/>East Tamaki, Auckland, NZ<br/>POBox:
                        76877<br/>Manukau City 2241, Auckland
                    </td>
                    <td>Office: 09 2637439, 09 2151601<br/>Mobile: 02102494202<br/>After Hours: 09-2673201<br/>Email
                        :info@divinelogistics.co.nz<br/>Web :www.divinelogistics.co.nz
                    </td>
                </tr>
            </table>
            <table border='0' cellspacing='0' class='invoice_table table' cellpadding='0'
                   style='border-collapse:collapse;'>
                <tr style='background:#999; color:#fff; font-size:25px;'>
                    <td>Sea Freight LCL Departure Booking Confirmation</td>
                </tr>
            </table>
            <form name="invoice" class="form-horizontal form-bordered" action=" " method="post">
                <input type="hidden" name="invoiceId" value="<?php echo $this->uri->segment(3);?>" />

                <table border='0' cellspacing='0' class='invoice_table table' cellpadding='0'
                       style='border-collapse:collapse;'>
                    <tr>
                        <td colspan="2">Bill TO<br/>
                            <textarea name="billTo"
                                      rows="5"><?php echo(!empty($rsResults) ? $rsResults[0]->billTO : "") ?></textarea>
                        </td>
                        <td colspan="2">
                            <table border="0" align="right" class="table">
                                <tr>
                                    <td>SHIPMENT</td>
                                    <td>
                                        SOO<input type="text" name="shipmentNo" class="form-control"
                                                  value="<?php echo (!empty($rsResults) ? $rsResults[0]->shipmentNo : "");?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>CONSOL</td>
                                    <td><input type="text" name="consol" class="form-control"
                                               value="<?php echo(!empty($rsResults) ? $rsResults[0]->consol : "") ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>DATE</td>
                                    <td><input type="text" name="billDate" id="billDate"
                                               class="form-control form-control datepicker"
                                               value="<?php echo(!empty($rsResults) ? date("d-m-Y", strtotime($rsResults[0]->billDate)) : date("d-m-Y", time())) ?>"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style='background:#999; color:#fff;' class="table" >
                        <td colspan='5' >SHIPMENT DETAILS</td>
                    </tr>
                    <tr style='background:#000; color:#fff;'class="table">
                        <td colspan="2">CONSIGNOR</td>
                        <td colspan="3" >CONSIGNEE</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <select name="consignorClientId">
                                <option selected disabled>Please Select</option>
                                <?php $clients = $this->admin_model->table_fetch_rows('clients', array(), array('name' => 'asc'));
                                foreach ($clients as $c) {
                                    if ($rsResults[0]->consignorClientId == ($c->id)) {
                                        ?>
                                        <option value="<?php echo $c->id; ?>"
                                                selected="selected"><?php echo $c->name; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $c->id; ?>"><?php echo $c->name; ?></option>
                                    <?php }
                                } ?>
                            </select><br/>
                            <textarea name="consignor" rows="3"><?php echo (!empty($rsResults) ? $rsResults[0]->consignor : " ");?></textarea></td>
                        <td colspan="3"><textarea name="consignee"
                                                  rows="3"><?php echo (!empty($rsResults) ? $rsResults[0]->consignee : " ");?></textarea></td>
                    </tr>
                    <tr style='background:#000; color:#fff;'>
                        <td colspan="2">EXPORT RECEIVING DEPOT</td>
                        <td>CONSIGNOR'S REFERENCE</td>
                        <td colspan="2">CARRIER BOOKING REFERENCE</td>
                    </tr>
                    <tr>
                        <td rowspan="5" colspan="2"><textarea name="exportReceivingDepot"
                                                              rows="3"><?php echo (!empty($rsResults) ? $rsResults[0]->exportReceivingDepot: " "); ?></textarea><br/>
                            Phone: <input type="text" name="phone" class="form-control"
                                          value="<?php echo (!empty($rsResults) ? $rsResults[0]->phone : "") ?>"/>
                            Fax: <input type="text" name="fax" class="form-control"
                                        value="<?php echo (!empty($rsResults) ? $rsResults[0]->fax : "") ?>"/>
                        </td>
                        <td><input type="text" name="consignorRef" class="form-control"
                                   value="<?php echo (!empty($rsResults) ? $rsResults[0]->consignorRef : "") ?>"/></td>
                        <td colspan="2"><input type="text" name="carrierBookingRef" class="form-control"
                                               value="<?php echo (!empty($rsResults) ? $rsResults[0]->carrierBookingRef : "") ?>"/>
                        </td>
                    </tr>
                    <tr style='background:#000; color:#fff;'>
                        <td colspan="3">GOODS DESCRIPTION</td>
                    </tr>
                    <tr>
                        <td colspan="3"><input type="text" name="carrierBookingRef" class="form-control"
                                               value="<?php echo (!empty($rsResults) ? $rsResults[0]->carrierBookingRef : "") ?>"/>
                        </td>
                    </tr>
                    <tr style='background:#000; color:#fff;'>
                        <td colspan="3">COMMODITY SUMMARY</td>
                    </tr>
                    <tr>
                        <td colspan="3"><input type="text" name="carrierBookingRef" class="form-control"
                                               value="<?php echo (!empty($rsResults) ? $rsResults[0]->carrierBookingRef : "") ?>"/>
                        </td>
                    </tr>
                    <tr style='background:#000; color:#fff;'>
                        <td>Ocean Bill of Lading</td>
                        <td>WEIGHT</td>
                        <td>VOLUME</td>
                        <td>CHARGEABLE</td>
                        <td>PACKAGES</td>
                    </tr>
                    <tr>
                        <td><input type="text" name="oceanBillOfLading" class="form-control"
                                   value="<?php echo (!empty($rsResults) ? $rsResults[0]->oceanBillOfLading : "") ?>"/></td>
                        <td><input type="text" name="weight" class="form-control"
                                   value="<?php echo (!empty($rsResults) ? $rsResults[0]->weight : "") ?>"/></td>
                        <td><input type="text" name="volume" class="form-control"
                                   value="<?php (!empty($rsResults) ? $rsResults[0]->volume : "") ?>"/></td>
                        <td><input type="text" name="chargeable" class="form-control"
                                   value="<?php echo (!empty($rsResults) ? $rsResults[0]->chargeable : "") ?>"/></td>
                        <td><input type="text" name="packages" class="form-control"
                                   value="<?php echo (!empty($rsResults) ? $rsResults[0]->packages : "") ?>"/></td>
                    </tr>
                    <tr style='background:#000; color:#fff;'>
                        <td>GOODS COLLECTED FROM</td>
                        <td>ETD</td>
                        <td colspan="2">GOODS DELIVERED TO</td>
                        <td>ETA</td>
                    </tr>
                    <tr>
                        <td><input type="text" name="goodsCollectedFrom" class="form-control"
                                   value="<?php echo (!empty($rsResults) ? $rsResults[0]->goodsCollectedFrom : "") ?>"/></td>
                        <td><input type="text" name="etdBooking" class="form-control datepicker"
                                   value="<?php echo (!empty($rsResults) ? date("d-m-Y", strtotime($rsResults[0]->etd)) : "") ?>"/>
                        </td>
                        <td><input type="text" name="goodsDeliveredTo" class="form-control"
                                   value="<?php echo (!empty($rsResults) ? $rsResults[0]->goodsDeliveredTo : "") ?>"/></td>
                        <td><input type="text" name="etaBooking" class="form-control datepicker"
                                   value="<?php echo (!empty($rsResults) ? date("d-m-Y", strtotime($rsResults[0]->eta)) : "") ?>"/>
                        </td>
                    </tr>
                    <tr style='background:#000; color:#fff;'>
                        <td colspan="2">ORDER NUMBERS / OWNER'S REFERENCE</td>
                        <td colspan="3">EXPORT CUSTOMS BROKER</td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="text" name="orderNumber_Ref" class="form-control"
                                               value="<?php echo (!empty($rsResults) ? $rsResults[0]->orderNumber_Ref : "") ?>"/>
                        </td>
                        <td colspan="3"><input type="text" name="exportCustomsBroker" class="form-control"
                                               value="<?php echo (!empty($rsResults) ? $rsResults[0]->exportCustomsBroker : "") ?>"/>
                        </td>
                    </tr>
                    <tr style='background:#000; color:#fff;'>
                        <td colspan="5">CARRIER</td>
                    </tr>
                    <tr>
                        <td colspan="5"><input type="text" name="Carrier" class="form-control"
                                               value="<?php echo (!empty($rsResults) ? $rsResults[0]->Carrier : "") ?>"/>
                        </td>
                    </tr>
                </table>

                <table class='invoice_table table' cellpadding='0' cellspacing='0'>
                    <tr style='background:#000; color:#fff;'>
                        <td width='80' class="bold">Mode</td>
                        <td width='200' class="bold">Vessel/Voyage/IMO</td>
                        <td width='100' class="bold">Carrier</td>
                        <td width='100' class="bold">Load</td>
                        <td width='100' class="bold">Disch.</td>
                        <td width='100' class="bold">ETD</td>
                        <td width='100' class="bold">ETA</td>
                    </tr>
                </table>
                <?php $weight = 0;
                $volume = 0;
                if(!empty($wsResults)){
                    foreach($wsResults as $result){ ?>
                        <table border='1'class="invoice_inner_table invoice_table table" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="80">
                                    <input type="hidden" name="id[]" value="<?php echo(!empty($result) ? $result->id : "") ?>"
                                           class="form-control"/>
                                    <input type="text" name="mode[]" value="<?php echo(!empty($result) ? $result->mode : "") ?>"
                                           class="form-control"/>
                                </td>
                                <td width="200">
                                    <input type="text" name="vessel_Voyage[]"
                                           value="<?php echo(!empty($result) ? $result->vessel_Voyage : "") ?>"
                                           class="form-control"/>/<input type="text" name="IMO[]" class="form-control"
                                                                         value="<?php echo $result->IMO ?>"/>
                                </td>
                                <td><input type="text" name="carrier[]"
                                           value="<?php echo(!empty($result) ? $result->carrier : "") ?>"
                                           class="form-control"/></td>
                                <td><input type="text" name="load[]" value="<?php echo(!empty($result) ? $result->load : "") ?>"
                                           class="form-control"/></td>
                                <td><input type="text" name="disch[]"
                                           value="<?php echo(!empty($result) ? $result->disch : ""); ?>"
                                           class="form-control"/></td>
                                <td><input type="text" class="datepicker" name="etd[]"
                                           value="<?php echo date("d-m-Y", strtotime($result->etd)) ?>"
                                           class="form-control"/></td>
                                <td><input type="text" class="datepicker" name="eta[]"
                                           value="<?php echo date("d-m-Y", strtotime($result->eta)) ?>"
                                           class="form-control"/></td>
                            </tr>
                        </table>

                        <?php
                        //$weight += $result['weight']; $volume += $result['volume'];
                    } } ?>
                <div id="more"></div>
                <br/>

                </td>
                </tr>
                <tr>
                    <td colspan='4' class="bold">
                        Yours Sincerely,<br/>
                        <textarea name="yourSincerely" rows="3"><?php echo (!empty($rsResults) ? $rsResults[0]->yourSincerely : "");?></textarea>
                    </td>
                </tr>
                </table>
                <br/><br/>
                <table class="invoice_inner_table invoice_table table" id="invoice_description" cellpadding="0"
                       cellspacing="0">
                    <tr>
                        <td><input type="button" id="add_more" value="ADD MORE"/>&nbsp;
                            <input type="submit" value="UPDATE" name="save"/> <br/>
                            <a target="_blank" href="<?php echo base_url('invoice/pdf_booking_airfreight?id='.$this->uri->segment(3)); ?>">PREVIEW
                                PDF</a>
                        </td>
                    </tr>
                </table>

            </form>
            </form>

        </div>
    </div>
</div>

<script language="javascript">
    $(document).ready(function() {
        $("#add_more").click(function() {
            $("#more").append('<table class="invoice_inner_table invoice_table table" cellpadding="0" cellspacing="0"><tr>	<td>	<input type="hidden" name="id[]" value="0"  class="form-control" />			<input type="text" name="mode[]" class="form-control" />			</td>		<td>				<input type="text"  name="vessel_Voyage[]" value="" class="form-control" />/<input type="text" name="IMO[]" class="" value="" class="form-control" />					</td>			<td><input type="text" name="carrier[]" value="" class="form-control" /></td>			<td><input type="text" name="load[]" value="" class="form-control" /></td>		<td><input type="text" name="disch[]" value=""  class="form-control" /></td>		<td><input type="text" class="datepicker" name="etd[]" value="" class="form-control" /></td>			<td><input type="text" class="datepicker" name="eta[]" value="" class="form-control" /></td>		 </tr>			</table>');
            $(".datepicker").datepicker({dateFormat: 'dd-mm-yy'});
        });
    });
</script>
