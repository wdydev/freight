<div class="row">
    <div class="row col-md-12">
        <div class="block">
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="<?php echo base_url('invoice/edit/' . $this->uri->segment(3)); ?>">Edit
                            Invoice</a></li>
                    <?php if (!empty($query) &&($query[0]->account_type == "Air")){ ?>
                        <li><a href="<?php echo base_url('invoice/edit_air_freight/' . $this->uri->segment(3)); ?>">Edit
                                Air Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_airfreight/' . $this->uri->segment(3)); ?>">Edit Booking(Air)</a></li>
                    <?php } else { ?>
                        <li><a href="<?php echo base_url('invoice/edit_sea_freight/' . $this->uri->segment(3)); ?>">Edit Sea
                                Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_seafreight/' . $this->uri->segment(3)); ?>">Edit Booking(Sea)</a></li>
                    <?php } ?>
                    <li><a href="<?php echo base_url('invoice/edit_acceptance/' . $this->uri->segment(3)); ?>">Edit
                            Acceptance</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_credit_note/' . $this->uri->segment(3)); ?>">Edit
                            Credit Note</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_airwaybill/' . $this->uri->segment(3)); ?>">Edit
                            AirwayBill</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_weight_slip/' . $this->uri->segment(3)); ?>">Edit
                            Weight Slip</a></li>
                    <li><a href="<?php echo base_url('invoice/inventory_list/' . $this->uri->segment(3)); ?>">Inventory
                            List</a></li>
                    <li><a href="<?php echo base_url('invoice/email/' . $this->uri->segment(3)); ?>">Email</a></li>
                </ul>
            </div>
            <div class="col-md-12">
                <div class="col-md-6"><img src="<?php echo base_url('assets/img/logo_03.png'); ?>"/></div>
                <div class="col-md-3">Divine Logistics Ltd<br/>Unit-3,12 Lambie Drive,<br/>Manukau Central, Auckland, NZ<br/>POBox:
                    76877<br/>Manukau City 2241, Auckland
                </div>
                <div class="col-md-3">Office: 09 2637439, 09 2151601<br/>Mobile: 02102494202<br/>After Hours: 09-2673201<br/>Email
                    :info@divinelogistics.co.nz<br/>Web :www.divinelogistics.co.nz
                </div>
            </div>
            <div class="clearfix"></div>
            <br/>

            <?php if (isset($message)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
                <?php
            } ?>

            <?php if (validation_errors()) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <?php
                    echo validation_errors();
                    ?>
                </div>
            <?php } ?>


            <form action="" method="post" class="form-horizontal form-bordered">
                <div class="block col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="invoice_number">Tax Invoice</label>

                            <div class="col-md-6">
                                <?php $details = $this->admin_model->table_fetch_rows('invoices', array(), array('id' => 'desc')); ?>
                                <input type="text" id="invoice_number" name="invoice_number" class="form-control"
                                       value="<?php echo(!empty($query) ? $query[0]->invoice_number : ""); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="GST">GST #:</label>

                            <div class="col-md-6">
                                102498070
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="client_name">Client name</label>

                            <div class="col-md-6">
                                <select id="clientId" name="clientId" class="form-control" size="1">
                                    <option selected disabled>Please Select</option>
                                    <?php $clients = $this->admin_model->table_fetch_rows('clients', array(), array('name' => 'asc'));
                                    foreach ($clients as $c) {
                                        if (($query[0]->clientId) == ($c->id)) {
                                            ?>
                                            <option value="<?php echo $c->id; ?>"
                                                    selected="selected"><?php echo $c->name; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $c->id; ?>"><?php echo $c->name; ?></option>
                                        <?php }
                                    } ?>
                                </select> OR
                                <input type="text" id="client_name" name="client_name" class="form-control"
                                       value="<?php echo(!empty($query) ? ($query[0]->client_name != '' ? $query[0]->client_name : '') : ""); ?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="client_address">Client Address</label>

                            <div class="col-md-6">
                    <textarea id="client_address" rows="3" cols="10" name="client_address" class="form-control"
                              style="width: 322px; height: 99px;"><?php echo(!empty($query) ? $query[0]->client_address : ""); ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="account_type">Account</label>

                            <div class="col-md-6">
                                <select id="account_type" name="account_type" class="form-control" size="1">
                                    <option
                                        value="Air" <?php echo(!empty($query) ? ($query[0]->account_type == "Air" ? "selected" : "") : ""); ?>>
                                        Air
                                    </option>
                                    <option
                                        value="Sea" <?php echo(!empty($query) ? ($query[0]->account_type == "Sea" ? "selected" : "") : ""); ?>>
                                        Sea
                                    </option>
                                    <option
                                        value="Personal" <?php echo(!empty($query) ? ($query[0]->account_type == "Personal" ? "selected" : "") : ""); ?>>
                                        Personal
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="invoice_date">Invoice Date</label>

                            <div class="col-md-6">
                                <input type="text" id="invoice_date" name="invoice_date"
                                       class="form-control input-datepicker" data-date-format="yyyy-mm-dd"
                                       value="<?php echo(!empty($query) ? $query[0]->invoice_date : ""); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="due_date">Due Date</label>

                            <div class="col-md-6">
                                <input type="text" id="due_date" name="due_date" class="form-control input-datepicker"
                                       data-date-format="yyyy-mm-dd"
                                       value="<?php echo(!empty($query) ? $query[0]->due_date : ""); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="terms">Terms</label>

                            <div class="col-md-6">
                                <input type="text" id="terms" name="terms" class="form-control"
                                       value="<?php echo(!empty($query) ? $query[0]->terms : ""); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="shipment">Shipment</label>

                            <div class="col-md-6">
                                <input type="text" id="shipment" name="shipment" class="form-control"
                                       value="<?php echo(!empty($query) ? $query[0]->shipment : ""); ?>">
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="consignor">Consignor</label>

                                <div class="col-md-8">
                                    <input type="text" id="consignor" name="consignor" class="form-control"
                                           value="<?php echo(!empty($query) ? $query[0]->consignor : ""); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="order_reference">Client/Order
                                    Reference</label>

                                <div class="col-md-8">
                                    <input type="text" id="order_reference" name="order_reference" class="form-control"
                                           value="<?php echo(!empty($query) ? $query[0]->order_reference : ""); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="consignee">Consignee</label>

                                <div class="col-md-8">
                                    <input type="text" id="consignee" name="consignee" class="form-control"
                                           value="<?php echo(!empty($query) ? $query[0]->consignee : ""); ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="goods_description">Goods Description</label>

                                <div class="col-md-6">
                                    <textarea id="goods_description" rows="3" cols="10" name="goods_description"
                                              class="form-control"
                                              style="width: 322px; height: 99px;"
                                              placeholder="Enter Goods Description"><?php echo(!empty($query) ? $query[0]->goods_description : ""); ?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="LCL_FCL">LCF/FCL</label>

                                <div class="col-md-6">
                                    <select id="LCL_FCL" name="LCL_FCL" class="form-control" size="1">
                                        <option
                                            value="" <?php echo(!empty($query) ? ($query[0]->LCL_FCL == "" ? "selected" : "") : ""); ?>>
                                            None
                                        </option>
                                        <option
                                            value="LCL" <?php echo(!empty($query) ? ($query[0]->LCL_FCL == "LCL" ? "selected" : "") : ""); ?>>
                                            LCL
                                        </option>
                                        <option
                                            value="FCL" <?php echo(!empty($query) ? ($query[0]->LCL_FCL == "FCL" ? "selected" : "") : ""); ?>>
                                            FCL
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="packages">Packages</label>

                                <div class="col-md-8">
                                    <input type="text" id="packages" name="packages" class="form-control"
                                           value="<?php echo(!empty($query) ? $query[0]->packages : ""); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="weight">Weight</label>

                                <div class="col-md-8">
                                    <input type="text" id="weight" name="weight" class="form-control"
                                           value="<?php echo(!empty($query) ? $query[0]->weight : ""); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="volume">Volume</label>

                                <div class="col-md-8">
                                    <input type="text" id="volume" name="volume" class="form-control"
                                           value="<?php echo(!empty($query) ? $query[0]->volume : ""); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="chargeable">Chargeable</label>

                                <div class="col-md-8">
                                    <input type="text" id="chargeable" name="chargeable" class="form-control"
                                           value="<?php echo(!empty($query) ? $query[0]->chargeable : ""); ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="flight_name" id="flight_vesel">Flight &
                                    Date</label>

                                <div class="col-md-4">
                                    <input type="text" id="flight_vesel_name" name="flight_name" class="form-control"
                                           value="<?php echo(!empty($query) ? $query[0]->flight_name : ""); ?>">
                                </div>
                                <div class="col-md-4">
                                    <input type="text" id="flight_date" name="flight_date"
                                           class="form-control input-datepicker" data-date-format="yyyy-mm-dd"
                                           value="<?php echo(!empty($query) ? $query[0]->flight_date : ""); ?>">
                                </div>

                            </div>
                        </div>

                        <div class="col-md-3 values">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="mawb">MAWB</label>

                                <div class="col-md-8">
                                    <input type="text" id="mawb" name="mawb" class="form-control"
                                           value="<?php echo(!empty($query) ? $query[0]->mawb : ""); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 values1">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="container_number">Container Number</label>

                                <div class="col-md-8">
                                    <input type="text" id="container_number" name="container_number"
                                           class="form-control"
                                           value="<?php echo(!empty($query) ? $query[0]->container_number : ""); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 values">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="hawb">HAWB</label>

                                <div class="col-md-8">
                                    <input type="text" id="hawb" name="hawb" class="form-control"
                                           value="<?php echo(!empty($query) ? $query[0]->hawb : ""); ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="origin">Origin</label>

                                <div class="col-md-8">
                                    <input type="text" id="origin" name="origin" class="form-control"
                                           value="<?php echo(!empty($query) ? $query[0]->origin : ""); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="etd">ETD</label>

                                <div class="col-md-8">
                                    <input type="text" id="etd" name="etd"
                                           class="form-control input-datepicker" data-date-format="yyyy-mm-dd"
                                           value="<?php echo(!empty($query) ? $query[0]->etd : ""); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="destination">Destination </label>

                                <div class="col-md-8">
                                    <input type="text" id="destination" name="destination" class="form-control"
                                           value="<?php echo(!empty($query) ? $query[0]->destination : ""); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="eta">ETA</label>

                                <div class="col-md-8">
                                    <input type="text" id="eta" name="eta"
                                           class="form-control input-datepicker" data-date-format="yyyy-mm-dd"
                                           value="<?php echo(!empty($query) ? $query[0]->eta : ""); ?>">
                                </div>
                            </div>
                        </div>
                    </div>


                    <table class="invoice_table invoice_inner_table col-md-12 table" id="invoice_description"
                               cellpadding="0" cellspacing="0">
                            <tr style='background:#000; color:#fff;'>
                                <td width="60%"><strong>Description</strong></td>
                                <td width="20%"><strong>GST in NZD</strong></td>
                                <td width="20%"><strong>Charges in NZD</strong> <a href="javascript:void();"
                                                                                   id="add_more"><button type="button" class="btn btn-sm btn-primary">More</button></a></td>
                            </tr>
                            <?php
                            $gst = 0;
                            $sub_total = 0;
                            if (is_array($query) && !empty($query)) {
                                $i = 1;
                                $result = $this->admin_model->invoiceDescription($query[0]->id);
                                foreach ($result as $res) {
                                    if (is_array($res) && !empty($res)) {
                                        $gst = $gst + ($res['gst'] == 15 ? (($res['charges'] * 15) / 100) : 0);
                                        $sub_total += $res['charges'];
                                        ?>

                                        <tr class="description_wrap">
                                            <td width="60%"><input type="hidden" class="form-control"
                                                                   name="descriptionId[]"
                                                                   value="<?php echo $res['id']; ?>"/>
                                                <div>
                                                <div class="col-sm-1"><input type="checkbox" class="form-control" name="remove[]"
                                                       value="<?php echo $res['id']; ?>" style="width: 3px;"/></div>

                                                <div style="margin-left:56px;"><input type="text" size="70" class="form-control" id="description_1"
                                                       name="description[]" value="<?php echo $res['description']; ?>" style="width:500px;"/></div>
                                                </div>
                                            </td>
                                            <td width="20%">
                                                <select name="gst_nzd[]" class="form-control"
                                                        id="gst_nzd_<?php echo $i; ?>">
                                                    <option value="zero">Zero Rated</option>
                                                    <option value="15" <?php echo($res['gst'] ? 'SELECTED' : ''); ?>>15
                                                        %
                                                    </option>
                                                </select>
                                            </td>
                                            <td width="20%"><input type="text" id="charges_nzd_<?php echo $i; ?>"
                                                                   name="charges_nzd[]" class="charges_nzd form-control"
                                                                   value="<?php echo $res['charges']; ?>"/></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }

                            } ?>

                        </table>

                    <table class="invoice_table invoice_inner_table table" id="invoice_description" cellpadding="0"
                           cellspacing="0">
                        <tr>
                            <td width="87%">Please contact us within 7 days should there</td>
                            <td width="40%"><label>Sub Total:</label>
                                <span id="sub_total"><?php echo round($sub_total, 2); ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="70%">by any discrepancies</td>
                            <td width="40%"><label>Add Gst:</label>
                                <span id="gst_total"><?php echo round($gst, 2); ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="70%">
                                <select name="contact_enum" id="contact_enum" class="form-control" style="width:100px;">
                                    <option
                                        value="1" <?php echo(!empty($query) ? ($query[0]->contact_enum == "1" ? "selected" : "") : ""); ?>>
                                        Yes
                                    </option>
                                    <option
                                        value="0" <?php echo(!empty($query) ? ($query[0]->contact_enum == "0" ? "selected" : "") : ""); ?>>
                                        No
                                    </option>
                                </select><br/><br/>Custom GST?
                                <select name="customGST" id="customGST" class="form-control" style="width:100px;">
                                    <option
                                        value="0" <?php echo(!empty($query) ? ($query[0]->customGST == "0" ? "selected" : "") : ""); ?>>
                                        No
                                    </option>
                                    <option
                                        value="1" <?php echo(!empty($query) ? ($query[0]->customGST == "1" ? "selected" : "") : ""); ?>>
                                        Yes
                                    </option>
                                </select>

                            </td>
                            <td width="40%"><label>Total NZD:</label>
                                <span id="total_amount"><?php echo round($sub_total + $gst, 2); ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="70%"></td>
                            <td width="40%"><label>Credit:</label>
                                <input type="text" class="form-control" name="discount" id="discount"
                                       value="<?php echo(!empty($query) ? $query[0]->discount : ""); ?>" size="5"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="70%"></td>
                            <td width="40%"><label>Final Total NZD:</label>
                                <span
                                    id="fina_total_amount"><?php echo(!empty($query) ? round(($sub_total + $gst) - $query[0]->discount, 2) : ""); ?></span>
                            </td>
                        </tr>
                    </table>
                    <table class="invoice_table table">
                        <tr>
                            <td width="65%">

                                <p>&nbsp;</p>

                                <p><i>Transfer Funds To:</i></p>

                                <p>Bank Details :</p>

                                <p><label>Bank </label>&emsp;&emsp; 011804</p>

                                <p><label>Account </label>&emsp;&emsp; 0151562-00 ANZ</p>

                                <p><label>Swift </label>&emsp;&emsp; ANZ BNZ22</p>
                            </td>
                            <td width="10%">&nbsp;</td>
                            <td width="45%">
                                <p>Divine Logistics Ltd. <br/> P.O Box - 76877 <br/> Manukau City - 2241 <br/> Manukau
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3"><p>**All shipments subject to our standard trading conditions, copies
                                    available on request or may be downloaded from our web site.**</p></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <?php
                                if (is_array($query) && !empty($query)) {
                                    if ($query[0]->account_type == "Air") {
                                        $result = $this->admin_model->booking_airFreight($query[0]->id);
                                    } else {
                                        $result = $this->admin_model->booking_seaFreight($query[0]->id);
                                    }
                                    if (is_array($result) && !empty($result)) {
                                        if (($result[0]['id'] != NULL) && ($result[0]['id'] > 0)) {
                                            ?>
                                            <br/>
                                            <br/>Booking Module done for this Invoice. Please select when you complete your invoice?
                                            <select name="bookingFlag" id="bookingFlag" class="form-control">
                                                <option
                                                    value="N" <?php echo(!empty($query) ? ($query[0]->bookingFlag == "N" ? "selected" : "") : ""); ?>>
                                                    No
                                                </option>
                                                <option
                                                    value="Y" <?php echo(!empty($query) ? ($query[0]->bookingFlag == "Y" ? "selected" : "") : ""); ?>>
                                                    Yes
                                                </option>
                                            </select>
                                            <?php
                                        }
                                    }
                                }
                                ?>

                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i
                                        class="fa fa-check"></i> Save
                                </button>
                                <br/>
                                <!--                                    <a  target="_blank" href="pdf.php?id=-->
                                <? //=$_GET['edit']?><!--">PREVIEW INVOICE PDF</a><br/>-->
                                <a target="_blank"
                                   href="<?php echo base_url('invoice/pdf_authority_letter?invoiceId=' . $this->uri->segment(3)); ?>">PREVIEW
                                    AUTHORITY LETTER PDF</a><br/>
                            </td>
                        </tr>
                    </table>


                </div>


        </div>
    </div>
</div>


<script language="javascript">
    $(document).ready(function () {
        $("#account_type").change(function () {
            var val = $(this).val();
            if (val == 'Air') {
                $(".values1").hide();
                $(".values").show();
                $("#flight_vesel").html('Flight & Date:');
                $("#flight_vesel_name").attr('placeholder', 'Flight');
                $("#flight_date").show();
            } else {
                $(".values1").show();
                $(".values").hide();
                $("#flight_vesel").html('Vessel / Voyage');
                $("#flight_vesel_name").attr('placeholder', 'Vessel / Voyage');
                $("#flight_date").hide();
            }
        });

        $("#add_more").click(function () {
            next_item = parseInt($(".description_wrap").length) + 1;
            $("#invoice_description").append('<tr class="description_wrap"><td width="60%"><input type="hidden" class="form-control" name="descriptionId[]" value="0" /><div style="margin-left:56px;"><input type="text" class="form-control" id="description_' + next_item + '" size="70" name="description[]" value="" style="width:500px;"/></div></td><td width="20%"><select class="form-control" id="gst_nzd_' + next_item + '" name="gst_nzd[]"><option value="zero">Zero Rated</option><option value="15">15 %</option></select></td><td width="20%"><input type="text" id="charges_nzd_' + next_item + '" name="charges_nzd[]" class="charges_nzd form-control" value="" /></td></tr>');

            $(".charges_nzd").keyup(function () {
                var total = 0;
                $(".charges_nzd").each(function () {
                    var val = $(this).val();
                    console.log(val);
                    total = parseFloat(total) + parseFloat(val);
                });
                $("#sub_total").html(total);
                calculateGST();
            });

            $("select[name='gst_nzd[]']").change(function () {
                calculateGST();
            });

        });

        $("#shipment, #order_reference").val($("#invoice_number").val());
        $("#invoice_number").keyup(function () {
            $("#shipment, #order_reference").val($("#invoice_number").val());
        });

        $(".charges_nzd").keyup(function () {
            var total = 0;
            $(".charges_nzd").each(function () {
                var val = $(this).val();
                console.log(val);
                total = parseFloat(total) + parseFloat(val);
            });
            $("#sub_total").html(total);
            calculateGST();
        });
        $("#discount").keyup(function () {
            calculateGST();
        });

        $("select[name='gst_nzd[]']").change(function () {
            calculateGST();
        });
        $("#account_type").trigger('change');
    });


    function calculateGST() {
        var gst = 0;
        $("select[name='gst_nzd[]']").each(function () {
            var val = $(this).val();
            if (val != 'zero') {
                id = $(this).attr('id').split('_');
                amount = $("#charges_nzd_" + id[2]).val();
                gst = gst + ((amount * parseInt(val)) / 100);
            }
            $("#gst_total").html(gst);
            $("#total_amount").html(parseFloat($("#gst_total").html()) + parseFloat($("#sub_total").html()));
            $("#fina_total_amount").html(parseFloat($("#total_amount").html()) - parseFloat($("#discount").val()));
        });
    }


</script>
