<div class="row">
    <div class="row col-md-12">
        <div class="block">
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li><a href="<?php echo base_url('invoice/edit/' . $this->uri->segment(3)); ?>">Edit Invoice</a></li>
                    <?php if (!empty($detail) &&($detail[0]->account_type == "Air")){ ?>
                        <li><a href="<?php echo base_url('invoice/edit_air_freight/' . $this->uri->segment(3)); ?>">Edit
                                Air Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_airfreight/' . $this->uri->segment(3)); ?>">Edit Booking(Air)</a></li>
                    <?php } else { ?>
                        <li><a href="<?php echo base_url('invoice/edit_sea_freight/' . $this->uri->segment(3)); ?>">Edit Sea
                                Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_seafreight/' . $this->uri->segment(3)); ?>">Edit Booking(Sea)</a></li>
                    <?php } ?>
                    <li class="active" ><a href="<?php echo base_url('invoice/edit_acceptance/' . $this->uri->segment(3)); ?>">Edit
                            Acceptance</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_credit_note/' . $this->uri->segment(3)); ?>">Edit
                            Credit Note</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_airwaybill/' . $this->uri->segment(3)); ?>">Edit
                            AirwayBill</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_weight_slip/' . $this->uri->segment(3)); ?>">Edit
                            Weight Slip</a></li>
                    <li><a href="<?php echo base_url('invoice/inventory_list/' . $this->uri->segment(3)); ?>">Inventory List</a></li>
                    <li><a href="<?php echo base_url('invoice/email/' . $this->uri->segment(3)); ?>">Email</a></li>
                </ul>
            </div>

            <?php if (isset($message)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
                <?php
            } ?>

            <div class="col-md-12">
                <div class="col-md-6"><img src="<?php echo base_url('assets/img/logo_03.png'); ?>"/></div>
                <div class="col-md-3" style="float:right;text-align: right;">Unit-3,61 Crooks Road<br/>East Tamaki, Auckland, NZ<br/>PO
                    Box: 76877 Manukau City-2241<br/>Phone: 09-263 7439 / 2151601<br/>Fax: 09-263 7438
                </div>
            </div>
            <div class="clearfix"></div><br /><br /><br />

            <div><b>Direct shippers Freight Acceptance Questionnaire</b><br/><br/></div>
            <div><p style='line-height:150%;'>The following details are required to assist in determining whether the provisions for the carriage
                of Dangerous goods are applicable to the despatch of your consignment. <br/>
                Please circle YES or No as appropriate</p></div><br/>

                    <form action="" method="post" class="form-horizontal form-bordered">
                        <table border='1' cellpadding='20' class='spacingtd table' cellspacing='20' style='width:1000px;border-collapse:collapse; border:1px solid #999;'>
                        <input type="hidden" name="invoiceId" value="<?php echo $this->uri->segment(3);?>" />
                        <tr>
                            <td colspan='2'>ARE ANY OF THE FOLLOWING ITEMS INCLUDED IN THE CONSIGNMENT BEING PRESENTED?</td>
                            <td class='noborderleft'>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td width='20'>1</td>
                            <td>Fireworks,ammunition,sporting firearms or explosives</td>
                            <td width='45'><input type="radio" name="acc_1" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]['acc_1']=="Y")?"checked":"" ?> />YES</td>
                            <td width='45'><input type="radio" name="acc_1" value="N" <?php echo (!empty($rsResults) && $rsResults[0]['acc_1']=="N")?"checked":"" ?> />NO</td>
                        </tr>
                        <tr>
                            <td width='20'>2</td>
                            <td>Cylinders of compressed air,oxygenor liquid petrolium gas(LPG)any type of aeroso</td>
                            <td width='45'><input type="radio" name="acc_2" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]['acc_2']=="Y")?"checked":"" ?> />YES</td>
                            <td width='45'><input type="radio" name="acc_2" value="N" <?php echo (!empty($rsResults) && $rsResults[0]['acc_2']=="N")?"checked":"" ?> />NO</td>
                        </tr>
                        <tr>
                            <td width='20'></td>
                            <td>canEg(Deodorant shaving cream hairspray paintetc</td>
                            <td width='45'><input type="radio" name="acc_2a" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]['acc_2a']=="Y")?"checked":"" ?> />YES</td>
                            <td width='45'><input type="radio" name="acc_2a" value="N" <?php echo (!empty($rsResults) && $rsResults[0]['acc_2a']=="N")?"checked":"" ?> />NO</td>
                        </tr>
                        <tr>
                            <td width='20'>3</td>
                            <td>Comping stoves, cigerrette or pipe lighters or cigarrette lighter refills</td>
                            <td width='45'><input type="radio" name="acc_3" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]['acc_3']=="Y")?"checked":"" ?> />YES</td>
                            <td width='45'><input type="radio" name="acc_3" value="N" <?php echo (!empty($rsResults) && $rsResults[0]['acc_3']=="N")?"checked":"" ?> />NO</td>
                        </tr>
                        <tr>
                            <td width='20'>4</td>
                            <td>Nail polish, colognes,perfumes,paint,fuel,or paintthinners</td>
                            <td width='45'><input type="radio" name="acc_4" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]['acc_4']=="Y")?"checked":"" ?> />YES</td>
                            <td width='45'><input type="radio" name="acc_4" value="N" <?php echo (!empty($rsResults) && $rsResults[0]['acc_4']=="N")?"checked":"" ?> />NO</td>
                        </tr>
                        <tr>
                            <td width='20'>5</td>
                            <td>Matches</td>
                            <td width='45'><input type="radio" name="acc_5" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]['acc_5']=="Y")?"checked":"" ?> />YES</td>
                            <td width='45'><input type="radio" name="acc_5" value="N" <?php echo (!empty($rsResults) && $rsResults[0]['acc_5']=="N")?"checked":"" ?> />NO</td>
                        </tr>
                        <tr>
                            <td width='20'>6</td>
                            <td>Dry Ice,specimens or samples</td>
                            <td width='45'><input type="radio" name="acc_6" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]['acc_6']=="Y")?"checked":"" ?> />YES</td>
                            <td width='45'><input type="radio" name="acc_6" value="N" <?php echo (!empty($rsResults) && $rsResults[0]['acc_6']=="N")?"checked":"" ?> />NO</td>
                        </tr>
                        <tr>
                            <td width='20'>7</td>
                            <td>Detergents, bleaches, drain or oven cleaners</td>
                            <td width='45'><input type="radio" name="acc_7" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]['acc_7']=="Y")?"checked":"" ?> />YES</td>
                            <td width='45'><input type="radio" name="acc_7" value="N" <?php echo (!empty($rsResults) && $rsResults[0]['acc_7']=="N")?"checked":"" ?> />NO</td>
                        </tr>
                        <tr>
                            <td width='20'>8</td>
                            <td>Wet cell batteries</td>
                            <td width='45'><input type="radio" name="acc_8" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]['acc_8']=="Y")?"checked":"" ?> />YES</td>
                            <td width='45'><input type="radio" name="acc_8" value="N" <?php echo (!empty($rsResults) && $rsResults[0]['acc_8']=="N")?"checked":"" ?> />NO</td>
                        </tr>
                        <tr>
                            <td width='20'>9</td>
                            <td>Fiberglass repair kits, adheves and puncture repair kits</td>
                            <td width='45'><input type="radio" name="acc_9" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]['acc_9']=="Y")?"checked":"" ?> />YES</td>
                            <td width='45'><input type="radio" name="acc_9" value="N" <?php echo (!empty($rsResults) && $rsResults[0]['acc_9']=="N")?"checked":"" ?> />NO</td>
                        </tr>
                        <tr>
                            <td width='20'>10</td>
                            <td>Medicines containing alcohol</td>
                            <td width='45'><input type="radio" name="acc_10" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]['acc_10']=="Y")?"checked":"" ?> />YES</td>
                            <td width='45'><input type="radio" name="acc_10" value="N" <?php echo (!empty($rsResults) && $rsResults[0]['acc_10']=="N")?"checked":"" ?> />NO</td>
                        </tr>
                        <tr>
                            <td width='20'>11</td>
                            <td>Camping equipment</td>
                            <td width='45'><input type="radio" name="acc_11" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]['acc_11']=="Y")?"checked":"" ?> />YES</td>
                            <td width='45'><input type="radio" name="acc_11" value="N" <?php echo (!empty($rsResults) && $rsResults[0]['acc_11']=="N")?"checked":"" ?> />NO</td>
                        </tr>
                        <tr>
                            <td width='20'>12</td>
                            <td>Diving equipment</td>
                            <td width='45'><input type="radio" name="acc_12" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]['acc_12']=="Y")?"checked":"" ?> />YES</td>
                            <td width='45'><input type="radio" name="acc_12" value="N" <?php echo (!empty($rsResults) && $rsResults[0]['acc_12']=="N")?"checked":"" ?> />NO</td>
                        </tr>
                        <tr>
                            <td width='20'>13</td>
                            <td>Anytype of chemical, pesticides,herbicides etc</td>
                            <td width='45'><input type="radio" name="acc_13" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]['acc_13']=="Y")?"checked":"" ?> />YES</td>
                            <td width='45'><input type="radio" name="acc_13" value="N" <?php echo (!empty($rsResults) && $rsResults[0]['acc_13']=="N")?"checked":"" ?> />NO</td>
                        </tr>
                        <tr>
                            <td width='20'>14</td>
                            <td>Machiner with internal comustion engines such as lawn mowers garden trimmers or chainsaws</td>
                            <td width='45'><input type="radio" name="acc_14" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]['acc_14']=="Y")?"checked":"" ?> />YES</td>
                            <td width='45'><input type="radio" name="acc_14" value="N" <?php echo (!empty($rsResults) && $rsResults[0]['acc_14']=="N")?"checked":"" ?> />NO</td>
                        </tr>

                    </table><br />
                   <div></div><p style='line-height:200%; font-size:10px;'><b style='font-size:10px;'>PLEASE NOTE: </b>The Civil Aviation Authority CAA has developed legeslation in regard to the management of Dangerous Goods
                        as airfreight. Under sub regulation 262B of the Civil Aviation Act, amended in 1992 the following could be applicable;
                        Heavy penalties,including imprisonment apply to shippers who do not declare dangerous goods in any item to an airline
                        IF YOU ARE UNSURE THAT ANY ITEM YOU ARE CONSIGNING COULD BE CONSIDERED DANGEROUS GOODS,PLEASE
                        ASK OUR STAFF FOR ASSISTANCE</p><div></div>
                    <table border='0' class='spacingtd'  cellpadding='10' cellspacing='0'>
                        <tr>
                            <td colspan='2' height='50'><b>SHIPPERS NAME</b> <span style='font-size:18px;'><input type="text" name="shipperName" class="form-control" value="<?php echo (!empty($rsResults) ? $rsResults[0]['shipperName'] : " ");?>" /></span></td>
                        </tr>
                        <tr>
                            <td width='350'><b>SHIPPERS SIGNATURE</b></td>
                            <td width='350'><b>DATE</b> <input type="text" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" name="date" id="date" value="<?php echo (!empty($rsResults)?((($rsResults[0]['date']=='')||$rsResults[0]['date'] == '0000-00-00')?date('Y-m-d') :$rsResults[0]['date']):"") ;?>" /></td>
                        </tr>
                    </table>
                    <table class="invoice_inner_table invoice_table" id="invoice_description" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i class="fa fa-check"></i> Update</button>
                                <a target="_blank" href="<?php echo base_url('invoice/pdf_acceptance?invoiceId='.$this->uri->segment(3));?>">PREVIEW PDF</a></td>
                        </tr>
                    </table>
                    <br/><br/><br/>
                    Airline personnel only to complete<br/><br/>
                    <div style='border-top:1px solid #999;height:25px;padding-top:5px;'>Airway Bill number</div>
                    <div style='border-top:1px solid #999;height:25px;padding-top:5px;'>Additional information</div>
                    <div style='border-top:1px solid #999;height:25px;padding-top:5px;'>&nbsp;</div>
                    <div style='border-top:1px solid #999;height:25px;'>&nbsp;</div>
                    </form>


            </div>
        </div>
    </div>