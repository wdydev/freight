
<div class="row">
    <div class="row col-md-12">
        <div class="block">
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li><a href="<?php echo base_url('invoice/add/'); ?>">Add Invoice</a></li>
                    <li class="active"><a href="<?php echo base_url('invoice/view/');?>">Invoice
                            List</a></li>
                </ul>
            </div>

            <?php if (isset($message)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
                <?php
            } ?>

            <?php if (validation_errors()) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <?php
                    echo validation_errors();
                    ?>
                </div>
            <?php } ?>


            <form action="" method="post" class="form-horizontal form-bordered">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="clientId">Search By Client name</label>

                    <div class="col-md-4">
                        <select id="clientId" name="clientId" class="form-control" size="1">
                            <option selected disabled>Please Select</option>
                            <?php $clients = $this->admin_model->table_fetch_rows('clients', array(), array('name' => 'asc'));
                            foreach ($clients as $c) {
                                if($search_value['id'] == ($c->id)){?>
                                    <option value="<?php echo $c->id;?>" selected="selected"><?php echo $c->name;?></option>
                                    <?php  }else {?>
                                    <option value="<?php echo $c->id;?>"><?php echo $c->name;?></option>
                                <?php } }?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="invoiceNo">Invoice No.</label>

                    <div class="col-md-4">
                        <input type="text" id="invoiceNo" name="invoiceNo" class="form-control"
                               placeholder="Enter Invoice number" value="<?php echo ($search_value['invoiceNo']!=NULL)?$search_value['invoiceNo']:'';?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="containerNo">Container No.</label>
                    <div class="col-md-4">
                        <input type="text" id="containerNo" name="containerNo" class="form-control"
                               placeholder="Enter Container number" value="<?php echo ($search_value['containerNo']!=NULL)?$search_value['containerNo']:'';?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="date">Date</label>
                    <div class="col-md-4">
                        <input name="date" id="date" class="form-control input-datepicker" type="text" data-date-format="yyyy-mm" value="<?php echo ($search_value['date']!=NULL)?$search_value['date']:'';?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="customGST">Include Custom GST</label>
                    <div class="col-md-4">
                        <select id="customGST" name="customGST" class="form-control" size="1">
                            <option value="1" <?php echo ($search_value['customGST']=="1"?"selected":"");?>>Yes</option>
                            <option value="0" <?php echo ($search_value['customGST']=="0"?"selected":"");?>>No</option>
                        </select>
                    </div>
                </div>


                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i
                                class="fa fa-search search"></i> Search
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="block full">

        <div class="table-responsive">
            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 100px;">Serial No</th>
                    <th>Invoice No.</th>
                    <th>Acc Type</th>
                    <th>Container No.</th>
                    <th>LCL/FCL</th>
                    <th>Invoice Date</th>
                    <th>Client Name</th>
                    <th>Total Amount</th>
                    <th>CR</th>
                    <th>B</th>
                    <th>Email</th>
                    <th class="text-center" style="width: 125px;"><i class="fa fa-flash"></i></th>
                </tr>
                </thead>
                <tbody>
                <?php $grand_total = 0;
                $grand_total1 = 0;
                $credit_total = 0;
                $balance_total = 0;
                if (is_array($query[0]) && !empty($query[0])) {
                    foreach ($query[0] as $k => $row) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $k + 1; ?></td>
                            <td><?php echo $row['invoice_number']; ?></td>
                            <td><?php echo $row['account_type']; ?></td>
                            <td><?php echo $row['container_number']; ?></td>
                            <td><?php echo $row['LCL_FCL']; ?></td>
                            <td><?php echo date("d M, Y", strtotime($row['invoice_date'])) ?></td>
                            <td><?php echo $row['clientName']; ?></td>
                            <td align="right">
                            <?php $gst = 0;
                            $total = 0;
                            $result = $this->admin_model->invoiceDescription($row['id']);
                        foreach ($result as $res) {
                            $gst = ($res['gst'] == 15 ? (($res['charges'] * 15) / 100) : 0);
                            $total = $total + $res['charges'] + $gst;
                        }
                        $total = $total-$row['discount'];
                        $grand_total += $total;
                        $balance_total += $total-$row['discount'];
                        $credit_total += $row['credit_note'];
                        echo $total;
                        ?>
                                <input type="hidden" value="<?php echo $total; ?>" name="total" id="total_<?php echo $row['id']; ?>" />
                            </td>
                            <td align="right">
                                <?php
                                $result = $this->admin_model->creditNoteTotal($row['id']);
                                $gst = 0;
                                $total = 0;
                        foreach ($result as $res) {
                            $gst =  ($res['gst'] == 15 ? (($res['charges'] * 15) / 100) : 0);
                            $total = $total + $res['charges'] + $gst;
                        }
                                $total = $total-$row['discount'];
                                $grand_total1 += $total;
                                $balance_total += $total-$row['discount'];
                                $credit_total += $row['credit_note'];
                                echo sprintf('%0.2f',$total);
                                ?>
                                <input type="hidden" value="<?php echo $total; ?>" name="total" id="total_<?php echo $row['id']; ?>" />
                            </td>

                            <td>
                                <?php
                                if ($row['account_type']=="Air"){
                                    $result = $this->admin_model->booking_airFreight($row['id']);
                                }else{
                                    $result = $this->admin_model->booking_seaFreight($row['id']);
                                }
                                if (!empty($result) && ($result['id'] > 0)){
                                    if ($row['bookingFlag']=="N"){ ?>
                                        <img src="<?php echo base_url('assets/img/delete.png');?>" />;
                                    <?php }else{ ?>
                                        <img src="<?php echo base_url('assets/img/green_button.png');?>" />;
                                    <?php }
                                }
                                ?>
                            </td>
                            <td><?php echo ($row['email'] == '1') ? 'Sent' : 'Not Sent'; ?></td>

                            <td class="text-center">
                                <a href="<?php echo base_url('invoice/edit/' . $row['id']); ?>" data-toggle="tooltip"
                                   title="edit_invoice" class="btn btn-effect-ripple btn-xs btn-success"><i
                                        class="fa fa-pencil"></i></a>
                                <a href="#" data-href="<?php echo base_url('invoice/delete/' . $row['id']); ?>"
                                   data-toggle="modal" data-name="<?php //echo $row->name;
                                ?>" title="delete_invoice" data-target="#confirm-delete"
                                   class="btn btn-effect-ripple btn-xs btn-danger del-row"><i
                                        class="fa fa-times"></i></a>
                            </td>
                </tr>
                <?php } } ?>
                <tr>
                    <td colspan="7" align="right">Grand Total: </td>
                    <td align="right"><?php echo $grand_total; ?></td>
                    <td align="right"><?php echo $grand_total1; ?></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="7" align="right">Balance: </td>
                    <td align="right" colspan='2'><?php echo ($grand_total - $grand_total1); ?></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </div>
        <a target="_blank"
           href="<?php echo base_url('invoice/pdf_invoice_list?clientId=' . $search_value['id'] . '&invoiceNo=' . $search_value['invoiceNo'] .'&containerNo=' . $search_value['containerNo'] . '&date=' . $search_value['date'].  '&customGST=' . $search_value['customGST']); ?>">View
            PDF</a>


    <h4><b>Invoice List for Custom GST</b></h4>
    <?php
    $grand_total = 0;
    $grand_total1 = 0;
    $credit_total = 0;
    $balance_total = 0;
    ?>
        <div class="table-responsive">
            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 100px;">Serial No</th>
                    <th>Invoice No.</th>
                    <th>Acc Type</th>
                    <th>Container No.</th>
                    <th>LCL/FCL</th>
                    <th>Invoice Date</th>
                    <th>Client Name</th>
                    <th>Total Amount</th>
                    <th>CR</th>
                    <th>B</th>
                    <th>Email</th>
                    <th class="text-center" style="width: 125px;"><i class="fa fa-flash"></i></th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (is_array($query[1]) && !empty($query[1])) {
                    foreach ($query[1] as $k => $row) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $k + 1; ?></td>
                            <td><?php echo $row['invoice_number']; ?></td>
                            <td><?php echo $row['account_type']; ?></td>
                            <td><?php echo $row['container_number']; ?></td>
                            <td><?php echo $row['LCL_FCL']; ?></td>
                            <td><?php echo date("d M, Y", strtotime($row['invoice_date'])) ?></td>
                            <td><?php echo $row['clientName']; ?></td>
                            <td align="right">
                                <?php $gst = 0;
                                $total = 0;
                                $result = $this->admin_model->invoiceDescription($row['id']);
                                foreach ($result as $res) {
                                    $gst = ($res['gst'] == 15 ? (($res['charges'] * 15) / 100) : 0);
                                    $total = $total + $res['charges'] + $gst;
                                }
                                $total = $total-$row['discount'];
                                $grand_total += $total;
                                $balance_total += $total-$row['discount'];
                                $credit_total += $row['credit_note'];
                                echo $total;
                                ?>
                                <input type="hidden" value="<?php echo $total; ?>" name="total" id="total_<?php echo $row['id']; ?>" />
                            </td>
                            <td align="right">
                                <?php
                                $result = $this->admin_model->creditNoteTotal($row['id']);
                                $gst = 0;
                                $total = 0;
                                foreach ($result as $res) {
                                    $gst =  ($res['gst'] == 15 ? (($res['charges'] * 15) / 100) : 0);
                                    $total = $total + $res['charges'] + $gst;
                                }
                                $total = $total-$row['discount'];
                                $grand_total1 += $total;
                                $balance_total += $total-$row['discount'];
                                $credit_total += $row['credit_note'];
                                echo sprintf('%0.2f',$total);
                                ?>
                                <input type="hidden" value="<?php echo $total; ?>" name="total" id="total_<?php echo $row['id']; ?>" />
                            </td>

                            <td>
                                <?php
                                if ($row['account_type']=="Air"){
                                    $result = $this->admin_model->booking_airFreight($row['id']);
                                }else{
                                    $result = $this->admin_model->booking_seaFreight($row['id']);
                                }
                                if(is_array($result) && !empty($result)) {
                                    if (($result[0]['id'] != NULL) && ($result[0]['id'] > 0)) {
                                        if ($row['bookingFlag'] == "N") { ?>
                                            <img src="<?php echo base_url('assets/img/delete.png'); ?>"/>
                                        <?php } else { ?>
                                            <img src="<?php echo base_url('assets/img/green_button.png'); ?>"/>
                                        <?php }
                                    }
                                }
                                ?>
                            </td>
                            <td><?php echo ($row['email'] == '1') ? 'Sent' : 'Not Sent'; ?></td>

                            <td class="text-center">
                                <a href="<?php echo base_url('invoice/edit/' . $row['id']); ?>" data-toggle="tooltip"
                                   title="edit_invoice" class="btn btn-effect-ripple btn-xs btn-success"><i
                                        class="fa fa-pencil"></i></a>
                                <a href="#" data-href="<?php echo base_url('invoice/delete/' . $row['id']); ?>"
                                   data-toggle="modal" data-name="<?php //echo $row->name;
                                ?>" title="delete_invoice" data-target="#confirm-delete"
                                   class="btn btn-effect-ripple btn-xs btn-danger del-row"><i
                                        class="fa fa-times"></i></a>
                            </td>
                        </tr>
                    <?php } } ?>
                <tr>
                    <td colspan="7" align="right">Grand Total: </td>
                    <td align="right"><?php echo $grand_total; ?></td>
                    <td align="right"><?php echo $grand_total1; ?></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="7" align="right">Balance: </td>
                    <td align="right" colspan='2'><?php echo ($grand_total - $grand_total1); ?></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </div>
        <a target="_blank"
           href="<?php echo base_url('invoice/pdf_invoice_list?clientId=' . $search_value['id'] . '&invoiceNo=' . $search_value['invoiceNo'] .'&containerNo=' . $search_value['containerNo'] . '&date=' . $search_value['date']. '&customGST=' . $search_value['customGST'].'&customYN=1'); ?>">View
            PDF</a>

    <h4><b>Reminders for Air Freight</b></h4>
    <?php
    $result = $this->admin_model->booking_airFreightDetails();
    $grand_total = 0;
    $grand_total1 = 0;
    $credit_total = 0;
    $balance_total = 0;
    ?>

        <div class="table-responsive">
            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 100px;">Serial No</th>
                    <th>Invoice No.</th>
                    <th>Acc Type</th>
                    <th>Container No.</th>
                    <th>LCL/FCL</th>
                    <th>Invoice Date</th>
                    <th>Client Name</th>
                    <th>Total Amount</th>
                    <th>CR</th>
                    <th>B</th>
                    <th>Email</th>
                    <th class="text-center" style="width: 125px;"><i class="fa fa-flash"></i></th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($result as $k => $row) {
                ?>
                <tr>
                    <td class="text-center"><?php echo $k + 1; ?></td>
                    <td><?php echo $row['invoice_number']; ?></td>
                    <td><?php echo $row['account_type']; ?></td>
                    <td><?php echo $row['container_number']; ?></td>
                    <td><?php echo $row['LCL_FCL']; ?></td>
                    <td><?php echo date("d M, Y", strtotime($row['invoice_date'])) ?></td>
                    <td><?php echo $row['clientName']; ?></td>
                    <td align="right">
                        <?php $gst = 0;
                        $total = 0;
                        $result = $this->admin_model->invoiceDescription($row['id']);
                        foreach ($result as $res) {
                            $gst = ($res['gst'] == 15 ? (($res['charges'] * 15) / 100) : 0);
                            $total = $total + $res['charges'] + $gst;
                        }
                        $total = $total-$row['discount'];
                        $grand_total += $total;
                        $balance_total += $total-$row['discount'];
                        $credit_total += $row['credit_note'];
                        echo $total;
                        ?>
                        <input type="hidden" value="<?php echo $total; ?>" name="total" id="total_<?php echo $row['id']; ?>" />
                    </td>
                    <td align="right">
                        <?php
                        $result = $this->admin_model->creditNoteTotal($row['id']);
                        $gst = 0;
                        $total = 0;
                        foreach ($result as $res) {
                            $gst =  ($res['gst'] == 15 ? (($res['charges'] * 15) / 100) : 0);
                            $total = $total + $res['charges'] + $gst;
                        }
                        $total = $total-$row['discount'];
                        $grand_total1 += $total;
                        $balance_total += $total-$row['discount'];
                        $credit_total += $row['credit_note'];
                        echo sprintf('%0.2f',$total);
                        ?>
                        <input type="hidden" value="<?php echo $total; ?>" name="total" id="total_<?php echo $row['id']; ?>" />
                    </td>

                    <td>
                        <?php
                        if ($row['account_type']=="Air"){
                            $result = $this->admin_model->booking_airFreight($row['id']);
                        }else{
                            $result = $this->admin_model->booking_seaFreight($row['id']);
                        }
                        if ($result['id'] > 0){
                            if ($row['bookingFlag']=="N"){ ?>
                                <img src="<?php echo base_url('assets/img/delete.png');?>" />;
                            <?php }else{ ?>
                                <img src="<?php echo base_url('assets/img/green_button.png');?>" />;
                            <?php }
                        }
                        ?>
                    </td>
                    <td><?php echo ($row['email'] == '1') ? 'Sent' : 'Not Sent'; ?></td>

                    <td class="text-center">
                        <a href="<?php echo base_url('invoice/edit/' . $row['id']); ?>" data-toggle="tooltip"
                           title="edit_invoice" class="btn btn-effect-ripple btn-xs btn-success"><i
                                class="fa fa-pencil"></i></a>
                        <a href="#" data-href="<?php echo base_url('invoice/delete/' . $row['id']); ?>"
                           data-toggle="modal" data-name="<?php //echo $row->name;
                        ?>" title="delete_invoice" data-target="#confirm-delete"
                           class="btn btn-effect-ripple btn-xs btn-danger del-row"><i
                                class="fa fa-times"></i></a>
                    </td>
                </tr>
                <?php }  ?>
                <tr>
                    <td colspan="7" align="right">Grand Total: </td>
                    <td align="right"><?php echo $grand_total; ?></td>
                    <td align="right"><?php echo $grand_total1; ?></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="7" align="right">Balance: </td>
                    <td align="right" colspan='2'><?php echo ($grand_total - $grand_total1); ?></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </div>
        <a target="_blank"
           href="<?php echo base_url('invoice/pdf_invoice_list?clientId=' . $search_value['id'] . '&invoiceNo=' . $search_value['invoiceNo'] .'&containerNo=' . $search_value['containerNo'] . '&date=' . $search_value['date']. '&customGST=' . $search_value['customGST'].'&customYN=1'); ?>">View
            PDF</a>

        <h4><b>Reminders for Sea Freight</b></h4>
        <?php
        $result = $this->admin_model->booking_seaFreightDetails();
        $grand_total = 0;
        $grand_total1 = 0;
        $credit_total = 0;
        $balance_total = 0;
        ?>

        <div class="table-responsive">
            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 100px;">Serial No</th>
                    <th>Invoice No.</th>
                    <th>Acc Type</th>
                    <th>Container No.</th>
                    <th>LCL/FCL</th>
                    <th>Invoice Date</th>
                    <th>Client Name</th>
                    <th>Total Amount</th>
                    <th>CR</th>
                    <th>B</th>
                    <th>Email</th>
                    <th class="text-center" style="width: 125px;"><i class="fa fa-flash"></i></th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($result as $k => $row) {
                    ?>
                    <tr>
                        <td class="text-center"><?php echo $k + 1; ?></td>
                        <td><?php echo $row['invoice_number']; ?></td>
                        <td><?php echo $row['account_type']; ?></td>
                        <td><?php echo $row['container_number']; ?></td>
                        <td><?php echo $row['LCL_FCL']; ?></td>
                        <td><?php echo date("d M, Y", strtotime($row['invoice_date'])) ?></td>
                        <td><?php echo $row['clientName']; ?></td>
                        <td align="right">
                            <?php $gst = 0;
                            $total = 0;
                            $result = $this->admin_model->invoiceDescription($row['id']);
                            foreach ($result as $res) {
                                $gst = ($res['gst'] == 15 ? (($res['charges'] * 15) / 100) : 0);
                                $total = $total + $res['charges'] + $gst;
                            }
                            $total = $total-$row['discount'];
                            $grand_total += $total;
                            $balance_total += $total-$row['discount'];
                            $credit_total += $row['credit_note'];
                            echo $total;
                            ?>
                            <input type="hidden" value="<?php echo $total; ?>" name="total" id="total_<?php echo $row['id']; ?>" />
                        </td>
                        <td align="right">
                            <?php
                            $result = $this->admin_model->creditNoteTotal($row['id']);
                            $gst = 0;
                            $total = 0;
                            foreach ($result as $res) {
                                $gst =  ($res['gst'] == 15 ? (($res['charges'] * 15) / 100) : 0);
                                $total = $total + $res['charges'] + $gst;
                            }
                            $total = $total-$row['discount'];
                            $grand_total1 += $total;
                            $balance_total += $total-$row['discount'];
                            $credit_total += $row['credit_note'];
                            echo sprintf('%0.2f',$total);
                            ?>
                            <input type="hidden" value="<?php echo $total; ?>" name="total" id="total_<?php echo $row['id']; ?>" />
                        </td>

                        <td>
                            <?php
                            if ($row['account_type']=="Air"){
                                $result = $this->admin_model->booking_airFreight($row['id']);
                            }else{
                                $result = $this->admin_model->booking_seaFreight($row['id']);
                            }
                            if ($result['id'] > 0){
                                if ($row['bookingFlag']=="N"){ ?>
                                    <img src="<?php echo base_url('assets/img/delete.png');?>" />;
                                <?php }else{ ?>
                                    <img src="<?php echo base_url('assets/img/green_button.png');?>" />;
                                <?php }
                            }
                            ?>
                        </td>
                        <td><?php echo ($row['email'] == '1') ? 'Sent' : 'Not Sent'; ?></td>

                        <td class="text-center">
                            <a href="<?php echo base_url('invoice/edit/' . $row['id']); ?>" data-toggle="tooltip"
                               title="edit_invoice" class="btn btn-effect-ripple btn-xs btn-success"><i
                                    class="fa fa-pencil"></i></a>
                            <a href="#" data-href="<?php echo base_url('invoice/delete/' . $row['id']); ?>"
                               data-toggle="modal" data-name="<?php //echo $row->name;
                            ?>" title="delete_invoice" data-target="#confirm-delete"
                               class="btn btn-effect-ripple btn-xs btn-danger del-row"><i
                                    class="fa fa-times"></i></a>
                        </td>
                    </tr>
                <?php }  ?>
                <tr>
                    <td colspan="7" align="right">Grand Total: </td>
                    <td align="right"><?php echo $grand_total; ?></td>
                    <td align="right"><?php echo $grand_total1; ?></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="7" align="right">Balance: </td>
                    <td align="right" colspan='2'><?php echo ($grand_total - $grand_total1); ?></td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </div>
        <a target="_blank"
           href="<?php echo base_url('invoice/pdf_invoice_list?clientId=' . $search_value['id'] . '&invoiceNo=' . $search_value['invoiceNo'] .'&containerNo=' . $search_value['containerNo'] . '&date=' . $search_value['date']. '&customGST=' . $search_value['customGST'].'&customYN=1'); ?>">View
            PDF</a>
    </div>

    <div id="confirm-delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><span id="del_name"></span></h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this record?
                </div>
                <div class="modal-footer">

                    <a class="btn btn-effect-ripple btn-danger">Delete</a>
                    <button type="button" data-dismiss="modal" class="btn btn-effect-ripple btn-default"
                            data-dismiss="modal">Cancel
                    </button>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(function () {
            $('a.del-row').click(function (e) {
                var name = $(this).attr('data-name');
                $('span#del_name').html(name);
            });
        });

        $('#confirm-delete').on('show.bs.modal', function (e) {
            $(this).find('.btn-danger').attr('href', $(e.relatedTarget).data('href'));
        });

//        $(function() {
//            $(".search").trigger("click");
//        });

    </script>



