<style type="text/css">
    .bold{
        font-weight: bold;
    }
</style>
<div class="row">
    <div class="row col-md-12">
        <div class="block">
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li><a href="<?php echo base_url('invoice/edit/' . $this->uri->segment(3)); ?>">Edit Invoice</a></li>
                    <?php if (!empty($detail) &&($detail[0]->account_type == "Air")){ ?>
                    <li class="active"><a href="<?php echo base_url('invoice/edit_air_freight/' . $this->uri->segment(3)); ?>">Edit
                            Air Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_airfreight/' . $this->uri->segment(3)); ?>">Edit Booking(Air)</a></li>
                    <?php } else { ?>
                    <li><a href="<?php echo base_url('invoice/edit_sea_freight/' . $this->uri->segment(3)); ?>">Edit Sea
                            Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_seafreight/' . $this->uri->segment(3)); ?>">Edit Booking(Sea)</a></li>
                    <?php } ?>
                    <li><a href="<?php echo base_url('invoice/edit_acceptance/' . $this->uri->segment(3)); ?>">Edit
                            Acceptance</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_credit_note/' . $this->uri->segment(3)); ?>">Edit
                            Credit Note</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_airwaybill/' . $this->uri->segment(3)); ?>">Edit
                            AirwayBill</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_weight_slip/' . $this->uri->segment(3)); ?>">Edit
                            Weight Slip</a></li>
                    <li><a href="<?php echo base_url('invoice/inventory_list/' . $this->uri->segment(3)); ?>">Inventory List</a></li>
                    <li><a href="<?php echo base_url('invoice/email/' . $this->uri->segment(3)); ?>">Email</a></li>
                </ul>
            </div>

            <?php if (isset($message)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
            <?php } ?>

            <form action="" method="post" class="form-horizontal form-bordered">
                <input type="hidden" name="invoiceId" value="<?php echo $this->uri->segment(3);?>" />

                <table cellspacing='0' class='invoice_table table' cellpadding='0'>
                    <tr>
                        <td style="padding: 0;">
                            <table class="table"  cellpadding='0' cellspacing='0'>
                                <tr>
                                    <td class="bold">SHIPPER:</td>
                                </tr>
                                <tr>
                                    <td height='50'>
                                        <select id="clientId" name="clientId" class="form-control" size="1">
                                            <option selected disabled>Please Select</option>
                                            <?php $clients = $this->admin_model->table_fetch_rows('clients', array(), array('name' => 'asc'));
                                            foreach ($clients as $c) {
                                                if ($rsResults[0]->clientId == ($c->id)) {
                                                    ?>
                                                    <option value="<?php echo $c->id; ?>"
                                                            selected="selected"><?php echo $c->name; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $c->id; ?>"><?php echo $c->name; ?></option>
                                                <?php }
                                            } ?>
                                        </select>
                                        <textarea class="form-control" name="shipper" rows="3"><?php echo (!empty($rsResults)?$rsResults[0]->shipper:""); ?></textarea>
                                </tr>
                                <tr>
                                    <td class="bold">CONSIGNEE:</td>
                                </tr>
                                <tr>
                                    <td height='50'><textarea name="consignee" class="form-control" rows="3"><?php echo (!empty($rsResults)?$rsResults[0]->consignee:""); ?></textarea></td>
                                </tr>
                                <tr>
                                    <td class="bold" height='40'>MAWB No. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>
                                        <input type="text" name="mawbNo" class="form-control" value="<?php echo (!empty($rsResults)?$rsResults[0]->mawbNo:""); ?>" /> Shipper's Ref <br/>
                                        <input type="text" class="form-control" name="shipperRef" value="<?php echo (!empty($rsResults)?$rsResults[0]->shipperRef:""); ?>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bold" height='40'>Airport of Departure (address of first<br/> Carrier) and requested Routing<br/>
                                        <input type="text" name="airportDept" class="form-control" value="<?php echo (!empty($rsResults)?$rsResults[0]->airportDept:""); ?>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="table" cellpadding='0' cellspacing='0'>
                                            <tr>
                                                <td class="bold">To</td>
                                                <td class="bold">Flight/ Date</td>
                                                <td class="bold">To/Flight/Date</td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" class="form-control" name="to" value="<?php echo (!empty($rsResults)? $rsResults[0]->to: ""); ?>" /></td>
                                                <td><input type="text"  class="form-control input-datepicker" data-date-format="yyyy-mm-dd" name="flightDate" id="flightDate" value="<?php echo (!empty($rsResults)?((($rsResults[0]->flightDate=='')||$rsResults[0]->flightDate == '0000-00-00')?date('Y-m-d') :$rsResults[0]->flightDate):"");?>" /></td>
                                                <td><input type="text"  class="form-control input-datepicker" data-date-format="yyyy-mm-dd" name="toFlightDate" id="toFlightDate" value="<?php echo (!empty($rsResults)?((($rsResults[0]->toFlightDate=='')||$rsResults[0]->toFlightDate == '0000-00-00')?date('Y-m-d') :$rsResults[0]->toFlightDate):"");?>" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="padding: 0;">
                            <table cellpadding='0' class="table" cellspacing='0' >
                                <tr>
                                    <td style='line-height:150%;  font-size:12px;'>
                                        <img align='left' style=' width:100px;' src='<?php echo base_url('assets/img/logo_04.png');?>'  />
                                        <div style='padding-left:150px;'>DIVINE LOGISTICS LTD<br/>
                                            Unit-3,61 Crooks Road<br/>East Tamaki, Auckland, NZ <br/>
                                            PO Box: 76877 Manukau City-2241<br/>

                                            Phone: 09-263 7439 / 2151601<br/>
                                            Fax: 09-263 7438<br/></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>SHIPPING LETTER OF INSTRUCTIONS</b></td>
                                </tr>
                                <tr>
                                    <td>
                                        <div  style='height:95px; line-height:150%; font-size:12px;'>
                                            It is agreed that goods are here in are accepted in apparent good order and condition Mawb No.
                                            Shipper's Ref (except as noted) SUBJECT TO THE CONDITIONS OF CONTRACT OBTAINABLE ON APPLICATION.
                                            THE SHIPPERS ATTENTION IS DRAWN TO THE NOTICE CONCERNING CARRIER'S LIMITATION Airport of Departure
                                            (address of first OF LIABILITY.Shipper may increase such limitation of liability by declaring a higher value for Carrier)
                                            and requested Routing carriage and paying asupplemental charge if required
                                        </div>
                                        <table border="1" class="invoice_table table">
                                            <!--<tr>
												<td class="bold">Chargeable Account</td>
												<td  width='150'><input type="text" name="chargeAcc" value="<?php //echo $rsResults[0]->chargeAcc;?>" /></td>
												<td class="bold">Account No</td>
												<td width='150'><input type="text" name="accNo" value="<?php //echo $rsResults[0]->accNo;?>" /></td>
											</tr>-->
                                            <tr>
                                                <td class="bold">Express</td>
                                                <td ><input type="text" class="form-control" name="express" value="<?php echo (!empty($rsResults)?$rsResults[0]->express:"");?>" /></td>
                                                <td class="bold">Standby</td>
                                                <td><input type="text" class="form-control" name="standBy" value="<?php echo (!empty($rsResults)?$rsResults[0]->standBy:"");?>" /></td>
                                            </tr>
                                            <tr>
                                                <td class="bold">Currency</td>
                                                <td ></td>
                                                <td class="bold">Declared/Value for customs</td>
                                                <td class="bold">NZ$ <input type="text" class="form-control" name="nzAmt" value="<?php echo (!empty($rsResults)?$rsResults[0]->nzAmt:"");?>" /></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class='invoice_table table' cellpadding='0' cellspacing='0'>
                    <tr>
                        <td width='50' class="bold">Airport of Destination</td>
                        <td style='width:100px;line-height:150%;  font-size:12px;'>
                            <input type="text" class="form-control" name="airportDest" value="<?php echo (!empty($rsResults)?$rsResults[0]->airportDest:"");?>" />
                        </td>
                        <td width='50' class="bold">Consol Ref.</td>
                        <td style='width:100px;line-height:150%;  font-size:12px;'>
                            <input type="text" class="form-control" name="consoleRef" value="<?php echo (!empty($rsResults)?$rsResults[0]->consoleRef:"");?>" />
                        </td>
                    </tr>
                    <tr>
                        <td class="bold">Handling Information</td>
                        <td colspan='3'><input type="text" class="form-control" name="handlingInf" value="<?php echo (!empty($rsResults)?$rsResults[0]->handlingInf:"");?>" /></td>
                    </tr>
                    <tr>
                        <td class="bold">Notify the consignee on arrival</td>
                        <td colspan='3'><input type="text" class="form-control" name="notifyConsignee" value="<?php echo (!empty($rsResults)?$rsResults[0]->notifyConsignee:"");?>" /></td>
                    </tr>
                </table>
                <table class='invoice_table table' cellpadding='0' cellspacing='0'>
                    <tr style='background:#000; color:#fff;'>
                        <td class="bold">No.Pieces <br/>Pkgs</td>
                        <td class="bold">Gross <br/>Weight Kg</td>
                        <td class="bold">Rate Class <br/>Commodity <br/>Item No.</td>
                        <td class="bold">Chargeable <br/>Weight Kg</td>
                        <td class="bold">Rate/Charge</td>
                        <td class="bold">Total </td>
                        <td class="bold">Nature and Quality of Goods<br/>Including Dimension of Volumne</td>
                    </tr>
                <?php   $weight = 0; $volume = 0;
                if(is_array($wsResults) && !empty($wsResults)){
                foreach($wsResults as $result){?>
                        <tr>
                            <td><input type="hidden" name="id[]" value="<?php echo $result->id;?>" class="form-control" />
                                <input type="text" name="noPieces[]" value="<?php echo $result->noPieces;?>" class="form-control" /></td>
                            <td><input type="text" name="grossWeightKg[]" value="<?php echo $result->grossWeightKg;?>" class="form-control" /></td>
                            <td><input type="text" name="rateClass[]" value="<?php echo $result->rateClass;?>" class="form-control" /></td>
                            <td><input type="text" name="chargeableWeight[]" value="<?php echo $result->chargeableWeight;?>" class="form-control" /></td>
                            <td><input type="text" name="rateCharge[]" value="<?php echo $result->rateCharge;?>"  class="form-control" /></td>
                            <td><input type="text" name="total[]" value="<?php echo $result->total;?>" class="form-control" /></td>
                            <td><input type="text" name="nature[]" value="<?php echo $result->nature;?>" class="form-control" /></td>
                        </tr>
                    </table>

                    <?php
                    //$weight += $result['weight']; $volume += $result['volume'];
                } } ?>
                <div id="more"></div>
                <table border='1' class='invoice_table table' cellpadding='0' cellspacing='0' style='font-size:12px;border-collapse:collapse;'>
                    <tr>
                        <td class="bold">FREIGHT CHARGES</td>
                        <td class="bold">PREPAID</td>
                        <td class="bold">COLLECT</td>
                        <td rowspan='4' class="bold">
                            Are goods security cleared as per Dept. <br/><br/>of transport and Regional services Regulations<br/><br/>
                            <input type="radio"  name="goodsSecurity" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]->goodsSecurity=="Y")?"checked":"" ?> />YES &nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio"  name="goodsSecurity" value="N" <?php echo (!empty($rsResults) &&  $rsResults[0]->goodsSecurity=="N")?"checked":"" ?> />NO
                        </td>
                    </tr>
                    <tr>
                        <td class="bold">NZ ZONE SERVICE</td>
                        <td><input type="radio" name="nzZoneService" value="P" <?php echo (!empty($rsResults) && $rsResults[0]->nzZoneService=="P")?"checked":"" ?> />Prepaid
                        </td>
                        <td><input type="radio" name="nzZoneService" value="C" <?php echo (!empty($rsResults) && $rsResults[0]->nzZoneService=="C")?"checked":"" ?> />Collect</td>
                    </tr><tr>
                        <td class="bold">NZ TERM CHARGE</td>
                        <td><input type="radio" name="nzTermCharge" value="P" <?php echo (!empty($rsResults) && $rsResults[0]->nzTermCharge=="P")?"checked":"" ?> />Prepaid</td>
                        <td><input type="radio" name="nzTermCharge" value="C" <?php echo (!empty($rsResults) && $rsResults[0]->nzTermCharge=="C")?"checked":"" ?> />Collect</td>
                    </tr><tr>
                        <td class="bold">Air FREIGHT</td>
                        <td><input type="radio" name="airFreight" value="P" <?php echo (!empty($rsResults) && $rsResults[0]->airFreight=="P")?"checked":"" ?> />Prepaid</td>
                        <td><input type="radio" name="airFreight" value="C" <?php echo (!empty($rsResults) && $rsResults[0]->airFreight=="C")?"checked":"" ?> />Collect</td>
                    </tr><tr>
                        <td class="bold">DEST THC</td>
                        <td><input type="radio" name="destThc" value="P" <?php echo (!empty($rsResults) && $rsResults[0]->destThc=="P")?"checked":"" ?> />Prepaid</td>
                        <td><input type="radio" name="destThc" value="C" <?php echo (!empty($rsResults) && $rsResults[0]->destThc=="C")?"checked":"" ?> />Collect</td>
                        <td width='425' rowspan='3'>
                            Shipper certifies that the particurals on the face hereof are correct and that insofar as
                            any part of the consignment contains dangerous goods, such part is propery described by
                            name and in proper condition for carriage by air according to the international Air Transport
                            Assosiation's Dangerous Goods Regulations.
                        </td>
                    </tr>
                    <tr>
                        <td class="bold"> DEST ZONE ONC</td>
                        <td><input type="radio" name="destZoneThc" value="P" <?php echo (!empty($rsResults) && $rsResults[0]->destZoneThc=="P")?"checked":"" ?> />Prepaid</td>
                        <td><input type="radio" name="destZoneThc" value="C" <?php echo (!empty($rsResults) && $rsResults[0]->destZoneThc=="C")?"checked":"" ?> />Collect</td>
                    </tr><tr>
                        <td class="bold">OTHER CHARGES</td>
                        <td><input type="radio" name="otherCharges" value="P" <?php echo (!empty($rsResults) && $rsResults[0]->otherCharges=="P")?"checked":"" ?> />Prepaid</td>
                        <td><input type="radio" name="otherCharges" value="C" <?php echo (!empty($rsResults) && $rsResults[0]->otherCharges=="C")?"checked":"" ?> />Collect</td>
                    </tr>
                    <tr>
                        <td colspan='4'>
                            <br/><br/><br/>
                            <table border='0' class="table">
                                <tr>
                                    <td class="bold">Executed on </td>
                                    <td class="bold">Date </td>
                                    <td><input type="text"  class="form-control input-datepicker" data-date-format="yyyy-mm-dd" name="executedDate" id="executedDate" value="<?php echo (!empty($rsResults)?((($rsResults[0]->executedDate=='')||$rsResults[0]->executedDate == '0000-00-00')?date('Y-m-d') :$rsResults[0]->executedDate):"") ;?>" /></td>
                                    <td class="bold">Signature of Shipper</td>
                                    <td></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='4' class="bold">
                            ALL BUSINESS IS TRANSACTED UNDER THE COMPANY'S STANDARD TRADING CONDITIONS OBTAINABLE
                            ON APPLICATION<br/>
                            <input type="checkbox" name="acceptTC" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]->acceptTC=="Y"?"checked":"")?> /> I accept to all the terms and conditions as on website
                        </td>
                    </tr>
                </table>
                <br/><br/>
                <table class="invoice_inner_table invoice_table table" id="invoice_description" cellpadding="0" cellspacing="0">
                    <tr>
                        <td><input type="button" id="add_more" value="ADD MORE" />&nbsp;
                            <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i class="fa fa-check"></i> Update</button>
                            <br/>
                            <a target="_blank" href="<?php echo base_url('invoice/pdf_airfreight?invoiceId='.$this->uri->segment(3));?>" >PREVIEW PDF</a>
                        </td>
                    </tr>
                </table>

            </form>
        </div>
    </div>
</div>

<script language="javascript">
    $(document).ready(function() {
        $("#add_more").click(function() {
            $("#more").append('<table class="invoice_inner_table invoice_table table" cellpadding="0" cellspacing="0"><tr><td><input type="hidden" name="id[]" value="0" class="form-control" /><input type="text" name="noPieces[]"  class="td50" /></td><td><input type="text" name="grossWeightKg[]" class="form-control" /></td><td><input type="text" name="rateClass[]" class="form-control" /></td><td><input type="text" name="chargeableWeight[]" class="form-control" /></td><td><input type="text" name="rateCharge[]"  class="form-control" /></td><td><input type="text" name="total[]" class="form-control" /></td><td><input type="text" name="nature[]" class="form-control" /></td></tr></table>');
        });
        //$("#add_more").trigger('click');
    });
</script>