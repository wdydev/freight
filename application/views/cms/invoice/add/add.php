<div class="row">
    <div class="row col-md-12">
        <div class="block">
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="<?php echo base_url('invoice/add/'); ?>">Add Invoice</a></li>
                    <li><a href="<?php echo base_url('invoice/view/');?>">Invoice
                            List</a></li>
                </ul>
            </div>
            <div class="col-md-12">
                <div class="col-md-6"><img src="<?php echo base_url('assets/img/logo_03.png'); ?>"/></div>
                <div class="col-md-3">Divine Logistics Ltd<br/>Unit-3,12 Lambie Drive,<br/>Manukau Central, Auckland, NZ<br/>POBox:
                    76877<br/>Manukau City 2241, Auckland
                </div>
                <div class="col-md-3">Office: 09 2637439, 09 2151601<br/>Mobile: 02102494202<br/>After Hours: 09-2673201<br/>Email
                    :info@divinelogistics.co.nz<br/>Web :www.divinelogistics.co.nz
                </div>
            </div>
            <div class="clearfix"></div>
            <br/>

            <?php if (isset($message)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
                <?php
            } ?>

            <?php if (validation_errors()) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <?php
                    echo validation_errors();
                    ?>
                </div>
            <?php } ?>


            <form action="" method="post" class="form-horizontal form-bordered">
                <div class="block col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="invoice_number">Tax Invoice</label>

                            <div class="col-md-6">
                                <?php $details = $this->admin_model->table_fetch_rows('invoices', array(), array('id' => 'desc')); ?>
                                <input type="text" id="invoice_number" name="invoice_number" class="form-control"
                                       value="<?php echo($details[0]->invoice_number + 1); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="GST">GST #:</label>
                            <div class="col-md-6">
                                102498070
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="client_name">Client name</label>

                            <div class="col-md-6">
                                <select id="clientId" name="clientId" class="form-control" size="1">
                                    <option selected disabled>Please Select</option>
                                    <?php $clients = $this->admin_model->table_fetch_rows('clients', array(), array('name' => 'asc'));
                                    foreach ($clients as $c) {
                                        ?>
                                        <option value="<?php echo $c->id; ?>"><?php echo $c->name; ?></option>
                                    <?php } ?>
                                </select> OR
                                <input type="text" id="client_name" name="client_name" class="form-control"
                                       placeholder="Enter Client Name"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="client_address">Client Address</label>

                            <div class="col-md-6">
                    <textarea id="client_address" rows="3" cols="10" name="client_address" class="form-control"
                              style="width: 322px; height: 99px;" placeholder="Enter client address"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="account_type">Account</label>

                            <div class="col-md-6">
                                <select id="account_type" name="account_type" class="form-control" size="1">
                                    <option value="Air">Air</option>
                                    <option value="Sea">Sea</option>
                                    <option value="Personal">Personal</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="invoice_date">Invoice Date</label>

                            <div class="col-md-6">
                                <input type="text" id="invoice_date" name="invoice_date"
                                       class="form-control input-datepicker" data-date-format="yyyy-mm-dd"
                                       value="<?php echo date('Y-m-d'); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="due_date">Due Date</label>

                            <div class="col-md-6">
                                <input type="text" id="due_date" name="due_date" class="form-control input-datepicker"
                                       data-date-format="yyyy-mm-dd" value="<?php echo date('Y-m-d'); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="terms">Terms</label>

                            <div class="col-md-6">
                                <input type="text" id="terms" name="terms" class="form-control"
                                       placeholder="Enter Terms">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="shipment">Shipment</label>

                            <div class="col-md-6">
                                <input type="text" id="shipment" name="shipment" class="form-control"
                                       placeholder="Enter Shipment">
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="consignor">Consignor</label>

                                <div class="col-md-8">
                                    <input type="text" id="consignor" name="consignor" class="form-control"
                                           placeholder="Enter consignor name">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="order_reference">Client/Order
                                    Reference</label>

                                <div class="col-md-8">
                                    <input type="text" id="order_reference" name="order_reference" class="form-control"
                                           placeholder="Enter order reference">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="consignee">Consignee</label>

                                <div class="col-md-8">
                                    <input type="text" id="consignee" name="consignee" class="form-control"
                                           placeholder="Enter consignee">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="goods_description">Goods Description</label>

                                <div class="col-md-6">
                                    <textarea id="goods_description" rows="3" cols="10" name="goods_description"
                                              class="form-control"
                                              style="width: 322px; height: 99px;"
                                              placeholder="Enter Goods Description"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="LCL_FCL">LCF/FCL</label>

                                <div class="col-md-6">
                                    <select id="LCL_FCL" name="LCL_FCL" class="form-control" size="1">
                                        <option value="">None</option>
                                        <option value="LCL">LCL</option>
                                        <option value="FCL">FCL</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="packages">Packages</label>
                                <div class="col-md-8">
                                    <input type="text" id="packages" name="packages" class="form-control"
                                           placeholder="Enter packages">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="weight">Weight</label>
                                <div class="col-md-8">
                                    <input type="text" id="weight" name="weight" class="form-control"
                                           placeholder="Enter weight">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="volume">Volume</label>
                                <div class="col-md-8">
                                    <input type="text" id="volume" name="volume" class="form-control"
                                           placeholder="Enter volume">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="chargeable">Chargeable</label>
                                <div class="col-md-8">
                                    <input type="text" id="chargeable" name="chargeable" class="form-control"
                                           placeholder="Chargeable">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="flight_name" id="flight_vesel">Flight & Date</label>
                                <div class="col-md-4">
                                    <input type="text" id="flight_vesel_name" name="flight_name" class="form-control"
                                           placeholder="Flight Name">
                                </div>
                                <div class="col-md-4">
                                    <input type="text" id="flight_date" name="flight_date"
                                           class="form-control input-datepicker" data-date-format="yyyy-mm-dd"
                                           value="<?php echo date('Y-m-d'); ?>">
                                </div>

                            </div>
                        </div>

                        <div class="col-md-3 values" >
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="mawb">MAWB</label>
                                <div class="col-md-8">
                                    <input type="text" id="mawb" name="mawb" class="form-control"
                                           placeholder="Enter mawb">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 values1" style="display: none;">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="container_number">Container Number</label>
                                <div class="col-md-8">
                                    <input type="text" id="container_number" name="container_number" class="form-control"
                                           placeholder="Enter container_number">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 values">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="hawb">HAWB</label>
                                <div class="col-md-8">
                                    <input type="text" id="hawb" name="hawb" class="form-control"
                                           placeholder="Enter hawb">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="origin">Origin</label>
                                <div class="col-md-8">
                                    <input type="text" id="origin" name="origin" class="form-control"
                                           placeholder="Enter origin">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="etd">ETD</label>
                                <div class="col-md-8">
                                    <input type="text" id="etd" name="etd"
                                           class="form-control input-datepicker" data-date-format="yyyy-mm-dd"
                                           value="<?php echo date('Y-m-d'); ?>">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="destination">Destination </label>
                                <div class="col-md-8">
                                    <input type="text" id="destination" name="destination" class="form-control"
                                           placeholder="Enter destination">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="eta">ETA</label>
                                <div class="col-md-8">
                                    <input type="text" id="eta" name="eta"
                                           class="form-control input-datepicker" data-date-format="yyyy-mm-dd"
                                           value="<?php echo date('Y-m-d'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" style="height: 150px;">
                        <table class="invoice_table invoice_inner_table  table col-md-12" id="invoice_description" cellpadding="0" cellspacing="0">
                            <tr style='background:#000; color:#fff;'>
                                <td width="60%"><strong>Description</strong></td>
                                <td width="20%"><strong>GST in NZD</strong></td>
                                <td width="20%"><strong>Charges in NZD</strong> <a href="javascript:void();" id="add_more">More</a></td>
                            </tr>

                                <tr class="description_wrap">
                                    <td width="60%"><input type="hidden" name="descriptionId[]" value="0" />
                                        <input type="text" size="70" class="form-control" id="description_1" name="description[]" value="" /></td>
                                    <td width="20%">
                                        <select name="gst_nzd[]" class="form-control" id="gst_nzd_1" >
                                            <option value="zero">Zero Rated</option>
                                            <option value="15">15 %</option>
                                        </select>
                                    </td>
                                    <td width="20%"><input type="text" id="charges_nzd_1" name="charges_nzd[]" class="charges_nzd form-control" value="" /></td>
                                </tr>

                        </table>
                    </div>
                        <table class="invoice_table invoice_inner_table table" id="invoice_description" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="87%">Please contact us within 7 days should there</td>
                                <td width="40%"><label>Sub Total:</label>
                                    <span id="sub_total"></span>
                                </td>
                            </tr>
                            <tr>
                                <td width="70%">by any discrepancies</td>
                                <td width="40%"><label>Add Gst:</label>
                                    <span id="gst_total"></span>
                                </td>
                            </tr>
                            <tr>
                                <td width="70%">
                                    <select name="contact_enum" class="form-control" id="contact_enum" style="width:100px;>
                                        <option value="1">Yes</option>
                                        <option value="0" >No</option>
                                    </select><br/><br/>Custom GST?
                                    <select name="customGST" class="form-control" id="customGST" style="width:100px;>
                                        <option value="0" >No</option>
                                        <option value="1" >Yes</option>
                                    </select>

                                </td>
                                <td width="40%"><label>Total NZD:</label>
                                    <span id="total_amount"></span>
                                </td>
                            </tr>
                            <tr>
                                <td width="70%"></td>
                                <td width="40%"><label>Credit:</label>
                                    <input type="text" class="form-control" name="discount" id="discount" value="" size="5" />
                                </td>
                            </tr>
                            <tr>
                                <td width="70%"></td>
                                <td width="40%"><label>Final Total NZD:</label>
                                    <span id="fina_total_amount"></span>
                                </td>
                            </tr>
                        </table>
                        <table class="invoice_table table">
                            <tr>
                                <td width="65%">

                                    <p>&nbsp;</p>
                                    <p><i>Transfer Funds To:</i></p>
                                    <p>Bank Details :</p>
                                    <p><label>Bank </label>&emsp;&emsp; 011804</p>
                                    <p><label>Account </label>&emsp;&emsp; 0151562-00 ANZ</p>
                                    <p><label>Swift </label>&emsp;&emsp; ANZ BNZ22</p>
                                </td>
                                <td width="10%">&nbsp;</td>
                                <td width="45%">
                                    <p>Divine Logistics Ltd. <br /> P.O Box - 76877 <br /> Manukau City - 2241 <br /> Manukau</p>
                                </td>
                            </tr>
                            <tr><td colspan="3"><p>**All shipments subject to our standard trading conditions, copies available on request or may be downloaded from our web site.**</p></td></tr>
                            <tr>
                                <td colspan="3">
                                    <input type="hidden" class="form-control" name="bookingFlag" value="N" />

                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i class="fa fa-check"></i> Save</button>
<!--                                    <a  target="_blank" href="pdf.php?id=--><?//=$_GET['edit']?><!--">PREVIEW INVOICE PDF</a><br/>-->
<!--                                    <a  target="_blank" href="pdf_authority.php?id=--><?//=$_GET['edit']?><!--">PREVIEW AUTHORITY LETTER PDF</a><br/>-->
                                </td>
                            </tr>
                        </table>




                </div>


        </div>
    </div>
</div>


<script language="javascript">
    $(document).ready(function() {
        $("#account_type").change(function () {
            var val = $(this).val();
            if (val == 'Air') {
                $(".values1").hide();
                $(".values").show();
                $("#flight_vesel").html('Flight & Date:');
                $("#flight_vesel_name").attr('placeholder', 'Flight');
                $("#flight_date").show();
            } else {
                $(".values1").show();
                $(".values").hide();
                $("#flight_vesel").html('Vessel / Voyage');
                $("#flight_vesel_name").attr('placeholder', 'Vessel / Voyage');
                $("#flight_date").hide();
            }
        });

        $("#add_more").click(function() {
            next_item = parseInt($(".description_wrap").length) + 1;
            $("#invoice_description").append('<tr class="description_wrap"><td width="60%"><input type="hidden" class="form-control" name="descriptionId[]" value="0" /><input type="text" class="form-control" id="description_'+next_item+'" size="70" name="description[]" value="" /></td><td width="20%"><select class="form-control" id="gst_nzd_'+next_item+'" name="gst_nzd[]"><option value="zero">Zero Rated</option><option value="15">15 %</option></select></td><td width="20%"><input type="text" id="charges_nzd_'+next_item+'" name="charges_nzd[]" class="charges_nzd form-control" value="" /></td></tr>');

            $(".charges_nzd").keyup(function() {
                var total = 0;
                $(".charges_nzd").each(function(){
                    var val = $(this).val();
                    console.log(val);
                    total = parseFloat(total) + parseFloat(val);
                });
                $("#sub_total").html(total);
                calculateGST();
            });

            $("select[name='gst_nzd[]']").change(function() {
                calculateGST();
            });
        });

        $("#shipment, #order_reference").val($("#invoice_number").val());
        $("#invoice_number").keyup(function() {
            $("#shipment, #order_reference").val($("#invoice_number").val());
        });

        $(".charges_nzd").keyup(function() {
            var total = 0;
            $(".charges_nzd").each(function(){
                var val = $(this).val();
                console.log(val);
                total = parseFloat(total) + parseFloat(val);
            });
            $("#sub_total").html(total);
            calculateGST();
        });
        $("#discount").keyup(function() {
            calculateGST();
        });

        $("select[name='gst_nzd[]']").change(function() {
            calculateGST();
        });
        $("#account_type").trigger('change');
    });


    function calculateGST() {
        var gst = 0;
        $("select[name='gst_nzd[]']").each(function() {
            var val = $(this).val();
            if(val != 'zero') {
                id = $(this).attr('id').split('_');
                amount = $("#charges_nzd_"+id[2]).val();
                gst = gst + ((amount * parseInt(val)) / 100);
            }
            $("#gst_total").html(gst);
            $("#total_amount").html(parseFloat($("#gst_total").html()) + parseFloat($("#sub_total").html()));
            $("#fina_total_amount").html(parseFloat($("#total_amount").html()) - parseFloat($("#discount").val()));
        });
    }


</script>
