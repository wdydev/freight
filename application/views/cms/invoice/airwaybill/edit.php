<style type="text/css">
    .bold{
        font-weight: bold;
    }
</style>

<div class="row">
    <div class="row col-md-12">
        <div class="block">
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li><a href="<?php echo base_url('invoice/edit/' . $this->uri->segment(3)); ?>">Edit Invoice</a></li>
                    <?php if (!empty($detail) &&($detail[0]->account_type == "Air")){ ?>
                        <li><a href="<?php echo base_url('invoice/edit_air_freight/' . $this->uri->segment(3)); ?>">Edit
                                Air Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_airfreight/' . $this->uri->segment(3)); ?>">Edit Booking(Air)</a></li>
                    <?php } else { ?>
                        <li class="active"><a href="<?php echo base_url('invoice/edit_sea_freight/' . $this->uri->segment(3)); ?>">Edit Sea
                                Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_seafreight/' . $this->uri->segment(3)); ?>">Edit Booking(Sea)</a></li>
                    <?php } ?>
                    <li><a href="<?php echo base_url('invoice/edit_acceptance/' . $this->uri->segment(3)); ?>">Edit
                            Acceptance</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_credit_note/' . $this->uri->segment(3)); ?>">Edit
                            Credit Note</a></li>
                    <li class="active"><a href="<?php echo base_url('invoice/edit_airwaybill/' . $this->uri->segment(3)); ?>">Edit
                            AirwayBill</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_weight_slip/' . $this->uri->segment(3)); ?>">Edit
                            Weight Slip</a></li>
                    <li><a href="<?php echo base_url('invoice/inventory_list/' . $this->uri->segment(3)); ?>">Inventory List</a></li>
                    <li><a href="<?php echo base_url('invoice/email/' . $this->uri->segment(3)); ?>">Email</a></li>
                </ul>
            </div>

            <form action="" method="post" class="form-horizontal form-bordered">
                <input type="hidden" name="invoiceId" value="<?php echo $this->uri->segment(3); ?>"/>

                <div style='width:700px;'>HAWB No. S
                    <input type="text" name="hawbNo" class="form-control" value="<?php echo (!empty($detail)?$detail[0]->invoice_number:""); ?>"/>
                </div>
                <table border='0' cellspacing='0' class='invoice_table table' cellpadding='0' style='border-collapse:collapse;'>
                    <tr>
                        <td style="padding: 0;">
                            <table border='1' class="table" cellpadding='0' cellspacing='0' style='border-collapse:collapse;'>
                                <tr>
                                    <td colspan='6'>Shipper Name and Address:<br/>
                                        <select id="clientId" name="clientId" class="form-control" size="1">
                                            <option selected disabled>Please Select</option>
                                            <?php $clients = $this->admin_model->table_fetch_rows('clients', array(), array('name' => 'asc'));
                                            foreach ($clients as $c) {
                                                if ($rsResults[0]->clientId == ($c->id)) {
                                                    ?>
                                                    <option value="<?php echo $c->id; ?>"
                                                            selected="selected"><?php echo $c->name; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $c->id; ?>"><?php echo $c->name; ?></option>
                                                <?php }
                                            } ?>
                                        </select><br/>
                                        <textarea name="shipper" clas="form-control" rows="3"><?php echo  (!empty($rsResults)?$rsResults[0]->shipper:""); ?></textarea>
                                        <br/>
                                        Shipper Account Number<br/>
                                        <input type="text" name="shipperAccNo" class="form-control"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->shipperAccNo:""); ?>"/>

                                    </td>
                                </tr>
                                <tr>
                                    <td colspan='6'>Consignee's Name and Address:<br/>
                                        <textarea name="consignee" class="form-control"
                                                  rows="3"><?php echo (!empty($rsResults)?$rsResults[0]->consignee:""); ?></textarea><br/>
                                        Consignee Account Number<br/>
                                        <input type="text" name="consigneeAccNo" class="form-control"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->consigneeAccNo:""); ?>"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan='6'>Issuing Carrier's Agent Name and City<br/>
                                        <textarea name="issueCarrier" class="form-control"
                                                  rows="3"><?php echo (!empty($rsResults)?$rsResults[0]->issueCarrier:""); ?></textarea><br/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan='2'>Agent's IATA Code<br/>
                                        <input type="text" name="agentIATACode" class="form-control"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->agentIATACode:""); ?>"/>
                                    </td>
                                    <td colspan='4'> Account No.<br/>
                                        <input type="text" name="agentAccNo" class="form-control"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->agentAccNo:""); ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan='6'>Airport of Departure (Addr. of First Carrier) and Requested
                                        Routing<br/>
                                        <input type="text" name="airportDeparture" class="form-control"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->airportDeparture:""); ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>To<br/>
                                        <input type="text" name="toMain" class="form-control"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->toMain:""); ?>"/>
                                    </td>
                                    <td>By First Carrier /Routing and Destination <br/>
                                        <input type="text" name="byFirstCarrier" class="form-control"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->byFirstCarrier:""); ?>"/><br/>

                                        <!--						<br/><input type="text" name="routingDestination" class="form-control" value="<?php //$rsResults['routingDestination'] ?>" />-->
                                    </td>
                                    <td>to<br/>
                                        <input type="text" name="to1" class="form-control" value="<?php echo (!empty($rsResults)?$rsResults[0]->to1:""); ?>"/>
                                    </td>
                                    <td>by<br/>
                                        <input type="text" name="by1" class="form-control"  value="<?php echo (!empty($rsResults)?$rsResults[0]->by1:""); ?>"/>
                                    </td>
                                    <td>to<br/>
                                        <input type="text" name="to2" class="form-control"  value="<?php echo (!empty($rsResults)?$rsResults[0]->to2:""); ?>"/>
                                    </td>
                                    <td>by<br/>
                                        <input type="text" name="by2" class="form-control"  value="<?php echo (!empty($rsResults)?$rsResults[0]->by2:""); ?>"/>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan='2'>Airport of Destination<br/>
                                        <input type="text" name="airportOfDestination" class="form-control"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->airportOfDestination:""); ?>"/>
                                    </td>
                                    <td colspan='4'>Requested Flight/Date<br/>
                                        <input type="text" name="requestedFlight" class="form-control"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->requestedFlight:""); ?>"/> / <input type="text"
                                                                                                       name="requestedDate"
                                                                                                       class="input-datepicker form-control" data-date-format="yyyy-mm-dd"
                                                                                                       value="<?php echo (!empty($rsResults)?$rsResults[0]->requestedDate:""); ?>"/><br/>


                                    </td>


                                </tr>
                            </table>
                        </td>
                        <td style="padding: 0;">
                            <table border='1' cellpadding='0' cellspacing='0' class="table" style='border-collapse:collapse;'>
                                <tr>
                                    <td colspan='3' style='line-height:150%;  font-size:12px;'>
                                        <div style='font-size:12px;width:150px;'><b>Not Negotiable</b><br/>
                                            <span style='font-size:24px;'>Air Waybill</span><br/>
                                            Issued by
                                        </div>
                                        <div class="clearfix"></div>
                                        <div style='float:left;padding-left:200px;300px; '>
                                            <img align="left" style=' width:100px;' src='<?php echo base_url('assets/img/logo_04.png');?>' />
                                            DIVINE LOGISTICS LTD<br/>
                                            Unit-3,61 Crooks Road<br/>East Tamaki, Auckland, NZ<br/>
                                            Phone: 09-263 7439 / 2151601<br/>
                                            Fax: 09-263 7438<br/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan='3'>Copies 1,2 and 3 of this Air Waybill are originals and have the same
                                        validity.
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan='3'>
                                        <div style=' line-height:150%; font-size:10px;width: 570px;'>
                                            It is agreed that the goods described herein are accepted in apparent good
                                            order and condition (except as noted) for carriage. SUBJECT TO THE
                                            CONDITIONS OF THE CONTRACT ON THE REVERSE HEREOF. ALL GOES MAY BE CARRIED BY
                                            AND OTHER MEANS INCLUDING ROAD OR ANY OTHER CARRIER UNLESS SPECIFIC CONTRARY
                                            INSTRUCTIONS ARE GIVEN HEREON BY THE SHIPPER, AND SHIPPER AGREES THAT THE
                                            SHIPMENT MAY BE CARRIED VIA INTERMEDIATE STOPPING PLACES WHICH THE CARRIER
                                            DEEMS APPROPRIATE THE SHIPPER'S ATTENTION IS DRAWN TO THE NOTIC CONCERNING
                                            CARRIER'S LIMITATION OF LIABILITY. Shipper may increase such limitation of
                                            liability by declaring a higher value for carriage and paying a supplemental
                                            charge if required.
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3"> Accounting Information<br/>
                                        <textarea name="accoutingInformation" class="form-control"
                                                  rows="3"><?php echo (!empty($rsResults)?$rsResults[0]->accoutingInformation:""); ?></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td> Reference Number<br/>
                                        <input type="text" name="referenceNo" class="form-control"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->referenceNo:""); ?>"/>
                                    </td>
                                    <td> Optional Shipping Information<br/>
                                        <input type="text" name="optionalShippingInformation" class="form-control"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->optionalShippingInformation:""); ?>"/>
                                    </td>
                                    <td>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border='1' class="table" style='border-collapse:collapse;font-size:12px;'>

                                            <tr>
                                                <td rowspan="2">Currency<br/>

                                                </td>
                                                <td rowspan="2">
                                                    CHGS<br/>Code
                                                </td>
                                                <td colspan="2">WT/Val</td>
                                                <td colspan="2">Other</td>
                                            </tr>
                                            <tr>
                                                <td>PPD</td>
                                                <td>COLL</td>
                                                <td>PPD</td>
                                                <td>COLL</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="text" name="currency" class="form-control"
                                                           value="<?php echo (!empty($rsResults)?$rsResults[0]->currency:""); ?>"/>
                                                </td>
                                                <td><input type="text" class="form-control" name="chgsCode"
                                                           value="<?php echo (!empty($rsResults)?$rsResults[0]->chgsCode:""); ?>"/></td>
                                                <td><input type="text" class="form-control" name="ppd_WTVal"
                                                           value="<?php echo (!empty($rsResults)?$rsResults[0]->ppd_WTVal:""); ?>"/></td>
                                                <td><input type="text" class="form-control" name="coll_WTVal"
                                                           value="<?php echo (!empty($rsResults)?$rsResults[0]->coll_WTVal:""); ?>"/></td>
                                                <td><input type="text" class="form-control" name="ppd_Other"
                                                           value="<?php echo (!empty($rsResults)?$rsResults[0]->ppd_Other:""); ?>"/></td>
                                                <td><input type="text" class="form-control" name="coll_Other"
                                                           value="<?php echo (!empty($rsResults)?$rsResults[0]->coll_Other:""); ?>"/></td>

                                            </tr>
                                        </table>
                                    </td>
                                    <td> Declared Value for Carriage<br/>
                                        <input type="text" name="declaredValueCarriage" class="form-control"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->declaredValueCarriage:""); ?>"/>
                                    </td>
                                    <td> Declared Value for Customs<br/>
                                        <input type="text" name="declaredValueCustoms" class="form-control"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->declaredValueCustoms:""); ?>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td> Amount of Insurance<br/>
                                        <input type="text" name="amountOfInsurance" class="form-control"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->amountOfInsurance:""); ?>"/>
                                    </td>
                                    <td colspan="2">
                                        <div style='line-height:150%; font-size:12px;'>INSURANCE - If Carrier offers
                                            Insurance, and such Insurance is requested in accordance with the conditions
                                            thereof, indicate amount to be insured in figures in box marked "amount of
                                            insurance"
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Handling Information <input type="text" name="handlingInformation" class="form-control"
                                                        value="<?php echo (!empty($rsResults)?$rsResults[0]->handlingInformation:""); ?>"/>
                        </td>
                        <td>
                            SCI
                            <input type="text" name="sci" class="form-control" value="<?php echo (!empty($rsResults)?$rsResults[0]->sci:""); ?>"/>
                        </td>
                    </tr>
                </table>

                <table border='1' class='invoice_table table' cellpadding='0' cellspacing='0'
                       style='font-size:12px;border-collapse:collapse;'>
                    <tr style='background:#000; color:#fff;'>
                        <td class="bold">No.Pieces <br/>Pkgs</td>
                        <td class="bold">Gross <br/>Weight Kg</td>
                        <td class="bold">Rate Class <br/>Commodity <br/>Item No.</td>
                        <td class="bold">Chargeable <br/>Weight Kg</td>
                        <td class="bold">Rate/Charge</td>
                        <td class="bold">Total</td>
                        <td class="bold">Nature and Quality of Goods<br/>Including Dimension of Volumne</td>
                    </tr>
                <?php $weight = 0; $volume = 0;
                if(is_array($wsResults) && !empty($wsResults)){
                foreach($wsResults as $result){?>
                        <tr>
                            <td><input type="hidden" name="id[]" value="<?php echo $result->id; ?>" class="form-control"/>
                                <input type="text" name="noPieces[]" value="<?php echo $result->noPieces; ?>" class="form-control"/>
                            </td>
                            <td><input type="text" name="grossWeightKg[]"
                                                  value="<?php echo $result->grossWeightKg; ?>" class="form-control"/></td>
                            <td><input type="text" name="rateClass[]" value="<?php echo $result->rateClass; ?>"
                                                   class="form-control"/></td>
                            <td><input type="text" name="chargeableWeight[]"
                                                   value="<?php echo $result->chargeableWeight; ?>" class="form-control"/></td>
                            <td><input type="text" name="rateCharge[]" value="<?php echo $result->rateCharge; ?>"
                                                   class="form-control"/></td>
                            <td><input type="text" name="total[]" value="<?php echo $result->total; ?>"
                                                   class="form-control"/></td>
                            <td><input type="text" name="nature[]" value="<?php echo $result->nature; ?>"
                                                   class="form-control"/></td>
                        </tr>
                    </table>

                    <?php
                    //$weight += $result['weight']; $volume += $result['volume'];
                } }?>
                <div id="more"></div>
                <table border='1' class='invoice_table table' cellpadding='0' cellspacing='0'>
                    <tr>
                        <td width='100' class="bold">Prepaid</td>
                        <td width='100' class="bold">Weight Charge</td>
                        <td width='100' class="bold">Collect</td>
                        <td width='425' rowspan='6' colspan='2'>
                            Other Charges<br/>
                            <textarea name="otherCharges" class="form-control" rows="3" cols="20"><?php echo (!empty($rsResults)?$rsResults[0]->otherCharges:""); ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><input type="text" name="prepaid" value="<?php echo (!empty($rsResults)?$rsResults[0]->prepaid:""); ?>" class="form-control"/></td>
                        <td><input type="text" name="weightCharge" value="<?php echo (!empty($rsResults)?$rsResults[0]->weightCharge:""); ?>" class="form-control"/>
                        </td>
                        <td><input type="text" name="collect" value="<?php echo (!empty($rsResults)?$rsResults[0]->collect:""); ?>" class="form-control"/></td>

                    </tr>
                    <tr>
                        <td colspan='3'>Valuation Charge</td>
                    </tr>
                    <tr>
                        <td colspan='3'><input type="text" name="valuationCharge"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->valuationCharge:""); ?>" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td colspan='3'>Tax</td>
                    </tr>
                    <tr>
                        <td colspan='3'><input type="text" name="tax" value="<?php echo (!empty($rsResults)?$rsResults[0]->tax:""); ?>" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td colspan='3'>Total Other Charges Due Agent</td>
                        <td width='425' rowspan='4' colspan='2'>
                            Shipper certifies that the particurals on the face hereof are correct and that insofar as
                            any part of the consignment contains dangerous goods, such part is propery described by
                            name and in proper condition for carriage by air according to the international Air
                            Transport
                            Assosiation's Dangerous Goods Regulations.
                        </td>
                    </tr>
                    <tr>
                        <td colspan='3'><input type="text" name="totalOtherChargesDueAgent"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->totalOtherChargesDueAgent:""); ?>" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td colspan='3'>Total Other Charges Due Carrier</td>
                    </tr>
                    <tr>
                        <td colspan='3'><input type="text" name="totalOtherChargesDueCarrier"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->totalOtherChargesDueCarrier:""); ?>" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td colspan='3' height='50'>&nbsp;</td>
                        <td align='center' colspan='2'>
                            <span style='font-size:25px;'>DIVINE LOGISTICS LTD</span>
                            <br/>Signature of Shipper or his Agent
                    </tr>
                    </tr>
                    <tr>
                        <td colspan='2'>Total Prepaid</td>
                        <td>Total Collect</td>
                        <td rowspan='4' colspan='2'>
                            <table border='0'>
                                <tr>
                                    <td height='50' colspan='3'>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td><input type="text" name="executedDate" value="<?php echo (!empty($rsResults)?$rsResults[0]->executedDate:""); ?>"
                                               data-date-format="yyyy-mm-dd" class="input-datepicker form-control"/></td>
                                    <td><input type="text" name="atPlace" value="<?php echo (!empty($rsResults)?$rsResults[0]->atPlace:""); ?>"
                                               class="form-control"/></td>
                                    <td><input type="text" name="signature" value="<?php echo (!empty($rsResults)?$rsResults[0]->signature:""); ?>"
                                               class="form-control"/></td>
                                </tr>
                                <tr>
                                    <td>Executed on(date)</td>
                                    <td>at (place)</td>
                                    <td>Signature of Issuing Carrier or its Agent</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="text" name="totalPrepaid" value="<?php echo (!empty($rsResults)?$rsResults[0]->totalPrepaid:""); ?>"
                                               class="form-control"/></td>
                        <td><input type="text" name="totalCollect" value="<?php echo (!empty($rsResults)?$rsResults[0]->totalCollect:""); ?>" class="form-control"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan='2'>Currency Conversion Rates</td>
                        <td colspan='1'>CC. Charges in Dest. Currency</td>
                    </tr>
                    <tr>
                        <td colspan="2"><input type="text" name="currencyConversionRates"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->currencyConversionRates:""); ?>" class="form-control"/></td>
                        <td><input type="text" name="ccChargesinDestCurrency"
                                   value="<?php echo (!empty($rsResults)?$rsResults[0]->ccChargesinDestCurrency:""); ?>" class="form-control"/></td>
                    </tr>
                    <tr>
                        <td rowspan="2" colspan="2">For Carrier's use only at Destination</td>
                        <td>Charges at Destination</td>
                        <td>Total Collect Charges</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td><input type="text" name="chargesAtDestination"
                                   value="<?php echo (!empty($rsResults)?$rsResults[0]->chargesAtDestination:""); ?>" class="form-control"/></td>
                        <td><input type="text" name="totalCollectCharges" value="<?php echo (!empty($rsResults)?$rsResults[0]->totalCollectCharges:""); ?>"
                                   class="form-control"/></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan='4' class="bold">
                            ALL BUSINESS IS TRANSACTED UNDER THE COMPANY'S STANDARD TRADING CONDITIONS OBTAINABLE
                            ON APPLICATION
                        </td>
                    </tr>
                </table>
                <br/><br/>
                <table class="invoice_inner_table invoice_table" id="invoice_description" cellpadding="0"
                       cellspacing="0">
                    <tr>
                        <td><input type="button" id="add_more" value="ADD MORE"/>&nbsp;
                            <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i class="fa fa-check"></i> Update</button>
                            <br/>
                            <a target="_blank" href="<?php echo base_url('invoice/pdf_airwaybill?invoiceId='.$this->uri->segment(3));?>">PREVIEW PDF</a></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>

<script language="javascript">
    $(document).ready(function () {
        $("#add_more").click(function () {
            $("#more").append('<table  class="invoice_inner_table invoice_table table" cellpadding="0" cellspacing="0"><tr><td><input type="hidden" name="id[]" value="0" class="form-control" /><input type="text" name="noPieces[]"  class="form-control" /></td><td><input type="text" name="grossWeightKg[]" class="form-control" /></td><td><input type="text" name="rateClass[]" class="form-control" /></td><td><input type="text" name="chargeableWeight[]" class="form-control" /></td><td><input type="text" name="rateCharge[]"  class="form-control" /></td><td><input type="text" name="total[]" class="form-control" /></td><td><input type="text" name="nature[]" class="form-control" /></td></tr></table>');
        });
        //$("#add_more").trigger('click');
    });
</script>