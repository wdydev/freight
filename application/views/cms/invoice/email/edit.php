<div class="row">
    <div class="row col-md-12">
        <div class="block">
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li><a href="<?php echo base_url('invoice/edit/' . $this->uri->segment(3)); ?>">Edit Invoice</a></li>
                    <?php if (!empty($detail) &&($detail[0]->account_type == "Air")){ ?>
                        <li><a href="<?php echo base_url('invoice/edit_air_freight/' . $this->uri->segment(3)); ?>">Edit
                                Air Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_airfreight/' . $this->uri->segment(3)); ?>">Edit Booking(Air)</a></li>
                    <?php } else { ?>
                        <li><a href="<?php echo base_url('invoice/edit_sea_freight/' . $this->uri->segment(3)); ?>">Edit Sea
                                Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_seafreight/' . $this->uri->segment(3)); ?>">Edit Booking(Sea)</a></li>
                    <?php } ?>
                    <li><a href="<?php echo base_url('invoice/edit_acceptance/' . $this->uri->segment(3)); ?>">Edit
                            Acceptance</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_credit_note/' . $this->uri->segment(3)); ?>">Edit
                            Credit Note</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_airwaybill/' . $this->uri->segment(3)); ?>">Edit
                            AirwayBill</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_weight_slip/' . $this->uri->segment(3)); ?>">Edit
                            Weight Slip</a></li>
                    <li><a href="<?php echo base_url('invoice/inventory_list/' . $this->uri->segment(3)); ?>">Inventory List</a></li>
                    <li class="active"><a href="<?php echo base_url('invoice/email/' . $this->uri->segment(3)); ?>">Email</a></li>
                </ul>
            </div>

            <form class="form-horizontal form-bordered" action="" method="post">
                <input type="hidden" name="account_type" value="<?php echo (!empty($detail)?$detail[0]->account_type:""); ?>"/>
                <input type="hidden" name="invoice_number" value="<?php echo (!empty($detail)?$detail[0]->invoice_number:""); ?>"/>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="client_name">Client name</label>

                    <div class="col-md-6">
                        <select id="clientId" name="clientId" class="form-control" size="1">
                            <option selected disabled>Please Select</option>
                            <?php $clients = $this->admin_model->table_fetch_rows('clients', array(), array('name' => 'asc'));
                            foreach ($clients as $c) {
                                ?>
                                <option value="<?php echo $c->id; ?>"
                                        data-email="<?php echo $c->email; ?>"><?php echo $c->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="email">Email Address </label>

                    <div class="col-md-6">
                        <input type="email" id="email" name="email" class="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="subject">Subject </label>

                    <div class="col-md-6">
                        <input type="text" id="subject" name="subject" class="form-control"/>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label" for="attachInvoice">Attach Invoice </label>

                    <div class="col-md-6">
                        <input name="attachInvoice" type="radio" value="Y" checked="checked"/> YES
                        <input name="attachInvoice" type="radio" value="N"/> NO
                        <iframe style="display:none;"
                                src="http://freight-report.divinelogistics.co.nz/pdf.php?id=<?php echo $invoiceId ?>"></iframe>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="attachAuthority">Attach Authority Letter </label>

                    <div class="col-md-6">
                        <input name="attachAuthority" type="radio" value="Y" checked="checked"/> YES
                        <input name="attachAuthority" type="radio" value="N"/> NO
                        <iframe style="display:none;"
                                src="http://freight-report.divinelogistics.co.nz/pdf_authority.php?id=<?php echo $invoiceId ?>"></iframe>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="attachWeightSlip">Attach Weight Slip </label>

                    <div class="col-md-6">
                        <input name="attachWeightSlip" type="radio" value="Y" checked="checked"/> YES
                        <input name="attachWeightSlip" type="radio" value="N"/> NO
                        <iframe style="display:none;"
                                src="http://freight-report.divinelogistics.co.nz/pdf_weight_slip.php?id=<?php echo $invoiceId ?>"></iframe>
                    </div>
                </div>

                <?php if (!empty($detail) && $detail[0]->account_type == "Air") { ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="attachAirFreight">Attach Air Freight </label>

                        <div class="col-md-6">
                            <input name="attachAirFreight" type="radio" value="Y" checked="checked"/> YES
                            <input name="attachAirFreight" type="radio" value="N"/> NO
                            <iframe style="display:none;"
                                    src="http://freight-report.divinelogistics.co.nz/pdf_shipping_letter.php?id=<?php echo $invoiceId ?>"></iframe>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="attachBookingAirFreight">Attach Booking Air
                            Freight </label>

                        <div class="col-md-6">
                            <input name="attachBookingAirFreight" type="radio" value="Y" checked="checked"/> YES
                            <input name="attachBookingAirFreight" type="radio" value="N"/> NO
                            <iframe style="display:none;"
                                    src="http://freight-report.divinelogistics.co.nz/pdf_booking_airfreight.php?id=<?php echo $invoiceId ?>"></iframe>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="attachSeaFreight">Attach Sea Freight </label>

                        <div class="col-md-6">
                            <input name="attachSeaFreight" type="radio" value="Y" checked="checked"/> YES
                            <input name="attachSeaFreight" type="radio" value="N"/> NO
                            <iframe style="display:none;"
                                    src="http://freight-report.divinelogistics.co.nz/pdf_shipping_instruction.php?id=<?php echo $invoiceId ?>"></iframe>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="attachBookingSeaFreight">Attach Booking Sea
                            Freight </label>

                        <div class="col-md-6">
                            <input name="attachBookingSeaFreight" type="radio" value="Y" checked="checked"/> YES
                            <input name="attachBookingSeaFreight" type="radio" value="N"/> NO
                            <iframe style="display:none;"
                                    src="http://freight-report.divinelogistics.co.nz/pdf_booking_seafreight.php?id=<?php echo $invoiceId ?>"></iframe>
                        </div>
                    </div>

                <?php } ?>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="attachAcceptanceForm">Attach Acceptance Form </label>

                    <div class="col-md-6">
                        <input name="attachAcceptanceForm" type="radio" value="Y" checked="checked"/> YES
                        <input name="attachAcceptanceForm" type="radio" value="N"/> NO
                        <iframe style="display:none;"
                                src="http://freight-report.divinelogistics.co.nz/pdf_acceptance_questionaire.php?id=<?php echo $invoiceId ?>"></iframe>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="attachInventoryList">Attach Inventory List </label>

                    <div class="col-md-6">
                        <input name="attachInventoryList" type="radio" value="Y" checked="checked"/> YES
                        <input name="attachInventoryList" type="radio" value="N"/> NO
                        <iframe style="display:none;"
                                src="http://freight-report.divinelogistics.co.nz/pdf_inventory_list.php?id=<?php echo $invoiceId ?>"></iframe>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="attachAirwaybill">Attach Airway Bill </label>

                    <div class="col-md-6">
                        <input name="attachAirwaybill" type="radio" value="Y" checked="checked"/> YES
                        <input name="attachAirwaybill" type="radio" value="N"/> NO
                        <iframe style="display:none;"
                                src="http://freight-report.divinelogistics.co.nz/pdf_airwaybill.php?id=<?php echo $invoiceId ?>"></iframe>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="message">Message</label>

                    <div class="col-md-6">
                        <textarea name="message" id="message" class="form-control required" rows="6"
                                  cols="40"></textarea>
                    </div>
                </div>


                <table border='1' class="invoice_table" width="60%" cellpadding='0' cellspacing='0' style="margin-left:200px;">
                    <tr style='background:#000; color:#fff;'>
                        <td>Upload Other Documents</td>
                        <td><input type="button" id="add_more"  class="btn-default" value="ADD MORE"/></td>
                    </tr>
                </table>
                <div id="more"></div><br>

                <input class="btn-primary" type="submit" value="Send!" name="send_email" id="send_email" style="margin-left:200px;"/>
            </form>


        </div>
    </div>
</div>

<script language="javascript">
    $(document).ready(function()
    {
        //$("#frmemail").validate();
        $("#clientId").change(function(){
            var element = $("option:selected", this);
            var myTag = element.attr("data-email");
            $("#email").val(myTag);
        });
        $("#add_more").click(function() {
            $("#more").append('<table class="invoice_inner_table invoice_table" width="60%" border="1" style="margin-left:200px;" cellpadding="0" cellspacing="0"><tr><td><input type="file" name="documents[]" /></td></tr></table>');
        });
        $("#add_more").trigger('click');
    });
</script>