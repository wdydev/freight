<div class="row">
    <div class="row col-md-12">
        <div class="block">
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li><a href="<?php echo base_url('invoice/edit/' . $this->uri->segment(3)); ?>">Edit Invoice</a></li>
                    <?php if (!empty($detail) &&($detail[0]->account_type == "Air")){ ?>
                        <li><a href="<?php echo base_url('invoice/edit_air_freight/' . $this->uri->segment(3)); ?>">Edit
                                Air Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_airfreight/' . $this->uri->segment(3)); ?>">Edit Booking(Air)</a></li>
                    <?php } else { ?>
                        <li class="active"><a href="<?php echo base_url('invoice/edit_sea_freight/' . $this->uri->segment(3)); ?>">Edit Sea
                                Freight</a></li>
                        <li><a href="<?php echo base_url('invoice/edit_booking_seafreight/' . $this->uri->segment(3)); ?>">Edit Booking(Sea)</a></li>
                    <?php } ?>
                    <li><a href="<?php echo base_url('invoice/edit_acceptance/' . $this->uri->segment(3)); ?>">Edit
                            Acceptance</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_credit_note/' . $this->uri->segment(3)); ?>">Edit
                            Credit Note</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_airwaybill/' . $this->uri->segment(3)); ?>">Edit
                            AirwayBill</a></li>
                    <li><a href="<?php echo base_url('invoice/edit_weight_slip/' . $this->uri->segment(3)); ?>">Edit
                            Weight Slip</a></li>
                    <li><a href="<?php echo base_url('invoice/inventory_list/' . $this->uri->segment(3)); ?>">Inventory List</a></li>
                    <li><a href="<?php echo base_url('invoice/email/' . $this->uri->segment(3)); ?>">Email</a></li>
                </ul>
            </div>

            <?php if (isset($message)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
            <?php } ?>

            <form action="" method="post" class="form-horizontal form-bordered">
                <input type="hidden" name="invoiceId" value="<?php echo $this->uri->segment(3);?>" />
                <table border='1' class="invoice_table table" width="100%" cellpadding='0' cellspacing='0'>
                    <tr>
                        <td><b>SHIPPER</b></td>
                        <td colspan='2'><span style='font-size:18px;font-weight:bold;'>SHIPPING INSTRUCTION</span></td>
                        <td rowspan='2'><b>BOOKING NO:</b><br/><br/><?php echo (!empty($detail)?$detail[0]->invoice_number:""); ?></td>
                    </tr>
                    <tr>
                        <td>
                            <select id="clientId" name="clientId" class="form-control" size="1">
                                <option selected disabled>Please Select</option>
                                <?php $clients = $this->admin_model->table_fetch_rows('clients', array(), array('name' => 'asc'));
                                foreach ($clients as $c) {
                                    if ($rsResults[0]->clientId == ($c->id)) {
                                        ?>
                                        <option value="<?php echo $c->id; ?>"
                                                selected="selected"><?php echo $c->name; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $c->id; ?>"><?php echo $c->name; ?></option>
                                    <?php }
                                } ?>
                            </select>
                            <textarea class="form-control" name="shipper" rows="3"><?php echo (!empty($rsResults)?$rsResults[0]->shipper:""); ?></textarea>
                        </td>
                        <td colspan='2' style='width:450px;line-height:150%; font-size:12px;'>
                            <img align='left' style=' width:100px;'
                                 src='<?php echo base_url('assets/img/logo_04.png'); ?>'/>

                            <div style='padding-left:100px;'>DIVINE LOGISTICS LTD<br/>
                                Unit-3,61 Crooks Road<br/>East Tamaki, Auckland, NZ <br/>
                                PO Box: 76877 Manukau City-2241<br/>

                                Phone: 09-263 7439 / 2151601<br/>
                                Fax: 09-263 7438<br/></div>
                        </td>
                    </tr>
                    <tr>
                        <td><b>CONSIGNEE:</b></td>
                        <td colspan='3' rowspan='3' style='width:450px;line-height:120%;font-size:11px;'>
                            Received by the carrier from the shipper in apparant good order and condition ( Unless
                            otherwise noted herein) the total numbers or quantity of containers or unit/packages
                            indicated stated by the shipper to comprise the good specified below for carriage
                            subject to the all the terms and conditions (Available on our website) from the place
                            of reciept or the port of loading whichever is applicable,to the port of discharge
                            or the place of delivery whichever is applicable. In accepting this Bill Of Lading
                            the merchant expressly accepts and agrees to all its terms conditions exceptions
                            whether printed stamped or written otherwise incorporated notwithstanding
                            the non signing of this Bill of Lading by the Merchant
                        </td>
                    </tr>
                    <tr>
                        <td class="lefttd">
                            <textarea name="consignee" class="form-control" rows="3"><?php echo  (!empty($rsResults)?$rsResults[0]->consignee:""); ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><b>NOTIFY PARTY:</b></td>
                    </tr>
                    <tr>
                        <td rowspan='6'>
                            <input type="text" class="form-control" name="notifyParty" value="<?php echo  (!empty($rsResults)?$rsResults[0]->notifyParty:""); ?>"/>
                        </td>
                        <td colspan='3'><b>
                            Total no. Of container/Packages: </b><input type="text" class="form-control" name="noOfContainer"
                                                                    value="<?php echo  (!empty($rsResults)?$rsResults[0]->noOfContainer:""); ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td width='150'><b>Vessel and Voyage No.</b></td>
                        <td width='260'><input type="text" class="form-control" name="vesselVoyageNo"
                                               value="<?php echo  (!empty($rsResults)?$rsResults[0]->vesselVoyageNo:""); ?>"/></td>
                        <td width='88'></td>
                    </tr>
                    <tr>
                        <td><b>Place of Receipt</b></td>
                        <td colspan='2'><b>Port of Loading</b></td>
                    </tr>
                    <tr>
                        <td><input type="text" class="form-control" name="placeReceipt" value="<?php echo (!empty($rsResults)?$rsResults[0]->placeReceipt:"") ;?>"/></td>
                        <td colspan='2'><input type="text" class="form-control" name="portLoading"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->portLoading:""); ?>"/></td>
                    </tr>
                    <tr>
                        <td><b>Port of Discharge</b></td>
                        <td colspan='2'><b>Final Destination</b></td>
                    </tr>
                    <tr>
                        <td><input type="text" class="form-control" name="portDischarge" value="<?php echo (!empty($rsResults)?$rsResults[0]->portDischarge:"") ;?>"/></td>
                        <td colspan='2'><input type="text" class="form-control" name="finalDestination"
                                               value="<?php echo (!empty($rsResults)?$rsResults[0]->finalDestination:""); ?>"/></td>
                    </tr>
                </table>

                <table class="invoice_table table" width="80" cellpadding='0' cellspacing='0'>
                    <tr style='background:#000; color:#fff;'>
                        <td>ITEM NO.</td>
                        <td>Marks and No.</td>
                        <td>Kind of Packages</td>
                        <td>Description</td>
                        <td>Net Weight</td>
                        <td>Gross Weight</td>
                        <td>Total Cubic Measurement</td>
                    </tr>
                    <?php  $weight = 0; $volume = 0;
                if(is_array($wsResults) && !empty($wsResults)){
                foreach($wsResults as $result){?>
                    <tr>
                        <td>
                            <input type="checkbox" name="remove[]" value="<?php echo $result->id;?>" />
                            <input type="hidden" name="id[]" value="<?php echo $result->id;?>" />
                            <input type="text" class="form-control" name="itemNo[]" value="<?php echo $result->itemNo;?>" />
                        </td>
                        <td><input type="text" class="form-control" name="marksAndNo[]" value="<?php echo $result->marksAndNo;?>"  /></td>
                        <td><input type="text" class="form-control" name="kindOfPackages[]" value="<?php echo $result->kindOfPackages;?>"  /></td>
                        <td><input type="text" class="form-control" name="description[]" value="<?php echo $result->description;?>" /></td>
                        <td><input type="text" class="form-control" name="netWeight[]" value="<?php echo $result->netWeight;?>"   /></td>
                        <td><input type="text" class="form-control" name="grossWeight[]" value="<?php echo $result->grossWeight;?>"  /></td>
                        <td><input type="text" class="form-control" name="totalCubic[]" value="<?php echo $result->totalCubic;?>"  /></td>
                    </tr>
                </table>
                <?php
                //$weight += $result['weight']; $volume += $result['volume'];
                } }  ?>
                <div id="more"></div>
                <table border='1' class='invoice_table table' width="100%" cellpadding='0' cellspacing='0'>
                    <tr>
                        <td><b>VALUE FOR CUSTOMS NZ$</b><br/><br/><input type="text" class="form-control" name="nzAmt" value="<?php echo (!empty($rsResults)?$rsResults[0]->nzAmt:"") ;?>" /></td>
                        <td width='40'>&nbsp;</td>
                        <td width='40'>&nbsp;</td>
                        <td width='40'>&nbsp;</td>
                        <td colspan='3'>&nbsp;</td>
                    </tr>
                    <tr style='background:#000; color:#fff;'>
                        <td width='160'><b>FREIGHT CHARGES</b></td>
                        <td width='40'><b>PREPAID</b></td>
                        <td width='40'><b>COLLECT</b></td>
                        <td width='40'>&nbsp;</td>
                        <td width='106'><b>TYPE OF SERVICE</b></td>
                        <td width='40'>&nbsp;</td>
                        <td width='170'><b>SPECIAL INSTRUCTION</b></td>

                    </tr>
                    <tr>
                        <td><b>1. NZ ZONE SERVICE</b></td>
                        <td><input type="radio" name="nzZoneService" value="P" <?php echo (!empty($rsResults) && $rsResults[0]->nzZoneService=="P"?"checked":"");?> />Prepaid
                        </td>
                        <td><input type="radio" name="nzZoneService" value="C" <?php echo (!empty($rsResults) && $rsResults[0]->nzZoneService=="C"?"checked":""); ?> />Collect</td>
                        <td rowspan='7'>&nbsp;</td>
                        <td><b>FCL/FCL</b></td>
                        <td><input type="text" name="service1" value="<?php echo (!empty($rsResults)?$rsResults[0]->service1:"");?>" /></td>
                        <td  rowspan='7'><input type="text"  class="form-control" name="specialInstruction" value="<?php echo (!empty($rsResults)? $rsResults[0]->specialInstruction:"");?>" /></td>
                    </tr>
                    <tr>
                        <td><b>2. NZ TERM CHARGE</b></td>
                        <td><input type="radio" name="nzTermCharge" value="P" <?php echo (!empty($rsResults) && $rsResults[0]->nzTermCharge=="P"?"checked":""); ?> />Prepaid
                        </td>
                        <td><input type="radio" name="nzTermCharge" value="C" <?php echo (!empty($rsResults) && $rsResults[0]->nzTermCharge=="C"?"checked":""); ?> />Collect</td>

                        <td><b>FCL/LCL</b></td>
                        <td><input type="text" class="form-control" name="service1" value="<?php echo (!empty($rsResults)?$rsResults[0]->service2:"");?>" /></td>

                    </tr>
                    <tr>
                        <td><b>3. OCEAN FREIGHT</b></td>
                        <td><input type="radio" name="oceanFreight" value="P" <?php echo (!empty($rsResults) && $rsResults[0]->oceanFreight=="P"?"checked":""); ?> />Prepaid
                        </td>
                        <td><input type="radio" name="oceanFreight" value="C" <?php echo (!empty($rsResults) && $rsResults[0]->oceanFreight=="C"?"checked":"");?> />Collect</td>

                        <td><b>LCL/FCL</b></td>
                        <td><input type="text" class="form-control" name="service3" value="<?php echo (!empty($rsResults)? $rsResults[0]->service3:"");?>" /></td>

                    </tr>
                    <tr>
                        <td><b>4. DEST THC</b></td>
                        <td><input type="radio" name="descThc" value="P" <?php echo (!empty($rsResults) && $rsResults[0]->descThc=="P"?"checked":""); ?> />Prepaid
                        </td>
                        <td><input type="radio" name="descThc" value="C" <?php echo (!empty($rsResults) && $rsResults[0]->descThc=="C"?"checked":""); ?> />Collect</td>

                        <td><b>LCL/LCL</b></td>
                        <td><input type="text" class="form-control" name="service4" value="<?php echo (!empty($rsResults)? $rsResults[0]->service4:"");?>" /></td>

                    </tr>
                    <tr>
                        <td><b>5. DEST ZONE ONC</b></td>
                        <td><input type="radio" name="destZoneThc" value="P" <?php echo (!empty($rsResults) && $rsResults[0]->destZoneThc=="P"?"checked":""); ?> />Prepaid
                        </td>
                        <td><input type="radio" name="destZoneThc" value="C" <?php echo (!empty($rsResults) && $rsResults[0]->destZoneThc=="C"?"checked":""); ?> />Collect</td>

                        <td><b>BREAK BULK</b></td>
                        <td><input type="text" class="form-control" name="service5" value="<?php echo (!empty($rsResults)? $rsResults[0]->service5:"");?>" /></td>

                    </tr>
                    <tr>
                        <td><b>6. OTHER CHARGES</b></td>
                        <td><input type="radio" name="otherCharges" value="P" <?php echo (!empty($rsResults) && $rsResults[0]->otherCharges=="P"?"checked":"");?> />Prepaid
                        </td>
                        <td><input type="radio" name="otherCharges" value="C" <?php echo (!empty($rsResults) && $rsResults[0]->otherCharges=="C"?"checked":""); ?> />Collect</td>

                        <td><b>RORO</b></td>
                        <td><input type="text" class="form-control" name="service6" value="<?php echo (!empty($rsResults)? $rsResults[0]->service6:"");?>" /></td>

                    </tr>
                </table>
                <table border='1' class='invoice_table table' cellpadding='0' cellspacing='0' width="100%">
                    <tr>
                        <td width='270'><b>IS THE SHIPMENT DANGEROUS GOODS</b><br/><br/>
                            <input type="radio" name="shipmentDangerous" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]->shipmentDangerous=="Y"?"checked":"");?> />YES &nbsp;
                            <input type="radio" name="shipmentDangerous" value="N" <?php echo (!empty($rsResults) && $rsResults[0]->shipmentDangerous=="N"?"checked":"");?> />NO
                            <br/><br/><br/>
                            <span style='font-size:10px;'>Any part of the consignment contains dangerous goods,such part is properly described by name and is NAME in proper condition for carriage by sea according to the applicable dangerous goods regulation.</span>
                        </td>
                        <td  width='258'>
                            <b>FOR THE SHIPPER<br/><br/>
                                NAME</b><br/>
                            <input type="text" name="shipperName" class="form-control" value="<?php echo (!empty($rsResults)? $rsResults[0]->shipperName:"");?>" />
                            <br/><br/>AT  <input type="text" class="form-control" name="at" value="<?php echo (!empty($rsResults)? $rsResults[0]->at:"");?>" />
                            <br/><br/>DATED <input type="text"  class=" form-control input-datepicker" data-date-format="yyyy-mm-dd" name="dated" id="dated" value="<?php echo (!empty($rsResults)? ((($rsResults[0]->dated=='')||$rsResults[0]->dated == '0000-00-00')?date('Y-m-d') :$rsResults[0]->dated):"") ;?>" />
                        </td>
                        <td width='160'><b>NO. OF ORIGINAL <br/>B/Ls</b><br/><br/><input type="text" class="form-control" name="originalBL" value="<?php echo (!empty($rsResults)? $rsResults[0]->originalBL:"");?>" /></td>
                    </tr>
                    <tr>
                        <td colspan='3' style='font-size:12px;'>ALL BUSINESS IS TRANSACTED UNDER THE COMPANY'S STANDARD TRADING CONDITIONS OBTAINABLE ON APPLICATION.
                            <br/>
                            <input type="checkbox" name="acceptTC" value="Y" <?php echo (!empty($rsResults) && $rsResults[0]->acceptTC=="Y"?"checked":"")?> /> I accept to all the terms and conditions as on website
                        </td>
                    </tr>
                </table>
                <br/>
                <table class="invoice_inner_table invoice_table table" id="invoice_description" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <input type="button"  id="add_more" value="ADD MORE" />&nbsp;
                            <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i class="fa fa-check"></i> Update</button>
                            <br/>
                            <a target="_blank" href="<?php echo base_url('invoice/pdf_edit_seafreight?invoiceId='.$this->uri->segment(3));?>">PREVIEW PDF</a>
                        </td>
                    </tr>
                </table>
            </form>

        </div>
    </div>
</div>

<script language="javascript">
    $(document).ready(function() {
        $("#add_more").click(function() {
            $("#more").append('<table class="invoice_table table" width="100%" cellpadding="0" cellspacing="0"><tr><td><input type="hidden" name="id[]" value="0"/><input type="text" class="form-control" name="itemNo[]"   /></td><td><input type="text" class="form-control" name="marksAndNo[]"/></td><td><input type="text" class="form-control" name="kindOfPackages[]" /></td><td><input type="text" class="form-control" name="description[]" /></td><td><input type="text" class="form-control" name="netWeight[]"  /></td><td><input type="text" class="form-control" name="grossWeight[]" /></td><td><input type="text" class="form-control" name="totalCubic[]" /></td></tr></table>');
        });
        //$("#add_more").trigger('click');

    });
</script>

