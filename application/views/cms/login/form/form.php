<!-- Login Header -->
<h1 class="h2 text-light text-center push-top-bottom animation-slideDown">
    <i class="fa fa-cube"></i> <strong>Login</strong>
</h1>
<!-- END Login Header -->

<!-- Login Block -->
<div class="block animation-fadeInQuickInv">
    <!-- Login Title -->
    <div class="block-title">
        <h2>Please Login</h2>
    </div>
    <!-- END Login Title -->

    <?php if(isset($message)){ ?>
        <div class="alert alert-danger alert-dismissable">
            <?php
            echo $message;
            ?>
        </div>
        <?php
    }?>

    <?php if(validation_errors()){?>
        <div class="alert alert-danger alert-dismissable">
            <?php
            echo validation_errors();
            ?>
        </div>
    <?php }?>

    <!-- Login Form -->
    <?php echo form_open("login/verify",'class="form-horizontal"'); ?>

    <div class="form-group">
        <div class="col-xs-12">
            <input type="text" id="user_email" name="user_email" class="form-control" placeholder="Your email..">
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <input type="password" id="user_password" name="user_password" class="form-control" placeholder="Your password..">
        </div>
    </div>
    <div class="form-group form-actions">
        <div class="col-xs-4 text-right">
            <button type="submit" class="btn btn-effect-ripple btn-sm btn-primary"><i class="fa fa-check"></i> Let's Go</button>
        </div>
    </div>
    </form>
    <!-- END Login Form -->
</div>
<!-- END Login Block -->

