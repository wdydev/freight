<div class="row">
    <div class="row col-md-12">
        <div class="block">
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li><a href="<?php echo base_url('reports/add_opening_blnc/'); ?>">Add Opening Balance</a></li>
                    <li class="active"><a href="<?php echo base_url('reports/view_opening_blnc/'); ?>">opening Balance
                            List</a></li>
                </ul>
            </div>

            <?php if (isset($message)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
                <?php
            } ?>

            <?php if (validation_errors()) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <?php
                    echo validation_errors();
                    ?>
                </div>
            <?php } ?>

            <form action="" method="post" class="form-horizontal form-bordered">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="client_name">Search By Client name</label>

                    <div class="col-md-6">
                        <select id="clientId" name="clientId" class="form-control" size="1">
                            <option selected disabled>Please Select</option>
                            <?php $clients = $this->admin_model->table_fetch_rows('clients', array(), array('name' => 'asc'));
                            foreach ($clients as $c) {
                                ?>
                                <option value="<?php echo $c->id; ?>"><?php echo $c->name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i
                                class="fa fa-search"></i> Search
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


    <div class="block full">

        <div class="table-responsive">
            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 100px;">Serial No</th>
                    <th>Client Name</th>
                    <th>Amount</th>
                    <th class="text-center" style="width: 125px;"><i class="fa fa-flash"></i></th>
                </tr>
                </thead>
                <tbody>
                <?php $amtTotal = 0;
                if (is_array($query) && !empty($query)) {
                    foreach ($query as $k => $row) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $k + 1; ?></td>
                            <td><?php $client_name = $this->admin_model->table_fetch_row('clients', array('id' => $row->clientId));
                                echo $client_name[0]->name; ?></td>
                            <td><?php echo $row->amount; ?></td>

                            <td class="text-center">
<!--                                <a href="--><?php //echo base_url('reports/edit_opening_blnc/' . $row->id); ?><!--"-->
<!--                                   data-toggle="tooltip" title="edit"-->
<!--                                   class="btn btn-effect-ripple btn-xs btn-success"><i class="fa fa-pencil"></i></a>-->

                                <a href="#" data-id="<?php echo $row->id ;?>" data-href="<?php //echo base_url('users/edit/' . $row->id); ?>"
                                   data-toggle="modal" title="edit"
                                   data-target="#modal-large" class="btn btn-effect-ripple btn-xs btn-success edit_opening_blnc"><i
                                        class="fa fa-pencil"></i></a>


                                <a href="#"
                                   data-href="<?php echo base_url('reports/delete_opening_blnc/' . $row->id); ?>"
                                   data-toggle="modal" data-name="<?php echo $client_name[0]->name; ?>"
                                   title="delete" data-target="#confirm-delete"
                                   class="btn btn-effect-ripple btn-xs btn-danger del-row"><i
                                        class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <?php $amtTotal = $amtTotal + $row->amount;
                    }
                } ?>
                <tr>
                    <td></td>
                    <td class="text-center"><b>Total : </b></td>
                    <td><b><?php echo $amtTotal; ?> </b></td>
                </tr>
                </tbody>
            </table>
        </div>

        <form action="" method="post" class="form-horizontal form-bordered">
            <div id="modal-large" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content col-md-12">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>

                        <?php $this->load->view('cms/reports/opening_balance/edit/edit');?>

                        <div class="modal-footer">
                            <button type="submit" class="btn btn-effect-ripple btn-primary"><i class="fa fa-check"></i>Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div id="confirm-delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><span id="del_name"></span></h4>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this record?
                    </div>
                    <div class="modal-footer">

                        <a class="btn btn-effect-ripple btn-danger">Delete</a>
                        <button type="button" data-dismiss="modal" class="btn btn-effect-ripple btn-default"
                                data-dismiss="modal">Cancel
                        </button>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $(function () {
        $('a.del-row').click(function (e) {
            var name = $(this).attr('data-name');
            $('span#del_name').html(name);
        });
    });

    $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-danger').attr('href', $(e.relatedTarget).data('href'));
    });

    $(document).ready(function () {
        $('a.edit_opening_blnc').click(function (e) {
            var id = $(this).attr('data-id');
            console.log(id);
            var data = new Array();
            $.ajax({
                url: '<?php echo base_url("reports/edit_opening_blnc");?>',
                type: "post",
                data: {id: id},
                dataType: 'json',
                success: function (response) {
                    data = response;
                    $("#account_id").val(data[0].id);
                    $("#amount").val(data[0].amount);
                }
            });
        });
    });

</script>













