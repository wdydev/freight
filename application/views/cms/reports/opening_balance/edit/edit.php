<div class="row">
    <div class="col-md-2"></div>
    <div class="row col-md-8">
        <div class="block" >

            <?php if(isset($message)){ ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
                <?php
            }?>

            <?php if(validation_errors()){?>
                <div class="alert alert-danger alert-dismissable">
                    <?php
                    echo validation_errors();
                    ?>
                </div>
            <?php }?>

<!--            <form action="" method="post" class="form-horizontal form-bordered">-->
            <input type="hidden" name="opening_blnc_id" id="opening_blnc_id" />
                <div class="form-group">
                    <label class="col-md-3 control-label" for="client_name">Client name</label>
                    <div class="col-md-6">
                        <select id="clientId" name="clientId" class="form-control" size="1">
                            <option selected disabled>Please Select</option>
                            <?php $clients = $this->admin_model->table_fetch_rows('clients',array(),array('name'=>'asc'));
                            foreach($clients as $c){
                                if(($query[0]->clientId) == ($c->id)){?>
                                <option value="<?php echo $c->id;?>" selected="selected"><?php echo $c->name;?></option>
                            <?php  }else {?>
                                <option value="<?php echo $c->id;?>"><?php echo $c->name;?></option>
                            <?php } }?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="amount">Amount</label>
                    <div class="col-md-6">
                        <input type="text" id="amount" name="amount" pattern="[0-9\.]+" title="Should be an integer" class="form-control" value="" >
                    </div>
                </div>

<!--                <div class="form-group form-actions">-->
<!--                    <div class="col-md-9 col-md-offset-3">-->
<!--                        <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i class="fa fa-check"></i> Submit</button>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </form>-->
        </div>
    </div>
</div>

