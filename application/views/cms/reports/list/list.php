<div class="row">
    <div class="row col-md-12">
        <div class="block">
            <div class="block-title">
                <h2>Reports List</h2>
            </div>

            <?php if (isset($message)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
                <?php
            } ?>

            <?php if (validation_errors()) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <?php
                    echo validation_errors();
                    ?>
                </div>
            <?php } ?>

            <form action="" method="post" class="form-horizontal form-bordered">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="clientId">Search By Client name</label>

                    <div class="col-md-6">
                        <select id="clientId" name="clientId" class="form-control" size="1">
                            <option selected disabled>Please Select</option>
                            <?php $clients = $this->admin_model->table_fetch_rows('clients', array(), array('name' => 'asc'));
                            foreach ($clients as $c) {
                                if($query[3] == ($c->id)){?>
                                    <option value="<?php echo $c->id;?>" selected="selected"><?php echo $c->name;?></option>
                                <?php  }else {?>
                                    <option value="<?php echo $c->id;?>"><?php echo $c->name;?></option>
                                <?php } }?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="startDate">Start Date</label>

                    <div class="col-md-6">
                        <input type="text" id="startDate" name="startDate" class="form-control input-datepicker"
                               data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo ($query[4]!=NULL)?$query[4]:'';?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="endDate">End Date</label>

                    <div class="col-md-6">
                        <input type="text" id="endDate" name="endDate" class="form-control input-datepicker"
                               data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo ($query[5]!=NULL)?$query[5]:date('Y-m-d');?>">
                    </div>
                </div>

                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i
                                class="fa fa-search"></i> Search
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="block full">
        <div class="table-responsive">
            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 100px;">Serial No</th>
                    <th>Invoice Date</th>
                    <th>Invoice No.</th>
                    <th>Container No.</th>
                    <th>Note</th>
                    <th>Debit</th>
                    <th>Credit</th>
                    <th>Balance</th>
                </tr>
                </thead>
                <tbody>
                <?php $grand_total = 0;
                $paidTotal = 0;
                $balanceTotal = 0;
                $credit_total = 0;
                $balance_total = 0;
                $amtTotal = 0; ?>
                <tr>
                    <td></td>
                    <td colspan="4" class="text-right"><b> Opening balance : </b></td>
                    <td><b><?php echo $query[1]; ?> </b></td>
                    <td><b><?php echo $query[2]; ?> </b></td>
                    <td><b><?php echo $query[1] - $query[2]; ?> </b></td>
                </tr>
                <?php if (is_array($query[0]) && !empty($query[0])) {
                    foreach ($query[0] as $k => $row) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $k + 1; ?></td>
                            <td><?php echo date("Y M d",strtotime($row['invoice_date'])); ?></td>
                            <td><?php echo $row['invoice_number']; ?></td>
                            <td><?php echo $row['container_number']; ?></td>

                            <td> <?php $paidTotal = 0;
                                $creditNoteTotal = 0;
                                if ($row['amount'] == "0.00" && !empty($row['invoice_number'])) {
                                    $result = $this->admin_model->creditNoteTotal($row['id']);
                                    $gst = 0;
                                    $total = 0;
                                    foreach ($result as $res) {

                                        $gst = $gst + ($res['gst'] == 15) ? (($res['charges'] * 15) / 100) : 0;
                                        $total = $total + $res['charges'] + $gst;
                                    }
                                    $creditNoteTotal = $total;
                                    $paidTotal += $creditNoteTotal;
                                }
                                ?></td>
                            <td align="right">
                                <?php
                                if ($row['amount'] == "0.00" && !empty($row['invoice_number'])) {
                                    $result = $this->admin_model->invoiceDescription($row['id']);
                                    $gst = 0;
                                    $total = 0;
                                    foreach ($result as $res) {
                                        $gst = ($res['gst'] == 15) ? (($res['charges'] * 15) / 100) : 0;
                                        $total = $total + $res['charges'] + $gst;
                                    }
                                    $total = $total - $row['discount'];
                                    $grand_total += $total;
                                    echo $total;


                                } else {
                                    $paidTotal += $row['amount'];
                                }
                                ?>
                                <?php ?>

                            </td>
                            <td align="right"><?php echo $row['amount']+$creditNoteTotal ; ?></td>
                            <td></td>
                        </tr>
                    <?php } }
                $totalRes = ($query[1] + $grand_total) - ($query[2] + $paidTotal);
                if ($totalRes > 0){ }else{ $totalRes = "0.00"; }
                ?>
                <tr>
                    <td></td>
                    <td colspan="4" class="text-right"><b> Grand Total : </b></td>
                    <td><b><?php echo $query[1] + $grand_total; ?> </b></td>
                    <td><b><?php echo $query[2] + $paidTotal; ?> </b></td>
                    <td><b><?php echo $totalRes; ?> </b></td>
                </tr>
                </tbody>
            </table>
        </div>
        <a target="_blank" href="<?php echo base_url('reports/pdf_reports?clientId='.$query[3].'&startDate='.$query[4].'&endDate='.$query[5]);?>">View PDF</a>
    </div>



