<div class="row">
    <div class="col-md-2"></div>
    <div class="row col-md-8">
        <div class="block" >
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="<?php echo base_url('reports/add_account/'); ?>">Add Account</a></li>
                    <li><a href="<?php echo base_url('reports/view_account/');?>">Account
                            List</a></li>
                </ul>
            </div>

            <?php if(isset($message)){ ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
                <?php
            }?>

            <?php if(validation_errors()){?>
                <div class="alert alert-danger alert-dismissable">
                    <?php
                    echo validation_errors();
                    ?>
                </div>
            <?php }?>

            <form action="" method="post" class="form-horizontal form-bordered">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="client_name">Client name</label>
                    <div class="col-md-6">
                        <select id="clientId" name="clientId" class="form-control" size="1">
                            <option selected disabled>Please Select</option>
                            <?php $clients = $this->admin_model->table_fetch_rows('clients',array(),array('name'=>'asc'));
                            foreach($clients as $c){?>
                                <option value="<?php echo $c->id;?>"><?php echo $c->name;?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="amount">Amount</label>
                    <div class="col-md-6">
                        <input type="text" id="amount" name="amount" pattern="[0-9\.]+" title="Should be an integer" class="form-control" placeholder="Enter amount">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="paidDate">Paid Date</label>
                    <div class="col-md-6">
                        <input type="text" id="paidDate" name="paidDate" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" value="<?php echo date('Y-m-d');?>">
                    </div>
                </div>

                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i class="fa fa-check"></i> Submit</button>
                        <button type="reset" href="<?php echo base_url('reports/add_account');?>" class="btn btn-effect-ripple btn-danger"><i class="fa fa-times"></i> Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
