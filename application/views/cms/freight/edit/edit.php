<div class="row">
    <div class="col-md-2"></div>
    <div class="row col-md-8">
        <div class="block" >

            <?php if(isset($message)){ ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
                <?php
            }?>

            <?php if(validation_errors()){?>
                <div class="alert alert-danger alert-dismissable">
                    <?php
                    echo validation_errors();
                    ?>
                </div>
            <?php }?>

<!--            <form action="" method="post" class="form-horizontal form-bordered">-->
            <input type="hidden" id="freight_id" name="freight_id" value="" />
                <div class="form-group">
                    <label class="col-md-3 control-label" for="date">Date</label>
                    <div class="col-md-9">
                        <input required type="text" id="booking_date" name="booking_date" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" value=" ">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="customer_name">Customer name</label>
                    <div class="col-md-9">
                        <input required type="text" id="customer_name" name="customer_name" class="form-control" value="">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Carriage</label>
                    <div class="col-md-9">
                        <label for="carriage" class="radio-inline">
                            <input type="radio" value="N" name="carriage" id="carriage_n"  /> N/A
                        </label>
                        <label for="carriage" class="radio-inline">
                            <input type="radio" value="I" name="carriage" id="carriage_import" /> Import
                        </label>
                        <label for="carriage" class="radio-inline">
                            <input type="radio" value="E" name="carriage" id="carriage_export"  /> Export
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="container_package">Container/Packages</label>
                    <div class="col-md-9">
                        <input required type="text" id="container_package" name="container_package" class="form-control" value=" ">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="container_number">Container No.</label>
                    <div class="col-md-9">
                        <input required type="text" id="container_number" name="container_number" class="form-control" value=" ">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="mawb">MAWB NO.</label>
                    <div class="col-md-9">
                        <input required type="text" id="mawb" name="mawb" class="form-control" value=" ">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="booking_reference">Divine Booking Reference</label>
                    <div class="col-md-9">
                        <input required type="text" id="booking_reference" name="booking_reference" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="carrier_booking_ref">Carrier Booking Reference</label>
                    <div class="col-md-9">
                        <input required type="text" id="carrier_booking_ref" name="carrier_booking_ref" class="form-control" value=" ">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="shipper">Shipper Name</label>
                    <div class="col-md-9">
                        <input required type="text" id="shipper" name="shipper" class="form-control" value=" ">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="consignee">Consignee Name</label>
                    <div class="col-md-9">
                        <input required type="text" id="consignee" name="consignee" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="origin">Origin</label>
                    <div class="col-md-9">
                        <input required type="text" id="origin" name="origin" class="form-control" value=" ">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="destination">Destination</label>
                    <div class="col-md-9">
                        <input required type="text" id="destination" name="destination" class="form-control" value=" ">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="bol">Bill of lading</label>
                    <div class="col-md-9">
                        <input required type="text" id="bol" name="bol" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="vessel">Vessel</label>
                    <div class="col-md-9">
                        <input required type="text" id="vessel" name="vessel" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="etadate">ETA</label>
                    <div class="col-md-9">
                        <input type="text" id="eta" name="eta" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" value=" ">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="comments">Comments</label>
                    <div class="col-md-9">
                    <textarea id="comments" rows="3" cols="47" name="comments" class="form-control"
                              placeholder="Enter comments"></textarea>
                    </div>
                </div>

<!--                <div class="form-group form-actions">-->
<!--                    <div class="col-md-9 col-md-offset-3">-->
<!--                        <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i class="fa fa-check"></i> Submit</button>-->
<!--                        <button type="reset" href="--><?php //echo base_url('freight/edit');?><!--" class="btn btn-effect-ripple btn-danger"><i class="fa fa-times"></i> Reset</button>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </form>-->
        </div>
    </div>
</div>
