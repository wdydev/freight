<div class="block full">
    <div class="block-title">
        <ul class="nav nav-tabs">
            <li><a href="<?php echo base_url('freight/add/'); ?>">Add Freight</a></li>
            <li class="active"><a href="<?php echo base_url('freight/view/');?>">Freight
                    List</a></li>
        </ul>
    </div>

    <?php if (isset($message)) { ?>
        <div class="alert alert-success alert-dismissable">
            <?php
            echo $message;
            ?>
        </div>
        <?php
    } ?>

    <div class="table-responsive">
        <table id="freight_table" class="table table-bordered table-vcenter">
            <thead>
            <tr>
                <th class="text-center">S.N.</th>
                <th>Booking Date</th>
                <th>Customer_name</th>
                <th>Carriage</th>
                <th>Container Package</th>
                <th>Container Number</th>
                <th>MAWB</th>
                <th>Booking Reference</th>
                <th>Carrier Booking Reference</th>
                <th>Shipper</th>
                <th>Consignee</th>
                <th>Origin</th>
                <th>Destination</th>
                <th>Bill of Lading</th>
                <th>Vessel</th>
                <th>ETA</th>
                <th>Comments</th>
                <th class="text-center" style="width:150px;"><i class="fa fa-flash"></i></th>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach ($query as $k => $row)
            {
            ?>
            <tr>
                <td class="text-center"><?php echo $k + 1; ?></td>
                <td><?php echo $row->booking_date; ?></td>
                <td>
                    <a href="#modal-fade" style="text-decoration: none;" title="view_freight"
                       class="view-modal" data-toggle="modal"
                       data-booking-date="<?php echo $row->booking_date; ?>"
                       data-customer-name="<?php echo $row->customer_name; ?>"
                       data-carriage="<?php echo $row->carriage; ?>"
                       data-container-package="<?php echo $row->container_package; ?>"
                       data-container-number="<?php echo $row->container_number; ?>"
                       data-mawb="<?php echo $row->mawb; ?>"
                       data-booking-ref="<?php echo $row->booking_reference; ?>"
                       data-carrier-booking-ref="<?php echo $row->carrier_booking_ref; ?>"
                       data-shipper="<?php echo $row->shipper; ?>"
                       data-consignee="<?php echo $row->consignee; ?>"
                       data-origin="<?php echo $row->origin; ?>"
                       data-destination="<?php echo $row->destination; ?>"
                       data-bol="<?php echo $row->bol; ?>"
                       data-vessel="<?php echo $row->vessel; ?>"
                       data-eta="<?php echo $row->eta; ?>"
                       data-comments="<?php echo $row->comments; ?>"
                       data-id="<?php echo $row->id; ?>">
                    <?php echo $row->customer_name; ?></td>
                <td><?php if ($row->carriage == "N") {
                        echo 'N/A';
                    } elseif ($row->carriage == "I") {
                        echo 'Import';
                    } else {
                        echo 'Export';
                    } ?>
                </td>
                <td><?php echo $row->container_package; ?></td>
                <td><?php echo $row->container_number; ?></td>
                <td><?php echo $row->mawb; ?></td>
                <td><?php echo $row->booking_reference; ?></td>
                <td><?php echo $row->carrier_booking_ref; ?></td>
                <td><?php echo $row->shipper; ?></td>
                <td><?php echo $row->consignee; ?></td>
                <td><?php echo $row->origin; ?></td>
                <td><?php echo $row->destination; ?></td>
                <td><?php echo $row->bol; ?></td>
                <td><?php echo $row->vessel; ?></td>
                <td><?php echo $row->eta; ?></td>
                <td><?php echo $row->comments; ?></td>

                <td class="text-center">
                    <a href="#modal-fade" title="view"
                       class="btn btn-effect-ripple btn-xs btn-info view-modal" data-toggle="modal"
                       data-booking-date="<?php echo $row->booking_date; ?>"
                       data-customer-name="<?php echo $row->customer_name; ?>"
                       data-carriage="<?php echo $row->carriage; ?>"
                       data-container-package="<?php echo $row->container_package; ?>"
                       data-container-number="<?php echo $row->container_number; ?>"
                       data-mawb="<?php echo $row->mawb; ?>"
                       data-booking-ref="<?php echo $row->booking_reference; ?>"
                       data-carrier-booking-ref="<?php echo $row->carrier_booking_ref; ?>"
                       data-shipper="<?php echo $row->shipper; ?>"
                       data-consignee="<?php echo $row->consignee; ?>"
                       data-origin="<?php echo $row->origin; ?>"
                       data-destination="<?php echo $row->destination; ?>"
                       data-bol="<?php echo $row->bol; ?>"
                       data-vessel="<?php echo $row->vessel; ?>"
                       data-eta="<?php echo $row->eta; ?>"
                       data-comments="<?php echo $row->comments; ?>"
                       data-id="<?php echo $row->id; ?>">
                        <i class="fa fa-eye"></i>
                    </a>
<!--                    <a href="--><?php //echo base_url('freight/edit/' . $row->id); ?><!--" data-toggle="tooltip"-->
<!--                       title="edit_freight" class="btn btn-effect-ripple btn-xs btn-success"><i-->
<!--                            class="fa fa-pencil"></i></a>-->

                    <a href="#" data-id="<?php echo $row->id ;?>" data-href="<?php //echo base_url('users/edit/' . $row->id); ?>"
                       data-toggle="modal" title="edit"
                       data-target="#modal-large" class="btn btn-effect-ripple btn-xs btn-success edit_freight"><i
                            class="fa fa-pencil"></i></a>

                    <a href="#" data-href="<?php echo base_url('freight/delete/' . $row->id); ?>"
                       data-toggle="modal" data-name="<?php echo $row->customer_name;
                    ?>" title="delete" data-target="#confirm-delete"
                       class="btn btn-effect-ripple btn-xs btn-danger del-row"><i class="fa fa-times"></i></a>
                </td>
            </tr>
    </div>
<?php } ?>
    </tbody>
    </table>
</div>

<form action="" method="post" class="form-horizontal form-bordered">
<div id="modal-large" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content col-md-12">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

            <?php $this->load->view('cms/freight/edit/edit');?>

            <div class="modal-footer">
                <button type="submit" class="btn btn-effect-ripple btn-primary"><i class="fa fa-check"></i>Submit</button>
            </div>
        </div>
    </div>
</div>
</form>

<div id="modal-fade" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title"><strong>Freight Details</strong></h3>
            </div>
            <div class="modal-body">
                <div class="box span3">
                    <div class="box-content">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td>ID</td>
                                <td id="freight_id"></td>
                            </tr>
                            <tr>
                                <td>Booking Date</td>
                                <td id="booking_date"></td>
                            </tr>
                            <tr>
                                <td>Customer Name</td>
                                <td id="customer_name"></td>
                            </tr>
                            <tr>
                                <td>Carriage</td>
                                <td id="carriage"></td>
                            </tr>
                            <tr>
                                <td>Container/Package</td>
                                <td id="container_package"></td>
                            </tr>
                            <tr>
                                <td>Container Number</td>
                                <td id="container_number"></td>
                            </tr>
                            <tr>
                                <td>MAWB</td>
                                <td id="mawb"></td>
                            </tr>
                            <tr>
                                <td>Booking Reference</td>
                                <td id="booking_ref"></td>
                            </tr>
                            <tr>
                                <td>Carrier Booking Reference</td>
                                <td id="carrier_booking_ref"></td>
                            </tr>
                            <tr>
                                <td>Shipper</td>
                                <td id="shipper"></td>
                            </tr>
                            <tr>
                                <td>Consignee</td>
                                <td id="consignee"></td>
                            </tr>
                            <tr>
                                <td>Origin</td>
                                <td id="origin"></td>
                            </tr>
                            <tr>
                                <td>Destination</td>
                                <td id="destination"></td>
                            </tr>
                            <tr>
                                <td>Bill of Lading</td>
                                <td id="bol"></td>
                            </tr>
                            <tr>
                                <td>Vessel</td>
                                <td id="vessel"></td>
                            </tr>
                            <tr>
                                <td>ETA</td>
                                <td id="eta"></td>
                            </tr>
                            <tr>
                                <td>Comments</td>
                                <td id="comments"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="confirm-delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><span id="del_name"></span></h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this record?
            </div>
            <div class="modal-footer">

                <a class="btn btn-effect-ripple btn-danger">Delete</a>
                <button type="button" data-dismiss="modal" class="btn btn-effect-ripple btn-default"
                        data-dismiss="modal">Cancel
                </button>

            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/js/pages/uiTables.js');?>"></script>
<script>$(function(){ UiTables.init(); });</script>


<script type="text/javascript">

    $(function () {
        $('a.view-modal').click(function (e) {
            var id = $(this).attr('data-id');
            var booking_date = $(this).attr('data-booking-date');
            var customer_name = $(this).attr('data-customer-name');
            var carriage = $(this).attr('data-carriage');
            var container_package = $(this).attr('data-container-package');
            var container_number = $(this).attr('data-container-number');
            var mawb = $(this).attr('data-mawb');
            var booking_ref = $(this).attr('data-booking-ref');
            var carrier_booking_ref = $(this).attr('data-carrier-booking-ref');
            var shipper = $(this).attr('data-shipper');
            var consignee = $(this).attr('data-consignee');
            var origin = $(this).attr('data-origin');
            var destination = $(this).attr('data-destination');
            var bol = $(this).attr('data-bol');
            var eta = $(this).attr('data-eta');
            var vessel = $(this).attr('data-vessel');
            var comments = $(this).attr('data-comments');


            $('td#freight_id').html(id);
            $('td#booking_date').html(booking_date);
            $('td#customer_name').html(customer_name);
            $('td#carriage').html(carriage);
            $('td#container_package').html(container_package);
            $('td#container_number').html(container_number);
            $('td#mawb').html(mawb);
            $('td#booking_ref').html(booking_ref);
            $('td#carrier_booking_ref').html(carrier_booking_ref);
            $('td#shipper').html(shipper);
            $('td#consignee').html(consignee);
            $('td#origin').html(origin);
            $('td#destination').html(destination);
            $('td#bol').html(bol);
            $('td#eta').html(eta);
            $('td#vessel').html(vessel);
            $('td#comments').html(comments);

        })
    });

    $(function () {
        $('a.del-row').click(function (e) {
            var name = $(this).attr('data-name');
            $('span#del_name').html(name);
        });
    });

    $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-danger').attr('href', $(e.relatedTarget).data('href'));
    });


    $(document).ready(function () {
        $('#freight_table').DataTable({
            "scrollX": true
        });

        $('a.edit_freight').click(function (e) {
            var id = $(this).attr('data-id');
            console.log(id);
            var data = new Array();
            $.ajax({
                url: '<?php echo base_url("freight/edit");?>',
                type: "post",
                data: {id: id},
                dataType: 'json',
                success: function (response) {
                    data = response;
                    $("#freight_id").val(data[0].id);
                    $("#booking_date").val(data[0].booking_date);
                    $("#customer_name").val(data[0].customer_name);
                    if(data[0].carriage == 'N'){
                        $("#carriage_n").attr("checked","checked");
                    }else if(data[0].carriage == 'I'){
                        $("#carriage_import").attr("checked","checked");
                    }else{
                        $("#carriage_export").attr("checked","checked");
                    }
                    $("#container_package").val(data[0].container_package);
                    $("#container_number").val(data[0].container_number);
                    $("#mawb").val(data[0].mawb);
                    $("#booking_reference").val(data[0].booking_reference);
                    $("#carrier_booking_ref").val(data[0].carrier_booking_ref);
                    $("#shipper").val(data[0].shipper);
                    $("#consignee").val(data[0].consignee);
                    $("#origin").val(data[0].origin);
                    $("#destination").val(data[0].destination);
                    $("#bol").val(data[0].bol);
                    $("#vessel").val(data[0].vessel);
                    $("#eta").val(data[0].eta);
                    $("#comments").val(data[0].comments);
                }
            });
        });
    });
</script>




