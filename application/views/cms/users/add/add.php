<div class="row">
    <div class="col-md-2"></div>
    <div class="row col-md-8">
        <div class="block" >
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="<?php echo base_url('users/add/'); ?>">Add User</a></li>
                    <li><a href="<?php echo base_url('users/view/');?>">Users
                            List</a></li>
                </ul>
            </div>

            <?php if(isset($message)){ ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
                <?php
            }?>

            <?php if(validation_errors()){?>
                <div class="alert alert-danger alert-dismissable">
                    <?php
                    echo validation_errors();
                    ?>
                </div>
            <?php }?>

            <form action="" method="post" class="form-horizontal form-bordered">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="name">User Name</label>
                    <div class="col-md-9">
                        <input type="text" id="name" name="name" class="form-control" placeholder="Enter user name">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="email">Email</label>
                    <div class="col-md-9">
                        <input type="email" id="email" name="email" class="form-control" placeholder="Enter user email">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="password">Password</label>
                    <div class="col-md-9">
                        <input type="password" id="password" name="password" class="form-control" placeholder="Enter user password">

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="confirm_password">Confirm Password</label>
                    <div class="col-md-9">
                        <input type="password" id="confirm_password" name="confirm_password" class="form-control" placeholder="Retype password">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Add Permission</label>
                    <div class="col-md-9">
                        <label for="add" class="radio-inline">
                            <input type="radio" value="1" name="add" id="access_add_yes" checked> Yes
                        </label>
                        <label for="add" class="radio-inline">
                            <input type="radio" value="0" name="add" id="access_add_no"> No
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Edit Permission</label>
                    <div class="col-md-9">
                        <label for="edit" class="radio-inline">
                            <input type="radio" value="1" name="edit" id="access_edit_yes" checked> Yes
                        </label>
                        <label for="edit" class="radio-inline">
                            <input type="radio" value="0" name="edit" id="access_edit_no"> No
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Delete Permission</label>
                    <div class="col-md-9">
                        <label for="delete" class="radio-inline">
                            <input type="radio" value="1" name="delete" id="access_delete_yes" checked> Yes
                        </label>
                        <label for="delete" class="radio-inline">
                            <input type="radio" value="0" name="delete" id="access_delete_no"> No
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Search Permission</label>
                    <div class="col-md-9">
                        <label for="search" class="radio-inline">
                            <input type="radio" value="1" name="search" id="access_search_yes" checked> Yes
                        </label>
                        <label for="search" class="radio-inline">
                            <input type="radio" value="0" name="search" id="access_search_no"> No
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="status">Status</label>
                    <div class="col-md-9">
                        <select id="status" name="status" class="form-control" size="1">
                            <option value="A">Activate</option>
                            <option value="D">Deactivate</option>
                        </select>
                    </div>
                </div>

                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i class="fa fa-check"></i> Submit</button>
                        <button type="reset" href="<?php echo base_url('users/add');?>" class="btn btn-effect-ripple btn-danger"><i class="fa fa-times"></i> Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
