
<div class="col-md-3"></div>
<div class="row col-md-12">
           <div class="block" >

            <?php if(isset($message)){ ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
                <?php
            }?>

            <?php if(validation_errors()){?>
                <div class="alert alert-danger alert-dismissable">
                    <?php
                    echo validation_errors();
                    ?>
                </div>
            <?php }?>


<!--            <form action="" method="post" class="form-horizontal form-bordered">-->
                <input type="hidden" name="user_id" id="user_id" value=""/>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="name">Name</label>
                    <div class="col-md-9">
                        <input type="text" id="name" name="name" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="email">Email</label>
                    <div class="col-md-9">
                        <input type="email" id="email" name="email" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="password">Password</label>
                    <div class="col-md-9">
                        <input type="password" id="password" name="password" class="form-control"  placeholder="Enter password to change" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="confirm_password">Confirm Password</label>
                    <div class="col-md-9">
                        <input type="password" id="confirm_password" name="confirm_password" class="form-control" placeholder="Retype password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Add Permission</label>
                    <div class="col-md-9">
                        <label for="add" class="radio-inline">
                            <input type="radio" value="1" name="add" id="access_add_yes"  /> Yes
                        </label>
                        <label for="add" class="radio-inline">
                            <input type="radio" value="0" name="add" id="access_add_no"  /> No
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Edit Permission</label>
                    <div class="col-md-9">
                        <label for="edit" class="radio-inline">
                            <input type="radio" value="1" name="edit" id="access_edit_yes" /> Yes
                        </label>
                        <label for="edit" class="radio-inline">
                            <input type="radio" value="0" name="edit" id="access_edit_no"  /> No
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Delete Permission</label>
                    <div class="col-md-9">
                        <label for="delete" class="radio-inline">
                            <input type="radio" value="1" name="delete" id="access_delete_yes"  /> Yes
                        </label>
                        <label for="delete" class="radio-inline">
                            <input type="radio" value="0" name="delete" id="access_delete_no"  /> No
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Search Permission</label>
                    <div class="col-md-9">
                        <label for="search" class="radio-inline">
                            <input type="radio" value="1" name="search" id="access_search_yes" /> Yes
                        </label>
                        <label for="search" class="radio-inline">
                            <input type="radio" value="0" name="search" id="access_search_no"  /> No
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label" for="status">Status</label>
                    <div class="col-md-9">
                        <select id="status" name="status" class="form-control" size="1">
                            <?php
                            if($query[0]->status == 'A')
                            {?>
                                <option value="A" selected="selected">Activate</option>
                                <option value="D">Deactivate</option>
                            <?php }
                            else
                            {?>
                                <option value="A">Activate</option>
                                <option value="D" selected="selected">Deactivate</option>

                            <?php } ?>
                        </select>
                    </div>
                </div>

<!--                <div class="form-group form-actions">-->
<!--                    <div class="col-md-9 col-md-offset-3">-->
<!--                        <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i class="fa fa-check"></i> Submit</button>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </form>-->
           </div>
    </div>
