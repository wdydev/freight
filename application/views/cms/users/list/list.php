<style type="text/css">
    .click{
        cursor: pointer;
    }
</style>
<div class="block full">
    <div class="block-title">
        <ul class="nav nav-tabs">
            <li><a href="<?php echo base_url('users/add/'); ?>">Add User</a></li>
            <li class="active"><a href="<?php echo base_url('users/view/');?>">Users
                    List</a></li>
        </ul>
    </div>

    <?php if (isset($message)) { ?>
        <div class="alert alert-success alert-dismissable">
            <?php
            echo $message;
            ?>
        </div>
        <?php
    } ?>

    <div class="table-responsive">
        <table id="example-datatable" class="table table-bordered table-vcenter">
            <thead>
            <tr>
                <th class="text-center" width="5%" rowspan="2">S.N.</th>
                <th class="text-center" width="23%" rowspan="2">User Name</th>
                <th class="text-center" width="23%" rowspan="2">Email</th>
                <th class="text-center" colspan="4">Permission</th>
                <th class="text-center" width="10%" rowspan="2">Status</th>
                <th class="text-center" width="20%" rowspan="2"><i class="fa fa-flash"></i></th>
            </tr>
            <tr>
                <th>Add</th>
                <th>Edit</th>
                <th>Delete</th>
                <th>Search</th>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach ($query as $k => $row) {
                ?>
                <div class="sortable">
                    <tr>
                        <td class="text-center"><?php echo $k + 1; ?></td>
                        <td><strong><?php echo $row->name; ?></strong></td>
                        <td><?php echo $row->email; ?></td>
                        <td class="text-center">
                            <?php if ($row->add == "1") { ?>
                                <img src="<?php echo base_url('assets/img/green_button.png'); ?>" title="Deny Add"
                                     onclick="return setAction('add','<?php echo $row->add ?>','<?php echo $row->id ?>');"
                                     class="click">
                            <?php } else { ?>
                                <img src="<?php echo base_url('assets/img/red_button.png'); ?>" title="Grant Add"
                                     onclick="return setAction('add','<?php echo $row->add ?>','<?php echo $row->id ?>');"
                                     class="click">
                            <?php } ?>
                        </td>
                        <td class="text-center">
                            <?php if ($row->edit == "1") { ?>
                                <img src="<?php echo base_url('assets/img/green_button.png'); ?>" title="Deny Edit"
                                     onclick="return setAction('edit','<?php echo $row->edit ?>','<?php echo $row->id ?>');"
                                     class="click">
                            <?php } else { ?>
                                <img src="<?php echo base_url('assets/img/red_button.png'); ?>" title="Grant Edit"
                                     onclick="return setAction('edit','<?php echo $row->edit ?>','<?php echo $row->id ?>');"
                                     class="click">
                            <?php } ?>
                        </td>
                        <td class="text-center">
                            <?php if ($row->delete == "1") { ?>
                                <img src="<?php echo base_url('assets/img/green_button.png'); ?>" title="Deny Delete"
                                     onclick="return setAction('delete','<?php echo $row->delete ?>','<?php echo $row->id ?>');"
                                     class="click">
                            <?php } else { ?>
                                <img src="<?php echo base_url('assets/img/red_button.png'); ?>" title="Grant Delete"
                                     onclick="return setAction('delete','<?php echo $row->delete ?>','<?php echo $row->id ?>');"
                                     class="click">
                            <?php } ?>
                        </td>
                        <td class="text-center">
                            <?php if ($row->search == "1") { ?>
                                <img src="<?php echo base_url('assets/img/green_button.png'); ?>" title="Deny Search"
                                     onclick="return setAction('search','<?php echo $row->search ?>','<?php echo $row->id ?>');"
                                     class="click">
                            <?php } else { ?>
                                <img src="<?php echo base_url('assets/img/red_button.png'); ?>" title="Grant Search"
                                     onclick="return setAction('search','<?php echo $row->search ?>','<?php echo $row->id ?>');"
                                     class="click">
                            <?php } ?>
                        </td>
                        <?php if ($row->status == 'A') { ?>
                            <td class="hidden-sm hidden-xs text-center"><a href="javascript:void(0)" title="Deactivate" onclick="return changeStatus('<?php echo $row->status ?>','<?php echo $row->id ?>');"
                                                                                                                                                      class="label label-success">Activate</a>
                            </td><?php } else { ?>
                            <td class="hidden-sm hidden-xs text-center"><a href="javascript:void(0)" title="Activate" onclick="return changeStatus('<?php echo $row->status ?>','<?php echo $row->id ?>');"
                                                                           class="label label-danger">Deactivate</a>
                            </td><?php } ?>

                        <td class="text-center">
                            <a href="#modal-fade" title="view"
                               class="btn btn-effect-ripple btn-xs btn-info view-modal" data-toggle="modal"
                               data-name="<?php echo $row->name; ?>" data-status="<?php echo $row->status; ?>"
                               data-email="<?php echo $row->email; ?>" data-add-access="<?php echo $row->add; ?>"
                               data-edit-access="<?php echo $row->edit; ?>" data-delete-access="<?php echo $row->delete; ?>"
                               data-search-access="<?php echo $row->search; ?>" data-id="<?php echo $row->id; ?>">
                                <i class="fa fa-eye"></i>
                            </a>
<!--                            <a href="--><?php //echo base_url('users/edit/' . $row->id); ?><!--" data-toggle="tooltip"-->
<!--                               title="edit_user" class="btn btn-effect-ripple btn-xs btn-success"><i-->
<!--                                    class="fa fa-pencil"></i></a>-->

                            <a href="#" data-id="<?php echo $row->id ;?>" data-href="<?php //echo base_url('users/edit/' . $row->id); ?>"
                               data-toggle="modal" title="edit"
                               data-target="#modal-large" class="btn btn-effect-ripple btn-xs btn-success edit_user"><i
                                    class="fa fa-pencil"></i></a>

                        </td>
                    </tr>
                </div>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<form action="" method="post" class="form-horizontal form-bordered">
<div id="modal-large" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content col-md-8">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                    <?php $this->load->view('cms/users/edit/edit');?>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-effect-ripple btn-primary"><i class="fa fa-check"></i>Submit</button>
                </div>
            </div>
        </div>
</div>
</form>

<div id="modal-fade" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title"><strong>Admin Details</strong></h3>
            </div>
            <div class="modal-body">
                <div class="box span3">
                    <div class="box-content">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td>ID</td>
                                <td id="user_id"></td>
                            </tr>
                            <tr>
                                <td>User Name</td>
                                <td id="user_name"></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td id="user_email"></td>
                            </tr>
                            <tr>
                                <td>Add Permission</td>
                                <td id="add_access"></td>
                            </tr>
                            <tr>
                                <td>Edit Permission</td>
                                <td id="edit_access"></td>
                            </tr>
                            <tr>
                                <td>Delete Permission</td>
                                <td id="delete_access"></td>
                            </tr>
                            <tr>
                                <td>Search Permission</td>
                                <td id="search_access"></td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td id="user_status"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-effect-ripple btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/js/pages/uiTables.js');?>"></script>
<script>$(function(){ UiTables.init(); });</script>

<script type="text/javascript">
    $(function () {
        $('a.view-modal').click(function (e) {
            var id = $(this).attr('data-id');
            var name = $(this).attr('data-name');
            var email = $(this).attr('data-email');
            var status = $(this).attr('data-status');
            var add_access = $(this).attr('data-add-access');
            var edit_access = $(this).attr('data-edit-access');
            var delete_access = $(this).attr('data-delete-access');
            var search_access = $(this).attr('data-search-access');
            $('td#user_id').html(id);
            $('td#user_name').html(name);
            $('td#user_email').html(email);
            if (add_access == 1) {
                $('td#add_access').text("Granted");
            } else {
                $('td#add_access').text("Denied");
            }
            if (edit_access == 1) {
                $('td#edit_access').text("Granted");
            } else {
                $('td#edit_access').text("Denied");
            }
            if (delete_access == 1) {
                $('td#delete_access').text("Granted");
            } else {
                $('td#delete_access').text("Denied");
            }
            if (search_access == 1) {
                $('td#search_access').text("Granted");
            } else {
                $('td#search_access').text("Denied");
            }

            if (status == 'A') {
                $('td#user_status').text("Activated");
            } else {
                $('td#user_status').text("Deactivated");
            }



        })
    });


    function setAction(type, value, id) {
        if (confirm('Are you sure you want to change the access?')) {
            $.ajax({
                url: '<?php echo base_url("users/change_access");?>',
                type: "post",
                data: {type: type, value: value, id: id},
                success: function (response) {
                    location.reload(true);
                }
            });
        }
    }

    function changeStatus(value, id) {
        if (confirm('Are you sure you want to change the status?')) {
            $.ajax({
                url: '<?php echo base_url("users/change_status");?>',
                type: "post",
                data: {value: value, id: id},
                success: function (response) {
                    location.reload(true);
                }
            });
        }
    }

    $(document).ready(function () {
        $('a.edit_user').click(function (e) {
                var id = $(this).attr('data-id');
                console.log(id);
                var data = new Array();
                $.ajax({
                    url: '<?php echo base_url("users/edit");?>',
                    type: "post",
                    data: {id: id},
                    dataType: 'json',
                    success: function (response) {
                        data = response;
                        $("#user_id").val(data[0].id);
                        $("#name").val(data[0].name);
                        $("#email").val(data[0].email);
                        if(data[0].add == '1'){
                            $("#access_add_yes").attr("checked","checked");
                        }else{
                            $("#access_add_no").attr("checked","checked");
                        }
                        if(data[0].edit == '1'){
                            $("#access_edit_yes").attr("checked","checked");
                        }else{
                            $("#access_edit_no").attr("checked","checked");
                        }
                        if(data[0].delete == '1'){
                            $("#access_delete_yes").attr("checked","checked");
                        }else{
                            $("#access_delete_no").attr("checked","checked");
                        }
                        if(data[0].search == '1'){
                            $("#access_search_yes").attr("checked","checked");
                        }else{
                            $("#access_search_no").attr("checked","checked");
                        }
                        $("#status").val(data[0].status);
                    }
                });
            });
        });


</script>


