<div class="row">
    <div class="row col-md-12">
        <div class="block">
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li><a href="<?php echo base_url('purchase/add/'); ?>">Add Purchase</a></li>
                    <li class="active"><a href="<?php echo base_url('purchase/view/');?>">Purchase
                            List</a></li>
                    <li><a href="<?php echo base_url('purchase/purchaseshead_view/');?>">PurchaseHead
                            List</a></li>
                </ul>
            </div>

            <?php if (isset($message)) { ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
                <?php
            } ?>

            <?php if (validation_errors()) { ?>
                <div class="alert alert-danger alert-dismissable">
                    <?php
                    echo validation_errors();
                    ?>
                </div>
            <?php } ?>

            <form action="" method="post" class="form-horizontal form-bordered">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="purchasesHeadId">Purchases Head</label>

                    <div class="col-md-6">
                        <select id="purchasesHeadId" name="purchasesHeadId" class="form-control" size="1">
                            <option selected disabled>Please Select</option>
                            <?php $clients = $this->admin_model->table_fetch_rows('purchaseshead', array(), array('name' => 'asc'));
                            foreach ($clients as $c) {
                                if($query[1] == ($c->id)){?>
                                    <option value="<?php echo $c->id;?>" selected="selected"><?php echo $c->name;?></option>
                                <?php  }else {?>
                                    <option value="<?php echo $c->id;?>"><?php echo $c->name;?></option>
                                <?php } }?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="invoice_no">Invoice No.</label>

                    <div class="col-md-6">
                        <input type="text" id="invoice_no" name="invoice_no" class="form-control"
                               placeholder="Enter Invoice number" value="<?php echo ($query[2]!=NULL)?$query[2]:'';?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="startDate">Start Date</label>

                    <div class="col-md-6">
                        <input type="text" id="startDate" name="startDate" class="form-control input-datepicker"
                               data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo ($query[3]!=NULL)?$query[3]:'';?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="endDate">End Date</label>

                    <div class="col-md-6">
                        <input type="text" id="endDate" name="endDate" class="form-control input-datepicker"
                               data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo ($query[4]!=NULL)?$query[4]:date('Y-m-d');?>">
                    </div>
                </div>

                <div class="form-group form-actions">
                    <div class="col-md-9 col-md-offset-3">
                        <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i
                                class="fa fa-search"></i> Search
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="block full">

        <div class="table-responsive">
            <table class="table table-bordered table-vcenter">
                <thead>
                <tr>
                    <th class="text-center" style="width: 100px;">Serial No</th>
                    <th>Issue Date</th>
                    <th>Invoice No.</th>
                    <th>CR No.</th>
                    <th>Name</th>
                    <th>Amount</th>
                    <th>GST</th>
                    <th>Paid</th>
                    <th>Paid Date</th>
                    <th class="text-center" style="width: 125px;"><i class="fa fa-flash"></i></th>
                </tr>
                </thead>
                <tbody>
                <?php $amtTotal = 0;
                $gstTotal = 0;
                $paidTotal = 0;
                if (is_array($query[0]) && !empty($query[0])) {
                    foreach ($query[0] as $k => $row) {
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $k + 1; ?></td>
                            <td><?php echo $row['issueDate']; ?></td>
                            <td><?php echo $row['invoiceNo']; ?></td>
                            <td><?php echo $row['crNo']; ?></td>
                            <td><?php echo $row['purchasesHeadName']; ?></td>
                            <td><?php echo $row['amount']; ?></td>
                            <td><?php echo $row['gst']; ?></td>
                            <td><?php echo $row['paid']; ?></td>
                            <td><?php echo $row['paidDate']; ?></td>

                            <td class="text-center">
<!--                                <a href="--><?php //echo base_url('purchase/edit/' . $row['id']); ?><!--" data-toggle="tooltip"-->
<!--                                   title="edit" class="btn btn-effect-ripple btn-xs btn-success"><i-->
<!--                                        class="fa fa-pencil"></i></a>-->

                                <a href="#" data-id="<?php echo $row['id'] ;?>" data-href="<?php //echo base_url('users/edit/' . $row->id); ?>"
                                   data-toggle="modal" title="edit"
                                   data-target="#modal-large" class="btn btn-effect-ripple btn-xs btn-success edit_purchase"><i
                                        class="fa fa-pencil"></i></a>


                                <a href="#" data-href="<?php echo base_url('purchase/delete/' . $row['id']); ?>"
                                   data-toggle="modal" data-name="<?php //echo $row->name;
                                ?>" title="delete" data-target="#confirm-delete"
                                   class="btn btn-effect-ripple btn-xs btn-danger del-row"><i
                                        class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <?php $amtTotal = $amtTotal + $row['amount'];
                        $gstTotal = $gstTotal + $row['gst'];
                        $paidTotal = $paidTotal + $row['paid'];
                    }
                } ?>
                <tr>
                    <td></td>
                    <td colspan="4" class="text-center"><b>Total : </b></td>
                    <td><b><?php echo $amtTotal; ?> </b></td>
                    <td><b><?php echo $gstTotal; ?> </b></td>
                    <td><b><?php echo $paidTotal; ?> </b></td>
                </tr>
                </tbody>
            </table>
        </div>
        <a target="_blank"
           href="<?php echo base_url('purchase/pdf_purchases?clientId=' . $query[1] . '&invoiceNo=' . $query[2] .'&startDate=' . $query[3] . '&endDate=' . $query[4]); ?>">View
            PDF</a>

    </div>

    <form action="" method="post" class="form-horizontal form-bordered">
    <div id="modal-large" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content col-md-12">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <?php $this->load->view('cms/purchase/edit/edit');?>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-effect-ripple btn-primary"><i class="fa fa-check"></i>Submit</button>
                </div>
            </div>
        </div>
    </div>
    </form>

    <div id="confirm-delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><span id="del_name"></span></h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this record?
                </div>
                <div class="modal-footer">

                    <a class="btn btn-effect-ripple btn-danger">Delete</a>
                    <button type="button" data-dismiss="modal" class="btn btn-effect-ripple btn-default"
                            data-dismiss="modal">Cancel
                    </button>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(function () {
            $('a.del-row').click(function (e) {
                var name = $(this).attr('data-name');
                $('span#del_name').html(name);
            });
        });

        $('#confirm-delete').on('show.bs.modal', function (e) {
            $(this).find('.btn-danger').attr('href', $(e.relatedTarget).data('href'));
        });

        $(document).ready(function () {
            $('a.edit_purchase').click(function (e) {
                var id = $(this).attr('data-id');
                console.log(id);
                var data = new Array();
                $.ajax({
                    url: '<?php echo base_url("purchase/edit");?>',
                    type: "post",
                    data: {id: id},
                    dataType: 'json',
                    success: function (response) {
                        data = response;
                        console.log(data);
                        $("#purchase_id").val(data[0].id);
                        $("#purchasesHeadId").val(data[0].purchasesHeadId);
                        $("#issueDate").val(data[0].issueDate);
                        $("#invoiceNo").val(data[0].invoiceNo);
                        $("#crNo").val(data[0].crNo);
                        $("#amount").val(data[0].amount);
                        $("#gst").val(data[0].gst);
                        $("#paid").val(data[0].paid);
                        $("#paidDate").val(data[0].paidDate);
                    }
                });
            });
        });
    </script>




