<div class="block full">
    <div class="block-title">
        <ul class="nav nav-tabs">
            <li><a href="<?php echo base_url('purchase/add/'); ?>">Add Purchase</a></li>
            <li><a href="<?php echo base_url('purchase/view/');?>">Purchase
                    List</a></li>
            <li class="active"><a href="<?php echo base_url('purchase/purchaseshead_view/');?>">PurchaseHead
                    List</a></li>
        </ul>
    </div>

    <?php if (isset($message)) { ?>
        <div class="alert alert-success alert-dismissable">
            <?php
            echo $message;
            ?>
        </div>
        <?php
    } ?>
    <div>
        <a href="#modal-tabs" style="text-decoration:none;" title="add_purchase_head" class="add-purchaseshead"
           data-toggle="modal">
            <input type="button" class="btn btn-info" value="Add PurchaseHead">
        </a></div>
    <br/>

    <div class="table-responsive">
        <table id="example-datatable" class="table table-bordered table-vcenter">
            <thead>
            <tr>
                <th class="text-center" style="width: 100px;">Serial No</th>
                <th>Purchases Head</th>
                <th class="text-center" style="width: 125px;"><i class="fa fa-flash"></i></th>
            </tr>
            </thead>
            <tbody>

            <?php
            foreach ($query as $k => $row)
            {
            ?>
            <tr>
                <td class="text-center"><?php echo $k + 1; ?></td>
                <td><strong><?php echo $row->name; ?></strong></td>

                <td class="text-center">
<!--                    <a href="--><?php //echo base_url('purchase/purchaseshead_edit/' . $row->id); ?><!--" data-toggle="tooltip"-->
<!--                       title="edit" class="btn btn-effect-ripple btn-xs btn-success"><i-->
<!--                            class="fa fa-pencil"></i></a>-->

                    <a href="#" data-id="<?php echo $row->id;?>" data-href="<?php //echo base_url('users/edit/' . $row->id); ?>"
                       data-toggle="modal" title="edit"
                       data-target="#modal-large" class="btn btn-effect-ripple btn-xs btn-success edit_purchasehead"><i
                            class="fa fa-pencil"></i></a>

                    <a href="#" data-href="<?php echo base_url('purchase/purchaseshead_delete/' . $row->id); ?>"
                       data-toggle="modal" data-name="<?php echo $row->name; ?>" title="delete_purchaseshead"
                       data-target="#confirm-delete" class="btn btn-effect-ripple btn-xs btn-danger del-row"><i
                            class="fa fa-times"></i></a>
                </td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>


</div>

<div id="confirm-delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><span id="del_name"></span></h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this record?
            </div>
            <div class="modal-footer">

                <a class="btn btn-effect-ripple btn-danger">Delete</a>
                <button type="button" data-dismiss="modal" class="btn btn-effect-ripple btn-default"
                        data-dismiss="modal">Cancel
                </button>

            </div>
        </div>
    </div>
</div>

<form action="" method="post" class="form-horizontal form-bordered">
<div id="modal-tabs" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <?php $this->load->view('cms/purchase/purchaseshead/add'); ?>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-effect-ripple btn-primary">Submit</button>
            </div>
        </div>
    </div>
</div>
</form>

<form action="" method="post" class="form-horizontal form-bordered">
    <div id="modal-large" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <?php $this->load->view('cms/purchase/purchaseshead/edit'); ?>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-effect-ripple btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>
</form>

<!--<script src="--><?php //echo base_url('assets/js/pages/uiTables.js');?><!--"></script>-->
<!--<script>$(function(){ UiTables.init(); });</script>-->

<script type="text/javascript">

    $(function () {
        $('a.del-row').click(function (e) {
            var name = $(this).attr('data-name');
            $('span#del_name').html(name);
        });
    });

    $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-danger').attr('href', $(e.relatedTarget).data('href'));
    });

    $(document).ready(function () {
        $('a.edit_purchasehead').click(function (e) {
            var id = $(this).attr('data-id');
            var data = new Array();
            $.ajax({
                url: '<?php echo base_url("purchase/purchaseshead_edit");?>',
                type: "post",
                data: {id: id},
                dataType: 'json',
                success: function (response) {
                    data = response;
                    console.log(data);
                    $("#purchasehead_id").val(data[0].id);
                    $("#purchasehead_name").val(data[0].name);
                }
            });
        });
    });
</script>
</script>




