<div class="row col-md-12">

    <div class="block" >
        <!-- Horizontal Form Title -->
        <div class="block-title">
            <h2>Add PurchasesHead</h2>
        </div>

        <?php if(isset($message)){ ?>
            <div class="alert alert-success alert-dismissable">
                <?php
                echo $message;
                ?>
            </div>
            <?php
        }?>

            <div class="form-group">
                <label class="col-md-3 control-label" for="purchaseshead">Purchases Head</label>
                <div class="col-md-9">
                    <input type="text" id="name" name="name" class="form-control" placeholder="Enter purchaseshead" required>
                </div>
            </div>

<!--            <div class="form-group form-actions">-->
<!--                <div class="col-md-9 col-md-offset-3">-->
<!--                    <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit">Submit</button>-->
<!--                </div>-->
<!--            </div>-->

    </div>
</div>
