<div class="row">
    <div class="col-md-2"></div>
    <div class="row col-md-8">
        <div class="block" >
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="<?php echo base_url('clients/add/'); ?>">Add Clients</a></li>
                    <li><a href="<?php echo base_url('clients/view/');?>">Clients
                            List</a></li>
                </ul>
            </div>
                       <?php if(isset($message)){ ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
                <?php
            }?>

            <?php if(validation_errors()){?>
                <div class="alert alert-danger alert-dismissable">
                    <?php
                    echo validation_errors();
                    ?>
                </div>
            <?php }?>

            <form action="" method="post" class="form-horizontal form-bordered">

                <div class="form-group">
                    <label class="col-md-3 control-label" for="name">Name</label>
                    <div class="col-md-6">
                        <input type="text" id="name" name="name" class="form-control" placeholder="Enter Client Name">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="address">Address</label>
                    <div class="col-md-9">
                    <textarea id="address" rows="3" cols="10" name="address" class="form-control"
                              style="width: 322px; height: 99px;" placeholder="Enter address"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="phone">Phone</label>
                    <div class="col-md-6">
                        <input type="text" id="phone" name="phone" class="form-control" placeholder="Enter phone number">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="email">Email</label>
                    <div class="col-md-6">
                        <input type="email" id="email" name="email" class="form-control" placeholder="Enter email">
                    </div>
                </div>

                <div class="form-group form-actions">
                    <div class="col-md-6 col-md-offset-3">
                        <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i class="fa fa-check"></i> Submit</button>
                        <button type="reset" href="<?php echo base_url('clients/add');?>" class="btn btn-effect-ripple btn-danger"><i class="fa fa-times"></i> Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
