
<div class="row">
    <div class="col-md-2"></div>
    <div class="row col-md-8">
        <div class="block" >
            <?php if(isset($message)){ ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
                <?php
            }?>

            <?php if(validation_errors()){?>
                <div class="alert alert-danger alert-dismissable">
                    <?php
                    echo validation_errors();
                    ?>
                </div>
            <?php }?>

<!--            <form action="" method="post" class="form-horizontal form-bordered">-->
            <input type="hidden" name="client_id" id="client_id" />
                <div class="form-group">
                    <label class="col-md-3 control-label" for="name">Name</label>
                    <div class="col-md-6">
                        <input type="text" id="name" name="name" class="form-control" value="">

                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="address">Address</label>
                    <div class="col-md-9">
                    <textarea id="address" rows="3" cols="10" name="address" class="form-control"
                              style="width: 322px; height: 99px;" placeholder="Enter address"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="phone">Phone</label>
                    <div class="col-md-6">
                        <input type="text" id="phone" name="phone" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="email">Email</label>
                    <div class="col-md-6">
                        <input type="email" id="email" name="email" class="form-control" value="">
                    </div>
                </div>

<!--                <div class="form-group form-actions">-->
<!--                    <div class="col-md-6 col-md-offset-3">-->
<!--                        <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i class="fa fa-check"></i> Submit</button>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </form>-->
        </div>
    </div>
</div>
