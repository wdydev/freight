<div class="row">
    <div class="row col-md-12">
        <div class="block" >
            <div class="block-title">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="<?php echo base_url('expenses/add/'); ?>">Add Expenses</a></li>
                    <li><a href="<?php echo base_url('expenses/view/');?>">Expenses
                            List</a></li>
                    <li><a href="<?php echo base_url('expenses/expenseshead_view/');?>">ExpensesHead
                            List</a></li>
                </ul>
            </div>
            <div class="col-md-12">
                    <div class="col-md-6"><img src="<?php echo base_url('assets/img/logo_03.png');?>"/> </div>
                    <div class="col-md-3">Divine Logistics Ltd<br/>Unit-3,12 Lambie Drive,<br/>Manukau Central, Auckland, NZ<br/>POBox: 76877<br/>Manukau City 2241, Auckland</div>
                    <div class="col-md-3">Office: 09 2637439, 09 2151601<br/>Mobile: 02102494202<br/>After Hours: 09-2673201<br/>Email :info@divinelogistics.co.nz<br/>Web :www.divinelogistics.co.nz</div>
               </div>
            <div class="clearfix"></div><br />

            <?php if(isset($message)){ ?>
                <div class="alert alert-success alert-dismissable">
                    <?php
                    echo $message;
                    ?>
                </div>
                <?php
            }?>

            <?php if(validation_errors()){?>
                <div class="alert alert-danger alert-dismissable">
                    <?php
                    echo validation_errors();
                    ?>
                </div>
            <?php }?>

            <form action="" method="post" class="form-horizontal form-bordered">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="name">Expenses</label>
                    <div class="col-md-6">
                        <select id="expensesHeadId" name="expensesHeadId" class="form-control" size="1">
                        <?php $clients = $this->admin_model->table_fetch_rows('expenseshead',array(),array('name'=>'asc'));
                        foreach($clients as $c){?>
                            <option value="<?php echo $c->id;?>"><?php echo $c->name;?></option>
                        <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="name">Name</label>
                    <div class="col-md-6">
                        <input type="text" id="name" name="name" class="form-control" placeholder="Enter name">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="amount">Amount</label>
                    <div class="col-md-6">
                        <input type="text" id="amount" name="amount" pattern="[0-9\.]+" title="Should be an integer" class="form-control" placeholder="Enter amount">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="paidDate">Date</label>
                    <div class="col-md-6">
                        <input type="text" id="paidDate" name="paidDate" class="form-control input-datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd">
                    </div>
                </div>

                <div class="form-group form-actions">
                    <div class="col-md-6 col-md-offset-3">
                        <button type="submit" class="btn btn-effect-ripple btn-primary" name="submit"><i class="fa fa-check"></i> Submit</button>
                        <button type="reset" href="<?php echo base_url('expenses/add');?>" class="btn btn-effect-ripple btn-danger"><i class="fa fa-times"></i> Reset</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
