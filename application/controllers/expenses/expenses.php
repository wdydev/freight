<?php

class Expenses extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
            $this->data['user'] = $this->session->userdata('logged_in');
        }
    }

    public function index()
    {

    }

    public function add()
    {
        if ($this->input->post(null, true) != false) {
            $add = $this->admin_model->table_insert('expenses',$_POST);
            if ($add!=false) {
                $this->data['message'] = 'Saved Successfully!!!';
            } else {
                $this->data['message'] = 'Sorry,The data could not be saved';
            }
        }
        $this->template = 'expenses/add/add';
        $this->load->view('themes/cms',$this->data);
    }

    public function edit()
    {
        //$id = $this->uri->segment(3);
        $id = $this->input->post('id');
        $this->data['query'] = $this->admin_model->table_fetch_row('expenses',array('id'=>$id));
        echo json_encode($this->data['query']);
    }

    public function view()
    {
        if (($this->input->post(null, true) != false)&& ($this->input->post('expense_id') != '')) {
            $update = $this->admin_model->table_update('expenses', $_POST, array('id' => $this->input->post('expense_id')));
            if ($update != false) {
                $this->session->set_flashdata('message','Updated successfully.');
                redirect('expenses/view');
            } else {
                $this->session->set_flashdata('message','Updated Unsuccessful.');
            }
        }else if(($this->input->post(null, true) != false) &&($this->input->post('expense_id') == '') && ($this->input->post('expensesHeadId') != '')) {
            $id = $this->input->post('expensesHeadId');
            $startDate = $this->input->post('startDate');
            $endDate = $this->input->post('endDate');
            $this->data['query'] = $this->admin_model->expenses_list($id,$startDate,$endDate);
        }else{
            $this->data['query'] = NULL;
        }

        $this->template = 'expenses/list/list';
        $this->load->view('themes/cms',$this->data);
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->admin_model->delete_table_row('expenses', array('id' => $id));
        redirect($_SERVER['HTTP_REFERER']);
        $this->load->view('themes/cms',$this->data);
    }

    public function expenseshead_view()
    {
        $this->data['query'] = $this->admin_model->table_fetch_rows('expenseshead');

        if (($this->input->post(null, true) != false) &&  ($this->input->post('expenseshead_id') != '')) {
            $data = array('name' => strtoupper($this->input->post('name')));
            $update = $this->admin_model->table_update('expenseshead', $data, array('id' => $this->input->post('expenseshead_id')));
            if ($update != false) {
                $this->session->set_flashdata('message', 'Updated successfully.');
                redirect('expenses/expenseshead_view');
            } else {
                $this->session->set_flashdata('message', 'Updated Unsuccessful.');
            }
        }

        else if (($this->input->post(null, true) != false)&&  ($this->input->post('expenseshead_id') == '')) {
            $data = array('name'=>strtoupper($this->input->post('name')));
            $add = $this->admin_model->table_insert('expenseshead',$data);
            if ($add!=false) {
                $this->data['message'] = 'Purchases Head Saved Successfully!!!';
            } else {
                $this->data['message'] = 'Sorry,The data could not be saved';
            }
        }
        $this->template = 'expenses/expenseshead/list';
        $this->load->view('themes/cms',$this->data);
    }

    public function expenseshead_edit()
    {
        //$id = $this->uri->segment(3);
        $id = $this->input->post('id');
        $this->data['query'] = $this->admin_model->table_fetch_row('expenseshead',array('id'=>$id));
        echo json_encode($this->data['query']);

//        if ($this->input->post(null, true) != false) {
//            $data = array('name'=>strtoupper($this->input->post('name')));
//            $update = $this->admin_model->table_update('expenseshead', $data, array('id'=>$id));
//            if ($update != false) {
//                $this->session->set_flashdata('message','Updated successfully.');
//                redirect('expenses/expenseshead_view');
//            } else {
//                $this->session->set_flashdata('message','Updated Unsuccessful.');
//            }
//        }
//
//        $this->template = 'expenses/expenseshead/edit';
//        $this->load->view('themes/cms',$this->data);

    }

    public function expenseshead_delete()
    {
        $id = $this->uri->segment(3);
        $this->admin_model->delete_table_row('expenseshead', array('id' => $id));
        redirect($_SERVER['HTTP_REFERER']);
        $this->load->view('themes/cms',$this->data);
    }

    public function pdf_expenses()
    {
        require_once(APPPATH.'third_party/html2pdf/html2pdf.class.php');
        $id= $this->input->get('clientId');
        $startDate= $this->input->get('startDate');
        $endDate= $this->input->get('endDate');
        $this->data['query'] = $this->admin_model->expenses_list($id,$startDate,$endDate);
        $template_pdf = $this->load->view('cms/pdf/pdf_expenses',$this->data,true);
        $html2pdf = new HTML2PDF('P','A4','en');
        $html2pdf->setTestIsImage(false);
        $html2pdf->WriteHTML($template_pdf);
        $html2pdf->Output('pdf_'.$id.'_expenses.pdf');
        EXIT();

    }


}
?>