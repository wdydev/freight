<?php

class Admin extends CI_Controller{
    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $this->data['primary_nav'] = $this->config->item('admin_nav');

            if ($this->session->userdata('logged_in')) {
                $session_data = $this->session->userdata('logged_in');
                $this->data['username'] = $session_data->name;
                $this->data['id'] = $session_data->id;
                $this->data['user'] = $this->session->userdata('logged_in');
                redirect('users/view');
//                $this->data['query'] = $this->admin_model->table_fetch_rows('users');
//                $this->template = 'users/list/list';
//                $this->load->view('themes/cms',$this->data);
            } else {
                redirect('admin/login');
            }
        }

    function logout()
    {
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect('admin', 'refresh');
    }

}
?>