<?php
class Clients extends CI_Controller{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
            $this->data['user'] = $this->session->userdata('logged_in');
        }
    }

    public function index()
    {
        //echo 'Testse';

    }

    public function add()
    {
        if ($this->input->post(null, true) != false) {
            $add = $this->admin_model->table_insert('clients',$_POST);
            if ($add!=false) {
                $this->data['message'] = 'Saved Successfully!!!';
            } else {
                $this->data['message'] = 'Sorry,The data could not be saved';
            }
        }
        $this->template = 'clients/add/add';
        $this->load->view('themes/cms',$this->data);
    }

    public function edit()
    {
//        $id = $this->uri->segment(3);
        $id = $this->input->post('id');
        $this->data['query'] = $this->admin_model->table_fetch_row('clients',array('id'=>$id));
        echo json_encode($this->data['query']);

    }

    public function view()
    {
        $this->data['query'] = $this->admin_model->table_fetch_rows('clients');

        if ($this->input->post(null, true) != false) {
            $update = $this->admin_model->table_update('clients', $_POST, array('id'=>$this->input->post('client_id')));
            if ($update != false) {
                $this->session->set_flashdata('message','Updated successfully.');
                redirect('clients/view');
            } else {
                $this->session->set_flashdata('message','Updated Unsuccessful.');
            }
        }
        $this->template = 'clients/list/list';
        $this->load->view('themes/cms',$this->data);
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->admin_model->delete_table_row('clients', array('id' => $id));
        redirect($_SERVER['HTTP_REFERER']);
        $this->load->view('themes/cms',$this->data);
    }
}