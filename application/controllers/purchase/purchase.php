<?php

class Purchase extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
            $this->data['user'] = $this->session->userdata('logged_in');
        }
    }

    public function index()
    {

    }

    public function add()
    {
        if ($this->input->post(null, true) != false) {
            $add = $this->admin_model->table_insert('purchases', $_POST);
            if ($add != false) {
                $this->data['message'] = 'Saved Successfully!!!';
            } else {
                $this->data['message'] = 'Sorry,The data could not be saved';
            }
        }
        $this->template = 'purchase/add/add';
        $this->load->view('themes/cms', $this->data);
    }

    public function edit()
    {
//        $id = $this->uri->segment(3);
        $id = $this->input->post('id');
        $this->data['query'] = $this->admin_model->table_fetch_row('purchases', array('id' => $id));
        echo json_encode($this->data['query']);
    }

    public function view()
    {
        if (($this->input->post(null, true) != false) && ($this->input->post('purchase_id') != '')) {
            $update = $this->admin_model->table_update('purchases', $_POST, array('id' => $this->input->post('purchase_id')));
            if ($update != false) {
                $this->session->set_flashdata('message', 'Updated successfully.');
                redirect('purchase/view');
            } else {
                $this->session->set_flashdata('message', 'Updated Unsuccessful.');
            }
        } elseif (($this->input->post(null, true) != false) &&($this->input->post('purchase_id') == '') && ($this->input->post('purchasesHeadId') != '')) {
            $id = $this->input->post('purchasesHeadId');
            $invoiceNo = $this->input->post('invoice_no');
            $startDate = $this->input->post('startDate');
            $endDate = $this->input->post('endDate');
            $this->data['query'] = $this->admin_model->purchase_list($id, $invoiceNo, $startDate, $endDate);
        } else {
            $this->data['query'] = NULL;
        }

        $this->template = 'purchase/list/list';
        $this->load->view('themes/cms', $this->data);
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->admin_model->delete_table_row('purchases', array('id' => $id));
        redirect($_SERVER['HTTP_REFERER']);
        $this->load->view('themes/cms', $this->data);
    }

    public function purchaseshead_view()
    {
        $this->data['query'] = $this->admin_model->table_fetch_rows('purchaseshead');

        if (($this->input->post(null, true) != false) &&  ($this->input->post('purchasehead_id') != '')) {
            $data = array('name' => strtoupper($this->input->post('name')));
            $update = $this->admin_model->table_update('purchaseshead', $data, array('id' => $this->input->post('purchasehead_id')));
            if ($update != false) {
                $this->session->set_flashdata('message', 'Updated successfully.');
                redirect('purchase/purchaseshead_view');
            } else {
                $this->session->set_flashdata('message', 'Updated Unsuccessful.');
            }
        }

        elseif (($this->input->post(null, true) != false) &&  ($this->input->post('purchasehead_id') == '')) {
            $data = array('name' => strtoupper($this->input->post('name')));
            $add = $this->admin_model->table_insert('purchaseshead', $data);
            if ($add != false) {
                $this->data['message'] = 'Purchases Head Saved Successfully!!!';
            } else {
                $this->data['message'] = 'Sorry,The data could not be saved';
            }
        }
        $this->template = 'purchase/purchaseshead/list';
        $this->load->view('themes/cms', $this->data);
    }

    public function purchaseshead_edit()
    {
        //$id = $this->uri->segment(3);
        $id = $this->input->post('id');
        $this->data['query'] = $this->admin_model->table_fetch_row('purchaseshead', array('id' => $id));
        echo json_encode($this->data['query']);

//        if ($this->input->post(null, true) != false) {
//            $data = array('name' => strtoupper($this->input->post('name')));
//            $update = $this->admin_model->table_update('purchaseshead', $data, array('id' => $id));
//            if ($update != false) {
//                $this->session->set_flashdata('message', 'Updated successfully.');
//                redirect('purchase/purchaseshead_view');
//            } else {
//                $this->session->set_flashdata('message', 'Updated Unsuccessful.');
//            }
//        }
//
//        $this->template = 'purchase/purchaseshead/edit';
//        $this->load->view('themes/cms', $this->data);

    }

    public function purchaseshead_delete()
    {
        $id = $this->uri->segment(3);
        $this->admin_model->delete_table_row('purchaseshead', array('id' => $id));
        redirect($_SERVER['HTTP_REFERER']);
        $this->load->view('themes/cms', $this->data);
    }

    public function pdf_purchases()
    {
        require_once(APPPATH . 'third_party/html2pdf/html2pdf.class.php');
        $id = $this->input->get('clientId');
        $invoiceNo = $this->input->get('invoiceNo');
        if(isset($_GET['startDate'])) {
            $startDate = date("Y-m-d", strtotime($_GET['startDate']));
            $endDate = date("Y-m-d", strtotime($_GET['endDate']));
        } else {
            $startDate = date('Y-m-01');
            $endDate = date("Y-m-d", strtotime($_GET['endDate']));
        }
        $this->data['query'] = $this->admin_model->purchase_list($id, $invoiceNo, $startDate, $endDate);
        $template_pdf = $this->load->view('cms/pdf/pdf_purchases', $this->data, true);
        $html2pdf = new HTML2PDF('P', 'A3', 'en');
        $html2pdf->setTestIsImage(false);
        $html2pdf->WriteHTML($template_pdf);
        $html2pdf->Output('pdf_'.$id.'_purchase.pdf');

    }


}

?>