<?php
class Login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in'))
        {
           redirect('admin');
        }
        $this->load->view('themes/login');
    }

    function index()
    {
        if (($this->session->userdata('user_email')!= FALSE))
        {
            redirect('admin');
        } else {
            $this->load->view('themes/login');
        }

    }

    function verify()
    {
        //This method will have the credentials validation
        $this->load->library('form_validation');

        $this->form_validation->set_rules('user_email', 'Username', 'required|trim|xss_clean');
        $this->form_validation->set_rules('user_password', 'Password', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            //Field validation failed.  User redirected to login page
            $this->template = 'login/form/form';
            $this->load->view('themes/login','');
        } else //valid
        {
            $this->load->model('admin_model');
            $res = $this->admin_model->login($this->input->post('user_email', TRUE), $this->input->post('user_password', TRUE));
            //echo "<pre>";print_r($res);die;
            if ($res != false && $res->status == 'A') {
                $this->session->set_userdata('logged_in', $res);
                redirect('admin','refresh');
            }
            else {
                //display message, user does not exist.
                $this->data['message'] = 'User does not exist.';
                $this->template = 'login/form/form';
                $this->load->view('themes/login',$this->data);
                return;
            }
        }
    }
}
?>