<?php

require_once(APPPATH . 'third_party/html2pdf/html2pdf.class.php');

class Invoice extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
            $this->data['user'] = $this->session->userdata('logged_in');
        }
    }

    public function index()
    {

    }

    public function add()
    {
        if ($this->input->post(null, true) != false) {
            $add = $this->admin_model->table_insert('invoices', $_POST);
            if ($add != false) {
                $query = $this->db->query("SELECT * FROM invoices WHERE invoice_number = '" . $this->input->post('invoice_number') . "' LIMIT 1");
                $detail = $query->row_array();
                for ($i = 0; $i < count($_POST['descriptionId']); $i++) {
                    $id = $_POST['descriptionId'][$i];
                    if ($id == "0") {
                        if (!empty($_POST['description'][$i])) {
                            $query = $this->db->query("INSERT INTO invoice_description set invoice_id = '" . $detail['id'] . "', description = '" . $_POST['description'][$i] . "',
						gst ='" . ($_POST['gst_nzd'][$i] == "zero" ? "0" : $_POST['gst_nzd'][$i]) . "', charges = '" . $_POST['charges_nzd'][$i] . "'");
                        }
                    }
                }
                $this->data['message'] = 'Saved Successfully!!!';
            } else {
                $this->data['message'] = 'Sorry,The data could not be saved';
            }
        }
        $this->template = 'invoice/add/add';
        $this->load->view('themes/cms', $this->data);
    }

    public function edit()
    {
        $id = $this->uri->segment(3);
        $this->data['query'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));

        if ($this->input->post(null, true) != false) {
            $update = $this->admin_model->table_update('invoices', $_POST, array('id' => $id));
            if ($update != false) {
                for ($i = 0; $i < count($_POST['descriptionId']); $i++) {
                    $desid = $_POST['descriptionId'][$i];
                    if ($desid == "0") {
                        if (!empty($_POST['description'][$i])) {
                            $this->db->query("INSERT INTO invoice_description set invoice_id = '" . $id . "', description = '" . $_POST['description'][$i] . "',
						gst ='" . $_POST['gst_nzd'][$i] . "', charges = '" . $_POST['charges_nzd'][$i] . "'");
                        }
                    } else {
                        if (!empty($_POST['description'][$i])) {
                            if (in_array($desid, $_POST['remove'])) {
                                foreach ($_POST['remove'] as $key) {
                                    if ($key == $desid) {
                                        $this->db->query("DELETE FROM invoice_description WHERE id = $desid");
                                    }
                                }
                            } else {
                                $this->db->query("update invoice_description set invoice_id = '" . $id . "', description = '" . $_POST['description'][$i] . "',
							gst ='" . ($_POST['gst_nzd'][$i] == "zero" ? "0" : $_POST['gst_nzd'][$i]) . "', charges = '" . $_POST['charges_nzd'][$i] . "'  where id = " . $desid);
                            }
                        }
                    }
                }
                $this->session->set_flashdata('message', 'Updated successfully.');
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                $this->session->set_flashdata('message', 'Updated Unsuccessful.');
            }
        }

        $this->template = 'invoice/edit/edit';
        $this->load->view('themes/cms', $this->data);
    }


    public function view()
    {
        if ($this->input->post(null, true) != false) {
            $id = $this->input->post('clientId');
            $invoiceNo = $this->input->post('invoiceNo');
            $containerNo = $this->input->post('containerNo');

            if ($this->input->post('date')) {
                $d = explode("-",$this->input->post('date'));
                $date = $d[0].$d[1];
            } else {
                $date = date('Y'). date('m');
            }
            if ($this->input->post('customGST ')) {
                $customGST = $this->input->post('customGST ');
            } else {
                $customGST = 1;
            }
            $this->data['query'] = $this->admin_model->invoice_list($id, $invoiceNo, $containerNo, $date, $customGST);
            $this->data['search_value'] = array(
                'id' => $id,
                'invoiceNo' => $invoiceNo,
                'containerNo' => $containerNo,
                'date' => $this->input->post('date'),
                'customGST' => $customGST,
            );
        } else {
            $this->data['query'] = NULL;
            $this->data['search_value'] = NULL;
        }
        $this->template = 'invoice/list/list';
        $this->load->view('themes/cms', $this->data);
    }

    public function edit_credit_note()
    {
        $id = $this->uri->segment(3);
        if ($id != NULL) {
            $this->data['query'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        } else {
            $this->data['query'] = NULL;
        }


        if ($this->input->post(null, true) != false) {
            $update = $this->admin_model->table_update('invoices', $_POST, array('id' => $id));
            if ($update != false) {
                for ($i = 0; $i < count($_POST['descriptionId']); $i++) {
                    $desid = $_POST['descriptionId'][$i];
                    if ($desid == "0") {
                        if (!empty($_POST['description'][$i])) {
                            $this->db->query("INSERT INTO credit_note_description set invoice_id = '" . $id . "', description = '" . $_POST['description'][$i] . "',
						gst ='" . $_POST['gst_nzd'][$i] . "', charges = '" . $_POST['charges_nzd'][$i] . "'");
                        }
                    } else {
                        if (!empty($_POST['description'][$i])) {
                            if (in_array($desid, $_POST['remove'])) {
                                foreach ($_POST['remove'] as $key) {
                                    if ($key == $desid) {
                                        $this->db->query("DELETE FROM credit_note_description WHERE id = $desid");
                                    }
                                }
                            } else {
                                $this->db->query("update credit_note_description set invoice_id = '" . $id . "', description = '" . $_POST['description'][$i] . "',
							gst ='" . $_POST['gst_nzd'][$i] . "', charges = '" . $_POST['charges_nzd'][$i] . "'  where id = " . $desid);
                            }
                        }
                    }
                }
                $this->session->set_flashdata('message', 'Updated successfully.');
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                $this->session->set_flashdata('message', 'Updated Unsuccessful.');
            }
        }

        $this->template = 'invoice/credit_note/edit';
        $this->load->view('themes/cms', $this->data);
    }

    public function edit_acceptance()
    {
        $id = $this->uri->segment(3);
        //$id = $this->input->get('id');
        if ($id != NULL) {
            $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
            $this->data['check'] = $this->admin_model->table_fetch_row('acceptance', array('invoiceId' => $id));
            if (empty($this->data['check'])) {
                $this->data['rsResults'] = NULL;
            } else {
                $this->data['rsResults'] = $this->admin_model->acceptance($id);
            }
        } else {
            $this->data['detail'] = NULL;
            $this->data['rsResults'] = NULL;
        }

        if ($this->input->post(null, true) != false) {
            $invoiceId = $id;
            $query = $this->db->query("SELECT * FROM acceptance WHERE invoiceId = " . $invoiceId . " LIMIT 1");
            if ($query->num_rows() > 0) {
                $update = $this->admin_model->table_update('acceptance', $_POST, array('invoiceId' => $id));
            } else {
                $update = $this->admin_model->table_insert('acceptance', $_POST);
            }
            if ($update != false) {
                $this->session->set_flashdata('message', 'Updated successfully.');
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                $this->session->set_flashdata('message', 'Updated Unsuccessful.');
            }
        }

        $this->template = 'invoice/acceptance/edit';
        $this->load->view('themes/cms', $this->data);
    }

    public function edit_sea_freight()
    {
        $id = $this->uri->segment(3);
        $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        $this->data['rsResults'] = $this->admin_model->table_fetch_row('sea_freight', array('invoiceId' => $id));
        $this->data['wsResults'] = $this->admin_model->table_fetch_row('sea_freight_detail', array('invoiceId' => $id));

        if ($this->input->post(null, true) != false) {
            $invoiceId = $id;
            //$acceptTC = (!empty($this->input->post('acceptTC')) ? $this->input->post('acceptTC') : "N");
            $rsRow = $this->admin_model->table_fetch_row('sea_freight', array('invoiceId' => $id));;
            if (!empty($rsRow)) {
                $update = $this->admin_model->table_update('sea_freight', $_POST, array('invoiceId' => $id));
            } else {
                $update = $this->admin_model->table_insert('sea_freight', $_POST);
            }
            if ($update != false) {
                for ($i = 0; $i < count($_POST['id']); $i++) {
                    $aid = $_POST['id'][$i];
                    if ($aid == "0") {
                        if (!empty($_POST['itemNo'][$i])) {
                            $this->db->query("INSERT INTO sea_freight_detail set invoiceId = $invoiceId, itemNo = '" . $_POST['itemNo'][$i] . "',
					marksAndNo = '" . $_POST['marksAndNo'][$i] . "',
					kindOfPackages = '" . $_POST['kindOfPackages'][$i] . "',description = '" . $_POST['description'][$i] . "',
					netWeight = '" . $_POST['netWeight'][$i] . "', totalCubic = '" . $_POST['totalCubic'][$i] . "',
					grossWeight = '" . $_POST['grossWeight'][$i] . "'");
                        }
                    } else {
                        if (!empty($_POST['itemNo'][$i])) {
                            if (in_array($aid, $_POST['remove'])) {
                                foreach ($_POST['remove'] as $key) {
                                    if ($key == $aid) {
                                        $this->db->query("DELETE FROM sea_freight_detail WHERE id = $aid");
                                    }
                                }
                            } else {
                                $this->db->query("update sea_freight_detail set invoiceId = $invoiceId, itemNo = '" . $_POST['itemNo'][$i] . "',
					marksAndNo = '" . $_POST['marksAndNo'][$i] . "',
					kindOfPackages = '" . $_POST['kindOfPackages'][$i] . "',description = '" . $_POST['description'][$i] . "',
					netWeight = '" . $_POST['netWeight'][$i] . "', totalCubic = '" . $_POST['totalCubic'][$i] . "',
					grossWeight = '" . $_POST['grossWeight'][$i] . "'  where id = " . $aid);
                            }
                        }
                    }
                }

                $this->session->set_flashdata('message', 'Updated successfully.');
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                $this->session->set_flashdata('message', 'Updated Unsuccessful.');
            }
        }


        $this->template = 'invoice/sea_freight/edit';
        $this->load->view('themes/cms', $this->data);
    }

    public function edit_air_freight()
    {
        $id = $this->uri->segment(3);
        $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        $this->data['rsResults'] = $this->admin_model->table_fetch_row('air_freight', array('invoiceId' => $id));
        $this->data['wsResults'] = $this->admin_model->table_fetch_row('air_freight_detail', array('invoiceId' => $id));

        if ($this->input->post(null, true) != false) {
            $invoiceId = $id;
            //$acceptTC = (!empty($this->input->post('acceptTC')) ? $this->input->post('acceptTC') : "N");
            $rsRow = $this->admin_model->table_fetch_row('air_freight', array('invoiceId' => $id));;
            if (!empty($rsRow)) {
                $update = $this->admin_model->table_update('air_freight', $_POST, array('invoiceId' => $id));
            } else {
                $update = $this->admin_model->table_insert('air_freight', $_POST);
            }
            if ($update != false) {
                for ($i = 0; $i < count($_POST['id']); $i++) {
                    $aid = $_POST['id'][$i];
                    if ($aid == "0") {
                        if (!empty($_POST['noPieces'][$i])) {
                            $this->db->query("INSERT INTO air_freight_detail set invoiceId = $invoiceId, noPieces = '" . $_POST['noPieces'][$i] . "',
					grossWeightKg = '" . $_POST['grossWeightKg'][$i] . "',
					rateClass = '" . $_POST['rateClass'][$i] . "',chargeableWeight = '" . $_POST['chargeableWeight'][$i] . "',
					rateCharge = '" . $_POST['rateCharge'][$i] . "', total = '" . $_POST['total'][$i] . "',
					nature = '" . $_POST['nature'][$i] . "'");
                        }
                    } else {
                        if (!empty($_POST['noPieces'][$i])) {
                            $this->db->query("update air_freight_detail set invoiceId = $invoiceId, noPieces = '" . $_POST['noPieces'][$i] . "',
					grossWeightKg = '" . $_POST['grossWeightKg'][$i] . "',
					rateClass = '" . $_POST['rateClass'][$i] . "',chargeableWeight = '" . $_POST['chargeableWeight'][$i] . "',
					rateCharge = '" . $_POST['rateCharge'][$i] . "', total = '" . $_POST['total'][$i] . "',
					nature = '" . $_POST['nature'][$i] . "'  where id = " . $aid);
                        }
                    }
                }

                $this->session->set_flashdata('message', 'Updated successfully.');
                redirect($_SERVER['HTTP_REFERER']);
            } else {
                $this->session->set_flashdata('message', 'Updated Unsuccessful.');
            }
        }

        $this->template = 'invoice/air_freight/edit';
        $this->load->view('themes/cms', $this->data);

    }

    public function edit_airwaybill()
    {
        $id = $this->uri->segment(3);
        $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        $this->data['rsResults'] = $this->admin_model->table_fetch_row('airwaybill', array('invoiceId' => $id));
        $this->data['wsResults'] = $this->admin_model->table_fetch_row('airwaybill_detail', array('invoiceId' => $id));
        if ($this->input->post(null, true) != false) {
            $invoiceId = $id;
            $rsRow = $this->admin_model->table_fetch_row('airwaybill', array('invoiceId' => $id));;
            if (!empty($rsRow)) {
                $update = $this->admin_model->table_update('airwaybill', $_POST, array('invoiceId' => $id));
            } else {
                $update = $this->admin_model->table_insert('airwaybill', $_POST);
            }
            if ($update != false) {
                for ($i = 0; $i<count($_POST['id']); $i++){
                    $aid = $_POST['id'][$i];
                    if ($aid == "0"){
                        if (!empty($_POST['noPieces'][$i])){
                            $this->db->query("INSERT INTO airwaybill_detail set invoiceId = $invoiceId, noPieces = '".$_POST['noPieces'][$i]."',
					grossWeightKg = '".$_POST['grossWeightKg'][$i]."',
					rateClass = '".$_POST['rateClass'][$i]."',chargeableWeight = '".$_POST['chargeableWeight'][$i]."',
					rateCharge = '".$_POST['rateCharge'][$i]."', total = '".$_POST['total'][$i]."',
					nature = '".$_POST['nature'][$i]."'");
                        }
                    }else{
                        if (!empty($_POST['itemNo'][$i])){
                            $this->db->query("update airwaybill_detail set invoiceId = $invoiceId, noPieces = '".$_POST['noPieces'][$i]."',
					grossWeightKg = '".$_POST['grossWeightKg'][$i]."',
					rateClass = '".$_POST['rateClass'][$i]."',chargeableWeight = '".$_POST['chargeableWeight'][$i]."',
					rateCharge = '".$_POST['rateCharge'][$i]."', total = '".$_POST['total'][$i]."',
					nature = '".$_POST['nature'][$i]."'  where id = ".$aid);
                        }
                    }
                }

                $this->session->set_flashdata('message', 'Updated successfully.');
                redirect($_SERVER['HTTP_REFERER']);
            }else {
                $this->session->set_flashdata('message', 'Updated Unsuccessful.');
            }
        }


        $this->template = 'invoice/airwaybill/edit';
        $this->load->view('themes/cms', $this->data);

    }

    public function edit_booking_seafreight()
    {
        $id = $this->uri->segment(3);
        if (!empty($id) && $id > 0) {
            $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
            $this->data['rsResults'] = $this->admin_model->table_fetch_row('booking_seafreight', array('invoiceId' => $id));
            $this->data['wsResults'] = $this->admin_model->table_fetch_row('booking_seafreight_detail', array('invoiceId' => $id));
            $this->data['detail1'] = NULL;
        }else{
            $this->data['detail'] =NULL;
            $this->data['detail1'] = $this->admin_model->table_fetch_rows('invoices',array(),array('id'=>'desc'));
        }

        if ($this->input->post(null, true) != false) {

            $invoiceId = $id;
            if ($invoiceId == 0) {
                $query = $this->db->query("INSERT INTO invoices set invoice_number = " . $_POST['shipmentNo'] . ", account_type='Air', invoice_date = " . date("Y-m-d", time()));
                $detail1 = $this->db->query("SELECT * FROM invoices order by id desc LIMIT 1");
                $invoiceId = $detail1['id'];
            }
            $rsRow = $this->admin_model->table_fetch_row('booking_seaFreight', array('invoiceId' => $id));
            if (!empty($rsRow)) {
                $update = $this->admin_model->table_update('booking_seaFreight', $_POST, array('invoiceId' => $id));
            } else {
                $update = $this->admin_model->table_insert('booking_seaFreight', $_POST);
            }

            if ($update != false) {
                for ($i = 0; $i < count($_POST['id']); $i++) {
                    $aid = $_POST['id'][$i];
                    if ($aid == "0") {
                        if (!empty($_POST['mode'][$i])) {
                            $this->db->query("INSERT INTO booking_seaFreight_detail set invoiceId = $invoiceId, mode = '" . $_POST['mode'][$i] . "', vessel_Voyage = '" . $_POST['vessel_Voyage'][$i] . "', IMO = '" . $_POST['IMO'][$i] . "',	carrier = '" . $_POST['carrier'][$i] . "', 	loadDetail = '" . $_POST['load'][$i] . "', disch = '" . $_POST['disch'][$i] . "',
					etd = '" . ($_POST['etd'][$i] == "" ? "0000-00-00" : date("Y-m-d", strtotime($_POST['etd'][$i]))) . "',
					eta = '" . ($_POST['eta'][$i] == "" ? "0000-00-00" : date("Y-m-d", strtotime($_POST['eta'][$i]))) . "'");
                        }
                    } else {
                        if (!empty($_POST['mode'][$i])) {
                            $this->db->query("update booking_seaFreight_detail set invoiceId = $invoiceId, mode = '" . $_POST['mode'][$i] . "', vessel_Voyage = '" . $_POST['vessel_Voyage'][$i] . "', IMO = '" . $_POST['IMO'][$i] . "',	carrier = '" . $_POST['carrier'][$i] . "', 	loadDetail = '" . $_POST['load'][$i] . "', disch = '" . $_POST['disch'][$i] . "',
					etd = '" . ($_POST['etd'][$i] == "" ? "0000-00-00" : date("Y-m-d", strtotime($_POST['etd'][$i]))) . "',
					eta = '" . ($_POST['eta'][$i] == "" ? "0000-00-00" : date("Y-m-d", strtotime($_POST['eta'][$i]))) . "' where id = " . $aid);

                        }
                    }
                }

                $this->session->set_flashdata('message', 'Updated successfully.');
                redirect($_SERVER['HTTP_REFERER']);
            }else {
                $this->session->set_flashdata('message', 'Updated Unsuccessful.');
            }
        }


        $this->template = 'invoice/booking_seafreight/edit';
        $this->load->view('themes/cms', $this->data);
    }

    public function edit_booking_airfreight()
    {
        $id = $this->uri->segment(3);
        if (!empty($id) && $id > 0) {
            $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
            $this->data['rsResults'] = $this->admin_model->table_fetch_row('booking_airfreight', array('invoiceId' => $id));
            $this->data['wsResults'] = $this->admin_model->table_fetch_row('booking_airfreight_detail', array('invoiceId' => $id));
            $this->data['detail1'] = NULL;
        }else{
            $this->data['detail'] =NULL;
            $this->data['detail1'] = $this->admin_model->table_fetch_rows('invoices',array(),array('id'=>'desc'));
        }

        if ($this->input->post(null, true) != false) {

            $invoiceId = $id;
            if ($invoiceId == 0) {
                $query = $this->db->query("INSERT INTO invoices set invoice_number = " . $_POST['shipmentNo'] . ", account_type='Air', invoice_date = " . date("Y-m-d", time()));
                $detail1 = $this->db->query("SELECT * FROM invoices order by id desc LIMIT 1");
                $invoiceId = $detail1['id'];
            }
            $rsRow = $this->admin_model->table_fetch_row('booking_airFreight', array('invoiceId' => $id));
            if (!empty($rsRow)) {
                $update = $this->admin_model->table_update('booking_airFreight', $_POST, array('invoiceId' => $id));
            } else {
                $update = $this->admin_model->table_insert('booking_airFreight', $_POST);
            }

            if ($update != false) {
                for ($i = 0; $i<=count($_POST['id']); $i++){
                    $aid = $_POST['id'][$i];
                    if ($aid == "0"){
                        if (!empty($_POST['mode'][$i])){
                            $this->db->query("INSERT INTO booking_airFreight_detail set invoiceId = $invoiceId, mode = '".$_POST['mode'][$i]."', flightName = '".$_POST['flightName'][$i]."',
					flightDate = '".($_POST['flightDate'][$i]==""?"0000-00-00":date("Y-m-d", strtotime($_POST['flightDate'][$i])))."',	carrier = '".$_POST['carrier'][$i]."', 	loadDetail = '".$_POST['load'][$i]."', disch = '".$_POST['disch'][$i]."',
					etd = '".($_POST['etd'][$i]==""?"0000-00-00":date("Y-m-d", strtotime($_POST['etd'][$i])))."',
					eta = '".($_POST['eta'][$i]==""?"0000-00-00":date("Y-m-d", strtotime($_POST['eta'][$i])))."'");
                        }
                    }else{
                        if (!empty($_POST['mode'][$i])){
                            $this->db->query("update booking_airFreight_detail set invoiceId = $invoiceId, mode = '".$_POST['mode'][$i]."', flightName = '".$_POST['flightName'][$i]."',
					flightDate = '".($_POST['flightDate'][$i]==""?"0000-00-00":date("Y-m-d", strtotime($_POST['flightDate'][$i])))."',	carrier = '".$_POST['carrier'][$i]."', 	loadDetail = '".$_POST['load'][$i]."', disch = '".$_POST['disch'][$i]."',
					etd = '".($_POST['etd'][$i]==""?"0000-00-00":date("Y-m-d", strtotime($_POST['etd'][$i])))."',
					eta = '".($_POST['eta'][$i]==""?"0000-00-00":date("Y-m-d", strtotime($_POST['eta'][$i])))."' where id = ".$aid);
                        }
                    }
                }

                $this->session->set_flashdata('message', 'Updated successfully.');
                redirect($_SERVER['HTTP_REFERER']);
            }else {
                $this->session->set_flashdata('message', 'Updated Unsuccessful.');
            }
        }


        $this->template = 'invoice/booking_airfreight/edit';
        $this->load->view('themes/cms', $this->data);
    }


    public function edit_weight_slip()
    {
        $id = $this->uri->segment(3);
        $this->data['detail1'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        $this->data['query'] = $this->admin_model->table_fetch_row('weight_slip_main', array('invoiceId' => $id));
        $this->data['wsResults'] = $this->admin_model->table_fetch_row('weight_slip', array('invoiceId' => $id));

        if ($this->input->post(null, true) != false) {
            $id = $this->uri->segment(3);
            $detail = $this->admin_model->table_fetch_row('weight_slip_main', array('invoiceId' => $id));

            if ($detail > 0) {
                $query = $this->admin_model->table_update('weight_slip_main', $_POST, array('invoiceId' => $id));
            } else {
                $query = $this->admin_model->table_insert('weight_slip_main', $_POST);
            }

            for ($i = 0; $i < count($_POST['id']); $i++) {
                $aid = $_POST['id'][$i];
                if ($aid == "0") {
                    if (!empty($_POST['pcs'][$i])) {
                        $query = $this->db->query("INSERT INTO weight_slip set invoiceId = $id, pcs = '" . $_POST['pcs'][$i] . "', weight = '" . $_POST['weight'][$i] . "',
					dims = '" . $_POST['dims'][$i] . "', lwh = '" . $_POST['lwh'][$i] . "', volume = '" . $_POST['volume'][$i] . "', volumeweight = '" . $_POST['volumeweight'][$i] . "'");

                    }
                } else {
                    if (!empty($_POST['pcs'][$i])) {
                        if (in_array($aid, $_POST['remove'])) {
                            foreach ($_POST['remove'] as $key) {
                                if ($key == $aid) {
                                    $query = $this->db->query("DELETE FROM weight_slip WHERE id =" . $aid);
                                }
                            }
                        } else {
                            $query = $this->db->query("update weight_slip set invoiceId = $id, pcs = '" . $_POST['pcs'][$i] . "', weight = '" . $_POST['weight'][$i] . "',
					dims = '" . $_POST['dims'][$i] . "', lwh = '" . $_POST['lwh'][$i] . "', volume = '" . $_POST['volume'][$i] . "', volumeweight = '" . $_POST['volumeweight'][$i] . "'  where id = " . $aid);
                        }
                    }
                }
            }
            $query = $this->db->query("update invoices set Destination_WS = '" . $_POST['destination'] . "' where id = $id");
        }


        $this->template = 'invoice/weight_slip/edit';
        $this->load->view('themes/cms', $this->data);

    }



    public function inventory_list()
    {
        $id = $this->uri->segment(3);
        $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        $this->data['ilResults'] = $this->admin_model->table_fetch_row('inventory_list', array('invoiceId' => $id));
        $this->data['wsResults'] = $this->admin_model->table_fetch_row('inventory_list_description', array('invoiceId' => $id));

        if ($this->input->post(null, true) != false) {
            $invoiceId = $this->uri->segment(3);

            for ($i = 0; $i < count($_POST['descriptionId']); $i++) {
                $aid = $_POST['descriptionId'][$i];
                if ($aid == "0") {
                    if (!empty($_POST['description'][$i])) {
                        $query = $this->db->query("INSERT INTO inventory_list_description set invoiceId = $invoiceId, description = '" . $_POST['description'][$i] . "'");
                    }
                } else {
                    if (!empty($_POST['description'][$i])) {
                        if (in_array($id, $_POST['remove'])) {
                            foreach ($_POST['remove'] as $key) {
                                if ($key == $aid) {
                                    $this->db->query("DELETE FROM inventory_list_description WHERE id = $aid");
                                }
                            }
                        } else {
                            $query = $this->db->query("update inventory_list_description set description = '" . $_POST['description'][$i] . "', invoiceId = $invoiceId
						where id = " . $aid);
                        }
                    }
                }
            }

            $ilResults = $this->db->query("select * from inventory_list where invoiceId = $invoiceId");
            if ($ilResults->num_rows() > 0) {
                $query = $this->db->query("update inventory_list set address = '" . $_POST['address'] . "',
			noPackages = '" . $_POST['noPackages'] . "', valueOfGoods = '" . $_POST['valueOfGoods'] . "', shipperName = '" . $_POST['shipperName'] . "' where invoiceId = $invoiceId");
            } else {
                $query = $this->db->query("insert into inventory_list set address = '" . $_POST['address'] . "',
			noPackages = '" . $_POST['noPackages'] . "', valueOfGoods = '" . $_POST['valueOfGoods'] . "' , shipperName = '" . $_POST['shipperName'] . "', invoiceId = $invoiceId");
            }
        }

        $this->template = 'invoice/inventory_list/edit';
        $this->load->view('themes/cms', $this->data);
    }

    public function email()
    {
        $invoiceId = $this->uri->segment(3);
        $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $invoiceId));

        if ($this->input->post(null, true) != false) {
            $invoiceId = $this->uri->segment(3);
            $accType = $this->input->post('account_type');
            $invoice_number = $this->input->post('invoice_number');

            $arrString = "";
            $dbString = "";
            $files = array();

            if ($_POST['attachInvoice'] == "Y") {
                $files[] = "pdf_".$invoice_number . "_invoice_list.pdf";
                $dbString .= "attachInvoice = 'Y',";
            }
            if ($_POST['attachAuthority'] == "Y") {
                $files[] = "pdf_".$invoice_number . "_authority_letter.pdf";
                $arrString .= "'". "pdf_". $invoice_number . "_authority_letter.pdf',";
                $dbString .= "attachAuthority = 'Y',";
            }
            if ($_POST['attachWeightSlip'] == "Y") {
                $files[] = "pdf_".$invoice_number . "_weight_slip.pdf";
                $arrString .= "'" ."pdf_". $invoice_number . "_weight_slip.pdf',";
                $dbString .= "attachWeightSlip = 'Y',";
            }
            if (isset($_POST['attachAirFreight']) && $_POST['attachAirFreight'] == "Y") {
                $files[] = "pdf_".$invoice_number . "_air_freight.pdf";
                $arrString .= "'" . "pdf_".$invoice_number . "_air_freight.pdf',";
                $dbString .= "attachAirFreight = 'Y',";
            }
            if (isset($_POST['attachSeaFreight']) && $_POST['attachSeaFreight'] == "Y") {
                $files[] = "pdf_".$invoice_number . "_sea_freight.pdf";
                $arrString .= "'" ."pdf_". $invoice_number . "_sea_freight.pdf',";
                $dbString .= "attachSeaFreight = 'Y',";
            }
            if (isset($_POST['attachBookingAirFreight']) && $_POST['attachBookingAirFreight'] == "Y") {
                $files[] = $invoice_number . "_booking_air_freight.pdf";
                $arrString .= "'" . $invoice_number . "_booking_air_freight.pdf',";
                $dbString .= "attachBookingAirFreight = 'Y',";
            }
            if (isset($_POST['attachBookingSeaFreight']) && $_POST['attachBookingSeaFreight'] == "Y") {
                $files[] = $invoice_number . "_booking_sea_freight.pdf";
                $arrString .= "'" . $invoice_number . "_booking_sea_freight.pdf',";
                $dbString .= "attachBookingSeaFreight = 'Y',";
            }
            if ($_POST['attachAcceptanceForm'] == "Y") {
                $files[] = "pdf_".$invoice_number . "_acceptance_questionaire.pdf";
                $arrString .= "'" ."pdf_". $invoice_number . "_acceptance_questionaire.pdf',";
                $dbString .= "attachAcceptanceForm = 'Y',";
            }
            if ($_POST['attachInventoryList'] == "Y") {
                $files[] = "pdf_".$invoice_number . "_inventory_list.pdf";
                $arrString .= "'" ."pdf_". $invoice_number . "_inventory_list.pdf',";
                $dbString .= "attachInventoryList = 'Y',";
            }
            if ($_POST['attachAirwaybill'] == "Y") {
                $files[] = "pdf_".$invoice_number . "_airwaybill.pdf";
                $arrString .= "'". "pdf_". $invoice_number . "_airwaybill.pdf',";
                $dbString .= "attachAirwaybill = 'Y',";
            }

            $imageNames = "";

	   if(isset($_FILES['documents']) && count($_FILES['documents']))
            for ($i = 0; $i < count($_FILES['documents']['name']); $i++) {
                if ($_FILES['documents']['name'][$i] != '') {
                    if ($_FILES['documents']['type'][$i] == "application/pdf" || $_FILES['documents']['type'][$i] == "text/pdf") {
                        $imagename = $_FILES['documents']['name'][$i];

                        $source = $_FILES['documents']['tmp_name'][$i];
                        $imagename = $invoice_number . '_' . str_replace(" ", "_", $imagename);
                        $files[] = $imagename;
                        $imageNames .= $imagename . ";";
                        $target = "pdf/" . $imagename;
                        move_uploaded_file($source, $target);
                    }
                }
            }

            if (!empty($imageNames)) {
                $imageNames1 = substr($imageNames, 0, strlen($imageNames) - 1);
                $dbString .= "documents = '" . $imageNames1 . "',";
            }

            $detail1 = $this->admin_model->table_fetch_row('clients', array('id' => $this->input->post('clientId')));
            if (!empty($detail1[0]->email)) {

                // email fields: to, from, subject, and so on
                $to = $this->input->post('email');
                $from = "sujakhu.umesh@gmail.com";
                $subject = $this->input->post('subject');
                $message = $this->input->post('message');


                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://rsj30.rhostjh.com',
                    'smtp_port' => 465,
                    'smtp_user' => 'support@rebannepal.com',
                    'smtp_pass' => 'rebannepal12',
                    'mailtype' => 'html',
                    'charset' => 'iso-8859-1',
                    'wordwrap' => TRUE
                );

                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                $this->email->from($from);
                $this->email->subject("(Support Mail) " . $subject);
                $this->email->message($message
                );
                $this->email->to($to);

                $query = $this->admin_model->table_insert('email', array('email' => $this->input->post('email'), 'invoiceId' => $invoiceId, 'subject' => $this->input->post('subject'), 'message' => $this->input->post('message'), 'createdDate' => date("Y-m-d H:i:s", time())));

            }
        }
        $this->template = 'invoice/email/edit';
        $this->load->view('themes/cms', $this->data);
    }



    public function pdf_invoice_list()
    {
        $id = $this->input->get('clientId');
        $invoiceNo = $this->input->get('invoiceNo');
        $containerNo = $this->input->get('containerNo');
        if ($this->input->get('date')) {
            $d = explode("-",$this->input->get('date'));
            $date = $d[0].$d[1];
        } else {
            $date = date('Y'). date('m');
        }
        $customGST = $this->input->get('customGST');
        $this->data['query'] = $this->admin_model->invoice_list($id, $invoiceNo, $containerNo, $date, $customGST);
        if($this->input->get('customYN')) {
            $template_pdf = $this->load->view('cms/pdf/pdf_invoice_customGST', $this->data, true);
        }else{
            $template_pdf = $this->load->view('cms/pdf/pdf_invoice_list', $this->data, true);
        }
        $html2pdf = new HTML2PDF('P', 'A3', 'en');
        $html2pdf->setTestIsImage(false);
        $html2pdf->WriteHTML($template_pdf);
        $html2pdf->Output('pdf_'.$id.'_invoice_list.pdf');
        EXIT();
    }

    public function pdf_invoice_credit_note()
    {
        require_once(APPPATH . 'third_party/fpdf/fpdf.php');
        $id = $this->input->get('invoice_no');
        $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        $this->data['query'] = $this->admin_model->creditNoteTotal($id);
        $this->load->view('cms/pdf/pdf_invoice_credit_note', $this->data, true);
    }

    public function pdf_acceptance()
    {
        $id = $this->input->get('invoiceId');
        $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        $this->data['rsResults'] = $this->admin_model->table_fetch_row('acceptance', array('invoiceId' => $id));
        $template_pdf = $this->load->view('cms/pdf/pdf_acceptance_questionaire', $this->data, true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en');
        $html2pdf->setTestIsImage(false);
        $html2pdf->WriteHTML($template_pdf);
        $html2pdf->Output('pdf_'.$id.'_acceptance_questionaire.pdf');
        EXIT();
    }

    public function pdf_authority_letter()
    {
        $id = $this->input->get('invoiceId');
        $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        $this->data['rsResults'] = $this->admin_model->table_fetch_row('invoice_description', array('invoice_id' => $id));
        $template_pdf = $this->load->view('cms/pdf/pdf_authority_letter', $this->data, true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en');
        $html2pdf->setTestIsImage(false);
        $html2pdf->WriteHTML($template_pdf);
        $html2pdf->Output('pdf_'.$id.'_authority_letter.pdf');
        exit;
    }

    public function pdf_airwaybill()
    {
        $id = $this->input->get('invoiceId');
        $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        $this->data['rsResults'] = $this->admin_model->airwaybill_detail($id);
        $this->data['wsResults'] = $this->admin_model->table_fetch_row('airwaybill', array('invoiceId' => $id));
        $template_pdf = $this->load->view('cms/pdf/pdf_airwaybill', $this->data, true);
        $html2pdf = new HTML2PDF('P', 'A3', 'en');
        $html2pdf->setTestIsImage(false);
        $html2pdf->WriteHTML($template_pdf);
        $html2pdf->Output('pdf_'.$id.'_airwaybill.pdf');
        exit;
    }

    public function pdf_inventory_list()
    {
        $id = $this->input->get('invoiceId');
        $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        $this->data['ilResults'] = $this->admin_model->table_fetch_row('inventory_list', array('invoiceId' => $id));
        $this->data['wsResults'] = $this->admin_model->table_fetch_row('inventory_list_description', array('invoiceId' => $id));
        $template_pdf = $this->load->view('cms/pdf/pdf_inventory_list', $this->data, true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en');
        $html2pdf->setTestIsImage(false);
        $html2pdf->WriteHTML($template_pdf);
        $html2pdf->Output('pdf_'.$id.'_inventory_list.pdf');
        exit;
    }

    public function pdf_airfreight()
    {
        $id = $this->input->get('invoiceId');
        $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        $this->data['rsResults'] = $this->admin_model->airfreight_detail($id);
        $this->data['wsResults'] = $this->admin_model->table_fetch_row('air_freight_detail', array('invoiceId' => $id));
        $template_pdf = $this->load->view('cms/pdf/pdf_airfreight', $this->data, true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en');
        $html2pdf->setTestIsImage(false);
        $html2pdf->WriteHTML($template_pdf);
        $html2pdf->Output('pdf_'.$id.'_airfreight.pdf');
        exit;
    }

    public function pdf_weight_slip()
    {
        $id = $this->input->get('invoiceId');
        $this->data['detail1'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        $this->data['detail'] = $this->admin_model->table_fetch_row('weight_slip_main', array('invoiceId' => $id));
        $this->data['rsResults'] = $this->admin_model->table_fetch_row('weight_slip', array('invoiceId' => $id));
        $template_pdf = $this->load->view('cms/pdf/pdf_weight_slip', $this->data, true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en');
        $html2pdf->setTestIsImage(false);
        $html2pdf->WriteHTML($template_pdf);
        $html2pdf->Output('pdf_'.$id.'_weight_slip.pdf');
        exit();
    }

    public function pdf_edit_seafreight()
    {
        $id = $this->input->get('invoiceId');
        $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        $this->data['rsResults'] = $this->admin_model->seafreight($id);
        $this->data['wsResults'] = $this->admin_model->table_fetch_row('sea_freight_detail', array('invoiceId' => $id));
        $template_pdf = $this->load->view('cms/pdf/pdf_edit_seafreight', $this->data, true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en');
        $html2pdf->setTestIsImage(false);
        $html2pdf->WriteHTML($template_pdf,false);
        $html2pdf->Output('pdf_'.$id.'_seafreight.pdf');
        //exit;
    }

    public function pdf_booking_seafreight()
    {
        $id = $this->input->get('invoiceId');
        $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        $this->data['rsResults'] = $this->admin_model->bookingseafreight_pdf($id);
        $this->data['wsResults'] = $this->admin_model->table_fetch_row('booking_seafreight_detail', array('invoiceId' => $id));
        $template_pdf = $this->load->view('cms/pdf/pdf_booking_seafreight', $this->data, true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en');
        $html2pdf->setTestIsImage(false);
        $html2pdf->WriteHTML($template_pdf,false);
        $html2pdf->Output('pdf_'.$id.'_booking_seafreight.pdf');
        //exit;
    }

    public function pdf_booking_airfreight()
    {
        $id = $this->input->get('invoiceId');
        $this->data['detail'] = $this->admin_model->table_fetch_row('invoices', array('id' => $id));
        $this->data['rsResults'] = $this->admin_model->bookingairfreight_pdf($id);
        $this->data['wsResults'] = $this->admin_model->table_fetch_row('booking_airfreight_detail', array('invoiceId' => $id));
        $template_pdf = $this->load->view('cms/pdf/pdf_booking_airfreight', $this->data, true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en');
        $html2pdf->setTestIsImage(false);
        $html2pdf->WriteHTML($template_pdf,false);
        $html2pdf->Output('pdf_'.$id.'_booking_airfreight.pdf');
        //exit;
    }
}

?>