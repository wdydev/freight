<?php

class Reports extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
            $this->data['user'] = $this->session->userdata('logged_in');
        }
    }

    public function index()
    {

    }

    public function view()
    {
        if ($this->input->post(null, true) != false) {
            $id = $this->input->post('clientId');
            $startDate = $this->input->post('startDate');
            $endDate = $this->input->post('endDate');
            $this->data['query'] = $this->admin_model->reports_list($id, $startDate, $endDate);
        } else {
            $this->data['query'] = NULL;
        }
        $this->template = 'reports/list/list';
        $this->load->view('themes/cms', $this->data);
    }

    public function add_account()
    {
        if ($this->input->post(null, true) != false) {
            $add = $this->admin_model->table_insert('account_paid', $_POST);
            if ($add != false) {
                $this->data['message'] = 'Saved Successfully!!!';
            } else {
                $this->data['message'] = 'Sorry,The data could not be saved';
            }
        }
        $this->template = 'reports/account/add/add';
        $this->load->view('themes/cms', $this->data);

    }

    public function edit_account()
    {
//        $id = $this->uri->segment(3);
        $id = $this->input->post('id');
        $this->data['query'] = $this->admin_model->table_fetch_row('account_paid', array('id' => $id));
        echo json_encode($this->data['query']);
    }

    public function view_account()
    {
        if (($this->input->post(null, true) != false) && ($this->input->post('account_id') != '')) {
            $update = $this->admin_model->table_update('account_paid', $_POST, array('id' => $this->input->post('account_id')));
            if ($update != false) {
                $this->session->set_flashdata('message', 'Updated successfully.');
                redirect('reports/view_account');
            } else {
                $this->session->set_flashdata('message', 'Updated Unsuccessful.');
            }
        } else if (($this->input->post(null, true) != false) && ($this->input->post('clientId') != '') && ($this->input->post('account_id') == '')) {
            $id = $this->input->post('clientId');
            $this->data['query'] = $this->admin_model->table_fetch_rows('account_paid', array('clientId' => $id), array('paidDate' => 'asc'));
        } else {
            $this->data['query'] = NULL;
        }
        $this->template = 'reports/account/list/list';
        $this->load->view('themes/cms', $this->data);
    }

    public function delete_account()
    {
        $id = $this->uri->segment(3);
        $this->admin_model->delete_table_row('account_paid', array('id' => $id));
        redirect($_SERVER['HTTP_REFERER']);
        $this->load->view('themes/cms', $this->data);
    }

    public function add_opening_blnc()
    {
        if ($this->input->post(null, true) != false) {
            $add = $this->admin_model->table_insert('client_openingbalance', $_POST);
            if ($add != false) {
                $this->data['message'] = 'Saved Successfully!!!';
            } else {
                $this->data['message'] = 'Sorry,The data could not be saved';
            }
        }
        $this->template = 'reports/opening_balance/add/add';
        $this->load->view('themes/cms', $this->data);

    }

    public function edit_opening_blnc()
    {
//        $id = $this->uri->segment(3);
        $id = $this->input->post('id');
        $this->data['query'] = $this->admin_model->table_fetch_row('client_openingbalance', array('id' => $id));
        echo json_encode($this->data['query']);
    }


    public function view_opening_blnc()
    {
        if (($this->input->post(null, true) != false) && ($this->input->post('opening_blnc_id') != '')) {
            $update = $this->admin_model->table_update('client_openingbalance', $_POST, array('id' => $this->input->post('opening_blnc_id')));
            if ($update != false) {
                $this->session->set_flashdata('message', 'Updated successfully.');
                redirect('reports/view_opening_blnc');
            } else {
                $this->session->set_flashdata('message', 'Updated Unsuccessful.');
            }
        } else if (($this->input->post(null, true) != false) && ($this->input->post('clientId') != '') && ($this->input->post('opening_blnc_id') == '')) {
            $id = $this->input->post('clientId');
            $this->data['query'] = $this->admin_model->table_fetch_rows('client_openingbalance', array('clientId' => $id), array());
        } else {
            $this->data['query'] = NULL;
        }
        $this->template = 'reports/opening_balance/list/list';
        $this->load->view('themes/cms', $this->data);
    }


    public function delete_opening_blnc()
    {
        $id = $this->uri->segment(3);
        $this->admin_model->delete_table_row('client_openingbalance', array('id' => $id));
        redirect($_SERVER['HTTP_REFERER']);
        $this->load->view('themes/cms', $this->data);
    }

    public function pdf_reports()
    {
        require_once(APPPATH . 'third_party/html2pdf/html2pdf.class.php');
        $id = $this->input->get('clientId');
        $startDate = $this->input->get('startDate');
        $endDate = $this->input->get('endDate');
        $this->data['query'] = $this->admin_model->reports_list($id, $startDate, $endDate);
        $template_pdf = $this->load->view('cms/pdf/pdf_reports', $this->data, true);
        $html2pdf = new HTML2PDF('P', 'A4', 'en');
        $html2pdf->setTestIsImage(false);
        $html2pdf->WriteHTML($template_pdf);
        $html2pdf->Output('pdf_'.$id.'_reports.pdf');
    }
}

?>