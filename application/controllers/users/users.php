<?php

class Users extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('logged_in')) {
            $this->data['user'] = $this->session->userdata('logged_in');
        }
    }

    public function index()
    {

    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'UserName', 'trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'trim|required|matches[password]');

        if ($this->form_validation->run() == FALSE) {
            $this->data['msg'] = $this->form_validation->error_string();
        } else {
        if ($this->input->post(null, true) != false) {
                $add = $this->admin_model->table_insert('users',$_POST);
                if ($add!=false) {
                    $this->data['message'] = 'Saved Successfully!!!';
                } else {
                    $this->data['message'] = 'Sorry,The data could not be saved';
                }
            }
        }

        $this->template = 'users/add/add';
        $this->load->view('themes/cms',$this->data);

    }

    public function edit()
    {
        //$id = $this->uri->segment(3);
        $id = $this->input->post('id');
        $this->data['query'] = $this->admin_model->table_fetch_row('users', array('id' => $id));
        echo json_encode($this->data['query']);
    }

    public function view()
    {
        $this->data['query'] = $this->admin_model->table_fetch_rows('users');

        if ($this->input->post(null, true) != false) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'UserName', 'trim|required|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim');
            $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'trim|matches[password]');

            $user_data = $this->db->where('id', $this->input->post('user_id'))->get('users')->result_object();
            $this->data['query'] = $user_data;

            if (
                $this->
                db->
                where('id !=', $this->input->post('user_id'))->
                where('email', $this->input->post('email'))->
                get('users')->num_rows() > 0
            ) {
                $this->data['message'] = 'Email is already in use by another account.';
                return;
            }

            if ($this->form_validation->run() == FALSE) {
                $this->data['msg'] = $this->form_validation->error_string();
            } else {
                if ($this->input->post(null, true) != false) {

                    $data = array(
                        'name'=> $this->input->post('name'),
                        'email'=> $this->input->post('email'),
                        'add'=> $this->input->post('add'),
                        'edit'=> $this->input->post('edit'),
                        'delete'=> $this->input->post('delete'),
                        'search'=> $this->input->post('search')
                    );
                    if ($this->input->post("password")) {
                        $data['password'] = $this->input->post("password");
                    }
                    if ($this->input->post("status")) {
                        $data['status'] = $this->input->post("status");
                    }
                    $this->db->where('id',$this->input->post('user_id'));
                    if ($this->db->update('users',$data)) {
                        $this->session->set_flashdata('message','Updated successfully.');
                        redirect('users/view');
                    } else {
                        $this->session->set_flashdata('message','Updated Unsuccessful.');
                    }
                }
            }
        }


        $this->template = 'users/list/list';
        $this->load->view('themes/cms',$this->data);
    }

    public function change_access()
    {
        $type= $_POST['type'];
        $value= $_POST['value'];
        $id= $_POST['id'];
        if($value == 1){
            $data = array(
                $type =>'0'
            );
        }else{
            $data = array(
                $type =>'1'
            );
        }
        $this->db->where('id',$id);
        $this->db->update('users',$data);
    }

    public function change_status()
    {
        $value= $_POST['value'];
        $id= $_POST['id'];
        if($value == 'A'){
            $data = array(
                'status' =>'D'
            );
        }else{
            $data = array(
                'status' =>'A'
            );
        }
        $this->db->where('id',$id);
        $this->db->update('users',$data);
    }


}